pluginManagement {
    repositories {
        google {
            content {
                includeGroupByRegex("com\\.android.*")
                includeGroupByRegex("com\\.google.*")
                includeGroupByRegex("androidx.*")
            }
        }
        mavenCentral()
        maven("https://clojars.org/repo/")
        maven("https://jitpack.io")
        maven("https://repo1.maven.org/maven2/")
        maven("https://maven.aliyun.com/repository/public")
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
        maven("https://tencent-tds-maven.pkg.coding.net/repository/shiply/repo")
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        maven("https://clojars.org/repo/")
        maven("https://jitpack.io")
        maven("https://repo1.maven.org/maven2/")
        maven("https://tencent-tds-maven.pkg.coding.net/repository/shiply/repo")
        maven("https://maven.aliyun.com/repository/public")
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
        mavenCentral()
    }
}

rootProject.name = "CourseTable-compose"
include(":app")
include(":DataModel")
