package com.mypotota.datamodel.model

import android.os.Build
import androidx.annotation.RequiresApi
import com.mypotota.datamodel.bean.CourseTable
import com.mypotota.datamodel.bean.Kb
import java.time.LocalDate

@RequiresApi(Build.VERSION_CODES.O)
class CourseTableModel {


    fun getToDayCourseTable(currentWeekNumber:Int,courseTable: CourseTable): ArrayList<Kb> {
        //获取今日是第几周、然后周几
        val today = LocalDate.now() // 获取当前日期
        val dayOfWeek = today.dayOfWeek // 获取今天是周几
        val toDayCourseTable = getCurrentWeekCourseTableByDay(currentWeekNumber,dayOfWeek.value,courseTable)
        return toDayCourseTable
    }

    /**
     * 获取明天的课表
     */

    fun getTomorrowCourseTable(currentWeekNumber:Int,courseTable: CourseTable): ArrayList<Kb> {
        val today = LocalDate.now() // 获取当前日期
        val dayOfWeek = today.dayOfWeek // 获取今天是周几
        val tomorrow = getCurrentWeekCourseTableByDay(currentWeekNumber,dayOfWeek.value + 1,courseTable)
        return tomorrow
    }



    private fun getCurrentWeekCourseTableByDay(currentWeekNumber:Int,dayOfWeek: Int,courseTable: CourseTable): ArrayList<Kb> {
        //解析指定周次的课程

        val currentWeekCourseTable: ArrayList<Kb>
        val weekStr: String
        when (dayOfWeek) {
            1 -> {
                weekStr = "星期一"
                currentWeekCourseTable = getCurrentCourseTable(courseTable,currentWeekNumber)
            }

            2 -> {
                weekStr = "星期二"
                currentWeekCourseTable = getCurrentCourseTable(courseTable,currentWeekNumber)
            }

            3 -> {
                weekStr = "星期三"
                currentWeekCourseTable = getCurrentCourseTable(courseTable,currentWeekNumber)
            }

            4 -> {
                weekStr = "星期四"
                currentWeekCourseTable = getCurrentCourseTable(courseTable,currentWeekNumber)
            }

            5 -> {
                weekStr = "星期五"
                currentWeekCourseTable = getCurrentCourseTable(courseTable,currentWeekNumber)
            }

            6 -> {
                weekStr = "星期六"
                currentWeekCourseTable = getCurrentCourseTable(courseTable,currentWeekNumber)
            }

            7 -> {
                weekStr = "星期日"
                currentWeekCourseTable = getCurrentCourseTable(courseTable,currentWeekNumber)
            }

            else -> {
                weekStr = "星期一"
                currentWeekCourseTable = getCurrentCourseTable(courseTable,currentWeekNumber + 1)
            }
        }


        //获取今日的课表
        val toDayCourseTableList = ArrayList<Kb>()
        currentWeekCourseTable.forEach {
            if (it.xqjmc.equals(weekStr)) {

                toDayCourseTableList.add(it)
            }
        }
        //根据现在的时段判断是否有课程
        return toDayCourseTableList
    }






    private fun getCurrentCourseTable(courseTable: CourseTable,weekNumber: Int): ArrayList<Kb> {
        //解析指定周次的课程
        val kbList = courseTable.kbList

        val courseList = ArrayList<Kb>()

        kbList.forEach { item ->
            //解析周次
            val zcd = item.zcd.replace("周", "")
            val zcds = zcd.split(",")
            //计算每节课是否包含在数据中 -- [4-8,13-20]
            zcds.forEach {
                if (it.indexOf("双") != -1) {
                    val week = it.replace("(双)", "")
                    val weekSp = week.split("-")
                    val newCourseWeek = weekSp[0].toInt()..weekSp[weekSp.size - 1].toInt()
                    if ((weekNumber % 2 == 0) && newCourseWeek.contains(weekNumber)) {
                        courseList.add(item)
                    }
                } else if (it.indexOf("单") != -1) {
                    val week = it.replace("(单)", "")
                    val weekSp = week.split("-")
                    val newCourseWeek = weekSp[0].toInt()..weekSp[weekSp.size - 1].toInt()
                    if ((weekNumber % 2 != 0) && newCourseWeek.contains(weekNumber)) {
                        courseList.add(item)
                    }
                } else {
                    if (it.indexOf("-") != -1) {
                        val newCourseWeek =
                            it.split("-")[0].toInt()..it.split("-")[it.split("-").size - 1].toInt()
                        if (newCourseWeek.contains(weekNumber)) {
                            courseList.add(item)
                        }
                    } else {
                        val courseWeek = it.toInt()
                        if (courseWeek == weekNumber) {
                            courseList.add(item)
                        }
                    }
                }
            }
        }
        return courseList
    }

    fun refreshColors(courseTable: CourseTable,colorMap:HashMap<String,Long>,colors:List<Long>): HashMap<String, Long> {
        val kbList = courseTable.kbList
        kbList.forEachIndexed { index, item ->
            if (colorMap["${item.kcmc}-${item.xm}"] == null) {
                colorMap["${item.kcmc}-${item.xm}"] = colors.random()
            }
        }
        return colorMap
    }


}