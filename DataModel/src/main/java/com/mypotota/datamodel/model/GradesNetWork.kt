package com.mypotota.datamodel.model

import com.example.coursetable_compose.api.Api
import com.example.coursetable_compose.api.LoginBuilder
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mypotota.datamodel.api.ApiService
import com.mypotota.datamodel.api.LoginTicket
import com.mypotota.datamodel.bean.BaseResponse
import com.mypotota.datamodel.bean.CourseTable
import com.mypotota.datamodel.bean.GradesBean
import com.mypotota.datamodel.bean.LoginBean
import com.mypotota.datamodel.bean.TokenBean
import com.mypotota.datamodel.utils.BuildConfig
import com.mypotota.datamodel.utils.Utils
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.Call
import okhttp3.Callback
import okhttp3.FormBody
import okhttp3.Request
import okhttp3.Response
import org.jsoup.Connection
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.io.IOException

class GradesNetWork(val onGetGradesStatus: IOnGetGradesStatus) {
    interface IOnGetGradesStatus{
        fun onLoading(msg:String)
        fun error(e:String)
        fun success(gradesBean: GradesBean)
    }
    private val cookieMap = HashMap<String, String>()
    private var mPassword = ""
    private var mAccount = ""
    private var mCurrentSemester=""
    /**
     * 获取登录验证码
     */
    @OptIn(DelicateCoroutinesApi::class)
    fun getLoginTicket(account: String, password: String,currentSemester:String) {
        GlobalScope.launch {
            withContext(Dispatchers.IO) {
                mCurrentSemester=currentSemester
                try {
                    onGetGradesStatus.onLoading("正在获取登录验证码")
                    val createLoginTicket = LoginTicket.createLoginTicket(account)
                    val loginTicket = ApiService.apiService.getLoginTicket(createLoginTicket)
                    if (loginTicket.code.equals("40001")) {
                        val content = loginTicket.content
                        //执行登录
                        onGetGradesStatus.onLoading("登录验证码生成成功")
                        mAccount = account
                        checkAccountTickt(password, content.ticket)
                    } else {
                        onGetGradesStatus.error("获取登录验证码失败，${loginTicket.message}")
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    onGetGradesStatus.error("获取登录验证码错误：${e.message.toString()}")
                }
            }
        }
    }

    /**
     * 登录
     */
    private fun checkAccountTickt(password: String, ticket: String) {
        GlobalScope.launch {
            try {
                onGetGradesStatus.onLoading("正在验证登录信息")
                val loginMap = LoginBuilder.getLogin(password)
                val stringBuffer = StringBuffer()
                loginMap.forEach {
                    stringBuffer.append("${it.key}=${it.value}&")
                }
                val okHttpClient = ApiService.okHttpClient
                val build = Request.Builder()
                    .url(
                        Api.BASE_URL + "/ump/pc/login/account/checkAccount/" + ticket + "?" + stringBuffer.substring(
                            0,
                            stringBuffer.length - 1
                        )
                    )
                    .post(FormBody.Builder().build())
                    .build()
                okHttpClient.newCall(build).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        e.printStackTrace()
                        onGetGradesStatus.error("登录时发生错误，" + e.message)
                    }

                    override fun onResponse(call: Call, response: Response) {
                        val login =
                            Gson().fromJson<BaseResponse<LoginBean>>(
                                response.body()!!.string(),
                                object : TypeToken<BaseResponse<LoginBean>>() {}.type
                            )
                        if (login.code.equals("40001")) {
                            //存放Cookie
                            val cookies = response.headers("Set-Cookie")
                            val cookie = cookies[0]
                            val authenticationNormalLogin = cookie.substring(
                                cookie.indexOf("AUTHENTICATION_NORMAL_LOGIN="),
                                cookie.indexOf(";")
                            ).replace("AUTHENTICATION_NORMAL_LOGIN=", "")
                            cookieMap["AUTHENTICATION_NORMAL_LOGIN"] = authenticationNormalLogin
                            onGetGradesStatus.onLoading("验证登录信息成功")
                            mPassword = password
                            //登录成功-获取主页
                            getMainHtml(ticket)
                        } else {
                            onGetGradesStatus.error(login.message)
                        }
                    }
                })

            } catch (e: Exception) {
                e.printStackTrace()
                onGetGradesStatus.error(e.message.toString())
            }
        }
    }

    /**
     * 获取主页的HTML
     */
    private fun getMainHtml(ticket: String) {
        GlobalScope.launch {
            try {
                onGetGradesStatus.onLoading("正在获取成绩验证码")
                val clientCategory = BuildConfig.CLIENT_CATEGORY
                val mainHtml = ApiService.apiService.getMainHtml(ticket, clientCategory)
                if (mainHtml.code.equals("40001")) {
                    //获取主页 html 成功
                    val url = mainHtml.content.redirectUrl
                    val newTicket = url.substring(url.indexOf("ticket="), url.indexOf("&"))
                        .replace("ticket=", "")
//                    Logger.d("通过主页拿到的 ticket $newTicket")
                    onGetGradesStatus.onLoading("获取成绩验证码成功")
                    getToken(newTicket);
                } else {
                    onGetGradesStatus.error("获取成绩验证码失败")
                }
            } catch (e: Exception) {
                onGetGradesStatus.error("获取成绩验证码错误")
            }
        }
    }

    /**
     * 获取Token
     */
    private fun getToken(newTicket: String) {
        GlobalScope.launch {
            withContext(Dispatchers.IO) {
                onGetGradesStatus.onLoading("正在获取Token")
                try {
                    val createMap = Utils.createMap()
                    val stringBuffer = StringBuffer()
                    createMap.forEach {
                        stringBuffer.append("${it.key}=${Utils.toUrlE(it.value.toString())}&")
                    }
                    val okHttpClient = ApiService.okHttpClient
                    val request = Request.Builder()
                        .url(
                            Api.BASE_URL + "/ump/officeHallPageHome/uap/check/$newTicket?${
                                stringBuffer.substring(0, stringBuffer.length - 1)
                            }"
                        )
                        .addHeader("Cookie", map2Cookie())
                        .get()
                        .build()
                    val response = okHttpClient.newCall(request).execute()
                    val token =
                        Gson().fromJson<BaseResponse<TokenBean>>(
                            response.body()!!.string(),
                            object : TypeToken<BaseResponse<TokenBean>>() {}.type
                        )
                    if (token.code.equals("40001")) {
//                        Logger.d("通过token 获得到 $token")
                        val cookie = response.headers()["Set-Cookie"]!!.toString()
                        cookieMap["ump_token_uap-web-key"] = token.content.token
                        cookieMap["_yh_office_ticket"] = newTicket
//                        Logger.d(spUtils.LOGIN_COOKIE)
                        onGetGradesStatus.onLoading("获取Token成功")
                        //获取用户信息
//                        getUserInfo(token.content.token)
                        //获取成绩信息
                        getClassAppTicket(token.content.token)
                    } else {
                        onGetGradesStatus.error("获取token 失败 " + token.message)
                    }
                } catch (e: Exception) {
                    onGetGradesStatus.error("获取Token错误")
                    e.printStackTrace()
                }
            }
        }
    }

    private fun getClassAppTicket(token: String) {
        GlobalScope.launch {
            try {
                onGetGradesStatus.onLoading("获取成绩应用认证中")
                val map = Utils.createMap()
                map.put("applicationCode", "DIxh30290")
                val loginCookie = map2Cookie()
                val classAppTicket =
                    ApiService.apiService.getClassAppTicket(token, loginCookie, map)
                if (classAppTicket.code.equals("40001")) {
                    //获取主页 html 成功
                    onGetGradesStatus.onLoading("成绩应用认证成功")
                    yhiotGrades(classAppTicket.content.redirectUrl)
                } else {
                    onGetGradesStatus.error("获取成绩应用认证失败 " + classAppTicket.message)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                onGetGradesStatus.error("获取成绩应用认证错误 ")
            }
        }
    }


    private fun yhiotGrades(redirectUrl: String) {
        GlobalScope.launch {
            withContext(Dispatchers.IO) {
                var response: Connection.Response? = null
                try {
                    onGetGradesStatus.onLoading("正在验证成绩")
                    response = Jsoup.connect(redirectUrl)
                        .timeout(20000)
                        .header("cookie", map2Cookie())
                        .method(Connection.Method.POST)
                        .execute()
                    val html = response.parse().html().replace(" ", "").replace("\n", "")
                    val data = html.substring(html.indexOf("vardata={"), html.indexOf("};") + 1)
                    val cookie = data.substring(data.indexOf("'cookie':"), data.indexOf("\","))
                        .replace("'cookie':\"", "")
                    val url = html.substring(html.indexOf("'uri':"), html.indexOf("\",}"))
                        .replace("'uri':\"", "")
                    cookieMap["muyun_sign_javascript"] = cookie
                    yhiotGrades(Utils.toUrlD(url))
                } catch (e: StringIndexOutOfBoundsException) {
                    onGetGradesStatus.onLoading("成绩验证成功")
                    response!!.cookies().forEach {
                        cookieMap[it.key] = it.value
                    }
                    getGradesHtml(
                        "http://jw.cqvie.edu.cn:8081/jwglxt/cjcx/cjcx_cxXsgrcj.html?doType=query&gnmkdm=N305005&su=${mAccount}",
                        response.headers()
                    )
                } catch (e: Exception) {
                    e.printStackTrace()
                    onGetGradesStatus.error("成绩验证失败")
                }
            }
        }
    }


    private fun getGradesHtml(url: String, headers: MutableMap<String, String>) {
        GlobalScope.launch {
            withContext(Dispatchers.IO) {
                var document: Document? = null
                try {
                    onGetGradesStatus.onLoading("正在获取成绩信息")
                    val response = Jsoup.connect(url.replace("https", "http"))
                        .header("Cookie", "muyun_sign_cookie=${cookieMap["muyun_sign_cookie"]}")
                        .header("cookie", map2Cookie())
                        .header("Host", "jw.cqvie.edu.cn:8081")
                        .header("x-requested-with", "XMLHttpRequest")
                        .requestBody(mCurrentSemester)
                        .header("Connection", "keep-alive")
                        .header("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
                        .header(
                            "Accept",
                            "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7"
                        )
                        .header(
                            "User-Agent",
                            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36"
                        )
                        .ignoreContentType(true)
                        .method(Connection.Method.POST)
                        .execute()
                    document = response.parse()
                    val html = document.html().replace(" ", "").replace("\n", "")
                    val data = html.substring(html.indexOf("vardata={"), html.indexOf("};") + 1)
                    val cookie = data.substring(data.indexOf("'cookie':"), data.indexOf("\","))
                        .replace("'cookie':\"", "")
                    val newUrl = html.substring(html.indexOf("'uri':"), html.indexOf("\",}"))
                        .replace("'uri':\"", "")
                    response.cookies().forEach {
                        cookieMap[it.key] = it.value
                    }
                    cookieMap["muyun_sign_javascript"] = cookie
                    getGradesHtml(Utils.toUrlD(newUrl), response.headers())
                } catch (e: StringIndexOutOfBoundsException) {
                    val content = document!!.body().html().toString()
                    val gradesBean = Gson().fromJson(content, GradesBean::class.java)
                    //写入成绩信息到文件
                    //通知更新
                    onGetGradesStatus.success(gradesBean)
                } catch (e: Exception) {
                    e.printStackTrace()
                    onGetGradesStatus.error("获取成绩信息错误")
                }
            }
        }
    }

    private fun map2Cookie(): String {
        var cookies = ""
        cookieMap.forEach {
            cookies += it.key + "=" + it.value + ";"
        }
        return cookies
    }
}