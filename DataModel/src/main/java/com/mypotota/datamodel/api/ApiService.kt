package com.mypotota.datamodel.api

import com.example.coursetable_compose.api.Api
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object ApiService {
    val okHttpClient = OkHttpClient().newBuilder().apply {

        followRedirects(false)//阻止重定向-方式出现错误：Too many follow-up requests: 21
        followSslRedirects(false)

        connectTimeout(60, TimeUnit.SECONDS)
        readTimeout(60, TimeUnit.SECONDS)
        writeTimeout(60, TimeUnit.SECONDS)
        retryOnConnectionFailure(true)//错误重连

//        val manager = SSLSocketClientUtil.x509TrustManager
//        sslSocketFactory(SSLSocketClientUtil.getSocketFactory(manager)!!, manager)// 忽略校验
//        hostnameVerifier(SSLSocketClientUtil.hostnameVerifier)//忽略校验
//        addInterceptor(ReceivedCookiesInterceptor())
    }.build()


//    class ReceivedCookiesInterceptor : Interceptor {
//        @Throws(IOException::class)
//        override fun intercept(chain: Interceptor.Chain): Response {
//            val originalResponse: Response = chain.proceed(chain.request())
//            if (originalResponse.headers("Set-Cookie").isNotEmpty()) {
//                //解析Cookie
//                val set=MutableSet<String>
//                originalResponse.headers("Set-Cookie").forEach {
//
//                }
//                Logger.d(.toString())
////                SPUtils.COOKIE=
//            }
//            return originalResponse
//        }
//    }

    //第二步
    //发送拦截器  添加Cookie到请求头
//    class AddCookiesInterceptor : Interceptor {
//        @Throws(IOException::class)
//        override fun intercept(chain: Chain): Response {
//            val builder: Request.Builder = chain.request().newBuilder()
//            //添加Cookie
//            if (!TextUtils.isEmpty(NetClient.COOKIE)) {
//                builder.addHeader("Cookie", NetClient.COOKIE)
//            }
//            return chain.proceed(builder.build())
//        }
//    }


    private val retrofit = Retrofit.Builder()
        .baseUrl(Api.BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build();

    val apiService: Api by lazy {
        retrofit.create(Api::class.java)
    }

}


