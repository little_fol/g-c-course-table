package com.mypotota.datamodel.api

import com.mypotota.datamodel.utils.Utils
import java.util.*


object LoginTicket {
    fun createLoginTicket(account: String): TreeMap<String, Any> {
        val map: MutableMap<String, Any> = Utils.createMap()
        map["accountNumber"] = account
        map["loginWay"] = "ACCOUNT"

        return Utils.builderRequestParm(map, "/ump/common/login/getLoginTicket")
    }

    fun createScanCodeLoginTicket(): TreeMap<String, Any> {
        val map: MutableMap<String, Any> = Utils.createMap()
        map["accountNumber"] = 0
        map["loginWay"] = "SCAN_CODE"

        return Utils.builderRequestParm(map, "/ump/common/login/getLoginTicket")
    }
}
