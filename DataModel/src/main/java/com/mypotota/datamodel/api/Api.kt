package com.example.coursetable_compose.api

import com.mypotota.datamodel.bean.BaseResponse
import com.mypotota.datamodel.bean.ClassAppTicket
import com.mypotota.datamodel.bean.LoginBean
import com.mypotota.datamodel.bean.LoginTicketBean
import com.mypotota.datamodel.bean.MainHtmlBean
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap
import java.util.TreeMap

interface Api {
    companion object{
        const val BASE_URL="https://ai.cqvie.edu.cn"
    }

    @POST("/ump/common/login/getLoginTicket")
    suspend fun getLoginTicket(@QueryMap map: TreeMap<String, Any>?): LoginTicketBean

    @POST("/ump/mobile/login/account/checkAccount/{loginTicket}")
    suspend fun login(@Path("loginTicket") loginTicket: String,@QueryMap loginMap: TreeMap<String, Any>?): BaseResponse<LoginBean>


    @POST("/ump/pc/login/uap/sso/{ticket}")
    suspend fun getMainHtml(@Path("ticket") ticket: String,@Query("clientCategory") clientCategory: String): BaseResponse<MainHtmlBean>


    @GET("/ump/officeHall/getApplicationUrl")
    suspend fun getClassAppTicket(
        @Header("token") token: String,
        @Header("Cookie") loginCookie: String?,
        @QueryMap map: TreeMap<String, Any>?
    ): BaseResponse<ClassAppTicket>

}