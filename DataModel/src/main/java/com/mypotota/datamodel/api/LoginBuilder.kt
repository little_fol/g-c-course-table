package com.example.coursetable_compose.api

import com.mypotota.datamodel.utils.Utils.builderRequestParm
import com.mypotota.datamodel.utils.Utils.createMap
import java.util.*


object LoginBuilder {
    fun getLogin(password: String): TreeMap<String, Any> {
        val map: MutableMap<String, Any> = createMap()
        map["password"] = password
        return builderRequestParm(map, "/ump/mobile/login/account/checkAccount")
    }
}
