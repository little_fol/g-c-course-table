package com.common.utils

import java.security.MessageDigest

object SignUtils {
    private val a = charArrayOf(
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        'A', 'B', 'C', 'D', 'E', 'F'
    )

    fun a(paramString: String): String? {
        try {
            var arrayOfByte = paramString.toByteArray()
            val messageDigest = MessageDigest.getInstance("MD5")
            messageDigest.update(arrayOfByte)
            arrayOfByte = messageDigest.digest()
            val i = arrayOfByte.size
            val arrayOfChar = CharArray(i * 2)
            var b: Byte = 0
            var j = 0
            while (b < i) {
                val b1 = arrayOfByte[b.toInt()]
                val k = j + 1
                val arrayOfChar1 = a
                arrayOfChar[j] = arrayOfChar1[b1.toInt() ushr 4 and 0xF]
                j = k + 1
                arrayOfChar[k] = arrayOfChar1[b1.toInt() and 0xF]
                b++
            }
            return String(arrayOfChar)
        } catch (exception: Exception) {
            exception.printStackTrace()
            return null
        }
    }
}
