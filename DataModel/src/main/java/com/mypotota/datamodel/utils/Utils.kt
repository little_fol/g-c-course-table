package com.mypotota.datamodel.utils

import com.common.utils.SignUtils
import com.example.coursetable_compose.utils.CipherE
import com.example.coursetable_compose.utils.Key
import com.google.gson.Gson
import java.net.URLDecoder
import java.net.URLEncoder
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.pow


object Utils {
    private const val TAG = "Utils"


    val moment: String
        get() {
            val date = Date()
            val pattern = "HH"
            val locale = Locale.getDefault()
            val simpleDateFormat = SimpleDateFormat(pattern, locale)
            val time = simpleDateFormat.format(date)
            val hour = time.toInt()
            if (hour in 0..6) {
                return "凌晨啦，早点休息吧"
            }
            if (hour in 7..12) {
                return "上午啦，又是元气满满的一天"
            }
            if (hour == 13) {
                return "中午咯，吃午饭了吗"
            }
            if (hour in 14..18) {
                return "下午咯，一天快结束啦"
            }
            if (hour in 19..24) {
                return "晚上了，美好的一天又结束啦"
            }
            return "现在时什么时候"
        }

    fun toUrlE(paramString: String?): String {
        if (paramString == null || paramString == "") {
            return ""
        }
        try {
            var str = String(paramString.toByteArray(), charset("UTF-8"))
            str = URLEncoder.encode(str, "UTF-8")
            return str
        } catch (localException: Exception) {
        }
        return ""
    }

    fun toUrlD(paramString: String?): String {
        if (paramString == null || paramString == "") {
            return ""
        }
        try {
            var str = String(paramString.toByteArray(), charset("UTF-8"))
            str = URLDecoder.decode(str, "UTF-8")
            return str
        } catch (localException: Exception) {
        }
        return ""
    }

    fun createMap(): TreeMap<String, Any> {
        val hashMap = TreeMap<String, Any>()
        hashMap["clientCategory"] = BuildConfig.CLIENT_CATEGORY
        hashMap["universityId"] = BuildConfig.UNIVERSITY_ID
        hashMap["appKey"] = BuildConfig.APP_KEY
        hashMap["equipmentName"] = BuildConfig.EQUIPMEN_NAME
        hashMap["appCode"] = "officeHallApplicationCode"
        hashMap["equipmentId"] = "Mac+OS+32%E4%BD%8D"
        hashMap["equipmentVersion"] = "123.0.0.0"
        hashMap["timestamp"] = System.currentTimeMillis().toString() + ""
        hashMap["nonce"] = random(13).toString() + ""
        return hashMap
    }


    fun random(paramInt: Int): Long {
        if (paramInt >= 1) {
            val l = System.currentTimeMillis()
            val d1 = Math.random()
            val d2 = (paramInt - 1).toDouble()
            return l + (d1 * 9.0 * 10.0.pow(d2)).toLong() + 10.0.pow(d2).toLong()
        }
        throw IllegalArgumentException("")
    }


    /**
     * 构建 secretParam map 等数据
     * @param paramMap
     * @param paramString
     * @return
     */
    fun builderRequestParm(paramMap: Map<String, Any>, paramString: String?): TreeMap<String, Any> {
        val treeMap = TreeMap<String, Any>()
        try {
            val gson = Gson()
            treeMap["secretParam"] = toUrlE(gson.toJson(a(gson.toJson(paramMap), Key.c, Key.d)))
            treeMap["clientCategory"] = BuildConfig.CLIENT_CATEGORY
            treeMap["universityId"] = "102574"
            b(treeMap, paramString, BuildConfig.APP_KEY, BuildConfig.APP_KEY_NUMBER)
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
        return treeMap
    }

    /**
     * 拼接数据
     * @param paramTreeMap
     * @param paramString1
     * @param paramString2
     * @param paramString3
     */
    fun b(
        paramTreeMap: TreeMap<String, Any>,
        paramString1: String?,
        paramString2: String,
        paramString3: String?
    ) {
        paramTreeMap["appKey"] = paramString2
        paramTreeMap["timestamp"] = System.currentTimeMillis().toString()
        paramTreeMap["nonce"] = random(13)
        val stringBuilder = StringBuilder()
        stringBuilder.append(paramString1)
        stringBuilder.append("?")
        //        if (Build.VERSION.SDK_INT >= 24) {
//            paramTreeMap.forEach(new H(stringBuilder));
//        } else {
        for ((key, value) in paramTreeMap) {
            stringBuilder.append(key)
            stringBuilder.append("=")
            stringBuilder.append(value)
            stringBuilder.append("&")
        }
        //        }
        stringBuilder.append("appSecret")
        stringBuilder.append("=")
        stringBuilder.append(paramString3)
        paramTreeMap["sign"] = SignUtils.a(stringBuilder.toString())!!
    }

    /**
     * 分段机密
     * @param paramString1
     * @param paramString2
     * @param parame
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    fun a(paramString1: String, paramString2: String, parame: CipherE): List<String> {
        val arrayList = ArrayList<String>()
        if (paramString1.length >= 60) for (b in 0 until paramString1.length / 60) {
            paramString1.length
            val i = b * 60
            arrayList.add(parame.c(paramString2, paramString1.substring(i, i + 60)))
        }
        arrayList.add(parame.c(paramString2, paramString1.substring(paramString1.length / 60 * 60)))
        return arrayList
    }

}
