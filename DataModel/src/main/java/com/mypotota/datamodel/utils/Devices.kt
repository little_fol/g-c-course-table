package com.example.coursetable_compose.utils

import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.Locale
import java.util.Scanner

object Devices {
    @get:Throws(Exception::class)
    val cpuId: String
        /**
         * 获取当前系统CPU序列，可区分linux系统和windows系统
         */
        get() {
            val cpuId: String?
            // 获取当前操作系统名称
            var os = System.getProperty("os.name")
            os = os.uppercase(Locale.getDefault())
            println(os)

            // linux系统用Runtime.getRuntime().exec()执行 dmidecode -t processor 查询cpu序列
            // windows系统用 wmic cpu get ProcessorId 查看cpu序列
            cpuId = if ("LINUX" == os || os.contains("MAC")) {
                getLinuxCpuId("dmidecode -t processor | grep 'ID'", "ID", ":")
            } else {
                windowsCpuId
            }
            return cpuId!!.uppercase(Locale.getDefault()).replace(" ", "")
        }

    /**
     * 获取linux系统CPU序列
     */
    @Throws(Exception::class)
    fun getLinuxCpuId(cmd: String?, record: String?, symbol: String): String? {
        val execResult = executeLinuxCmd(cmd)
        val infos = execResult.split("\n".toRegex()).dropLastWhile { it.isEmpty() }
            .toTypedArray()
        for (info in infos) {
            val info1 = info.trim { it <= ' ' }
            if (info1.indexOf(record!!) != -1) {
                info1.replace(" ", "")
                val sn = info1.split(symbol.toRegex()).dropLastWhile { it.isEmpty() }
                    .toTypedArray()
                return sn[1]
            }
        }
        return null
    }

    @Throws(Exception::class)
    fun executeLinuxCmd(cmd: String?): String {
        val run = Runtime.getRuntime()
        val process: Process
        process = run.exec(cmd)
        val `in` = process.inputStream
        val bs = BufferedReader(InputStreamReader(`in`))
        val out = StringBuffer()
        val b = ByteArray(8192)
        var n: Int
        while (`in`.read(b).also { n = it } != -1) {
            out.append(String(b, 0, n))
        }
        `in`.close()
        process.destroy()
        return out.toString()
    }

    @get:Throws(Exception::class)
    val windowsCpuId: String
        /**
         * 获取windows系统CPU序列
         */
        get() {
            val process = Runtime.getRuntime()
                .exec(arrayOf("wmic", "cpu", "get", "ProcessorId"))
            process.outputStream.close()
            val sc = Scanner(process.inputStream)
            sc.next()
            return sc.next()
        }
}
