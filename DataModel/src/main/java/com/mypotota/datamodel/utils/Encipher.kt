package com.example.coursetable_compose.utils

object Encipher {
    fun a(paramString: String, paramInt: Int): ByteArray {
        return b(paramString.toByteArray(), paramInt)
    }

    fun b(paramArrayOfbyte: ByteArray, paramInt: Int): ByteArray {
        return c(paramArrayOfbyte, 0, paramArrayOfbyte.size, paramInt)
    }

    fun c(paramArrayOfbyte: ByteArray, paramInt1: Int, paramInt2: Int, paramInt3: Int): ByteArray {
        var paramArrayOfbyte = paramArrayOfbyte
        var paramInt1 = paramInt1
        val b = B(paramInt3, ByteArray(paramInt2 * 3 / 4))
        if (b.a(paramArrayOfbyte, paramInt1, paramInt2, true)) {
            paramInt1 = b.b
            paramArrayOfbyte = b.a
            if (paramInt1 == paramArrayOfbyte.size) return paramArrayOfbyte
            val arrayOfByte = ByteArray(paramInt1)
            System.arraycopy(paramArrayOfbyte, 0, arrayOfByte, 0, paramInt1)
            return arrayOfByte
        }
        throw IllegalArgumentException("bad base-64")
    }

    fun d(paramArrayOfbyte: ByteArray, paramInt: Int): ByteArray {
        val e = e(paramArrayOfbyte, 0, paramArrayOfbyte.size, paramInt)
        return e
    }

    fun e(paramArrayOfbyte: ByteArray, paramInt1: Int, paramInt2: Int, paramInt3: Int): ByteArray {
        var paramInt3 = paramInt3
        val c = c(paramInt3, null)
        var i = paramInt2 / 3 * 4
        val bool = c.f
        val b: Byte = 2
        if (bool) {
            paramInt3 = i
            if (paramInt2 % 3 > 0) paramInt3 = i + 4
        } else {
            paramInt3 = paramInt2 % 3
            paramInt3 = if (paramInt3 != 1) {
                if (paramInt3 != 2) {
                    i
                } else {
                    i + 3
                }
            } else {
                i + 2
            }
        }
        i = paramInt3
        if (c.g) {
            i = paramInt3
            if (paramInt2 > 0) {
                val j = (paramInt2 - 1) / 57
                i = if (c.h) {
                    b.toInt()
                } else {
                    1
                }
                i = paramInt3 + (j + 1) * i
            }
        }
        c.a = ByteArray(i)
        c.a(paramArrayOfbyte, paramInt1, paramInt2, true)
        return c.a
    }

    internal abstract class a {
        lateinit var a: ByteArray

        var b: Int = 0
    }

    internal class B(param1Int: Int, param1ArrayOfbyte: ByteArray?) : a() {
        private var c: Int

        private var d: Int

        private val e: IntArray

        init {
            this.a = param1ArrayOfbyte!!
            val arrayOfInt = if ((param1Int and 0x8) == 0) {
                f
            } else {
                g
            }
            this.e = arrayOfInt
            this.c = 0
            this.d = 0
        }

        fun a(bArr: ByteArray, i: Int, i2: Int, z: Boolean): Boolean {
            val i3 = this.c
            if (i3 == 6) {
                return false
            }
            val i4 = i2 + i
            val i5 = this.d
            val bArr2 = this.a
            val iArr = this.e
            var i6 = i5
            var i7 = 0
            var i8 = i3
            var i9 = i
            while (i9 < i4) {
                if (i8 == 0) {
                    while (true) {
                        val i10 = i9 + 4
                        if (i10 > i4 || (((iArr[bArr[i9].toInt() and 255] shl 18) or (iArr[bArr[i9 + 1].toInt() and 255] shl 12) or (iArr[bArr[i9 + 2].toInt() and 255] shl 6) or iArr[bArr[i9 + 3].toInt() and 255]).also {
                                i6 = it
                            }) < 0) {
                            break
                        }
                        bArr2[i7 + 2] = i6.toByte()
                        bArr2[i7 + 1] = (i6 shr 8).toByte()
                        bArr2[i7] = (i6 shr 16).toByte()
                        i7 += 3
                        i9 = i10
                    }
                    if (i9 >= i4) {
                        break
                    }
                }
                val i11 = i9 + 1
                var i12 = iArr[bArr[i9].toInt() and 255]
                if (i8 != 0) {
                    if (i8 == 1) {
                        if (i12 < 0) {
                            if (i12 != -1) {
                                this.c = 6
                                return false
                            }
                        }
                        i12 = i12 or (i6 shl 6)
                    } else if (i8 == 2) {
                        if (i12 < 0) {
                            if (i12 == -2) {
                                bArr2[i7] = (i6 shr 4).toByte()
                                i7++
                                i8 = 4
                            } else if (i12 != -1) {
                                this.c = 6
                                return false
                            }
                        }
                        i12 = i12 or (i6 shl 6)
                    } else if (i8 != 3) {
                        if (i8 != 4) {
                            if (i8 == 5 && i12 != -1) {
                                this.c = 6
                                return false
                            }
                        } else if (i12 == -2) {
                            i8++
                        } else if (i12 != -1) {
                            this.c = 6
                            return false
                        }
                    } else if (i12 >= 0) {
                        val i13 = i12 or (i6 shl 6)
                        bArr2[i7 + 2] = i13.toByte()
                        bArr2[i7 + 1] = (i13 shr 8).toByte()
                        bArr2[i7] = (i13 shr 16).toByte()
                        i7 += 3
                        i6 = i13
                        i8 = 0
                    } else if (i12 == -2) {
                        bArr2[i7 + 1] = (i6 shr 2).toByte()
                        bArr2[i7] = (i6 shr 10).toByte()
                        i7 += 2
                        i8 = 5
                    } else if (i12 != -1) {
                        this.c = 6
                        return false
                    }
                    i8++
                    i6 = i12
                } else {
                    if (i12 < 0) {
                        if (i12 != -1) {
                            this.c = 6
                            return false
                        }
                    }
                    i8++
                    i6 = i12
                }
                i9 = i11
            }
            if (!z) {
                this.c = i8
                this.d = i6
                this.b = i7
                return true
            } else if (i8 != 1) {
                if (i8 == 2) {
                    bArr2[i7] = (i6 shr 4).toByte()
                    i7++
                } else if (i8 == 3) {
                    val i14 = i7 + 1
                    bArr2[i7] = (i6 shr 10).toByte()
                    i7 = i14 + 1
                    bArr2[i14] = (i6 shr 2).toByte()
                } else if (i8 == 4) {
                    this.c = 6
                    return false
                }
                this.c = i8
                this.b = i7
                return true
            } else {
                this.c = 6
                return false
            }
        }

        companion object {
            private val f = intArrayOf(
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, 62, -1, -1, -1, 63, 52, 53,
                54, 55, 56, 57, 58, 59, 60, 61, -1, -1,
                -1, -2, -1, -1, -1, 0, 1, 2, 3, 4,
                5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
                25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
                29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
                39, 40, 41, 42, 43, 44, 45, 46, 47, 48,
                49, 50, 51, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1
            )

            private val g = intArrayOf(
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, 62, -1, -1, 52, 53,
                54, 55, 56, 57, 58, 59, 60, 61, -1, -1,
                -1, -2, -1, -1, -1, 0, 1, 2, 3, 4,
                5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
                25, -1, -1, -1, -1, 63, -1, 26, 27, 28,
                29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
                39, 40, 41, 42, 43, 44, 45, 46, 47, 48,
                49, 50, 51, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1
            )
        }
    }

    internal class c(param1Int: Int, param1ArrayOfbyte: ByteArray?) : a() {
        private val c: ByteArray

        var d: Int

        private var e: Int

        val f: Boolean

        val g: Boolean

        val h: Boolean

        private val i: ByteArray?

        init {
            var param1Int = param1Int
            var param1ArrayOfbyte = param1ArrayOfbyte
            if (param1ArrayOfbyte != null) {
                this.a = param1ArrayOfbyte
            }
            var bool1 = true
            var bool2 = if ((param1Int and 0x1) == 0) {
                true
            } else {
                false
            }
            this.f = bool2
            bool2 = if ((param1Int and 0x2) == 0) {
                true
            } else {
                false
            }
            this.g = bool2
            if ((param1Int and 0x4) == 0) bool1 = false
            this.h = bool1
            param1ArrayOfbyte = if ((param1Int and 0x8) == 0) {
                j
            } else {
                k
            }
            this.i = param1ArrayOfbyte
            this.c = ByteArray(2)
            this.d = 0
            param1Int = if (bool2) {
                19
            } else {
                -1
            }
            this.e = param1Int
        }

        fun a(var1: ByteArray, var2: Int, var3: Int, var4: Boolean): Boolean {
            var var1 = var1
            var var2 = var2
            var var3 = var3
            var var5: Int
            var var6: Int
            var var7: Int
            var var8: Byte
            var var9: Int
            var var10: Int
            var var11: ByteArray
            var var12: ByteArray?
            var var14: Byte
            run{
                var12 = this.i
                var11 = super.a
                var5 = this.e
                var9 = var3 + var2
                var3 = this.d
                var8 = 0
                var7 = 0
                if (var3 != 1) {
                    if (var3 == 2) {
                        var3 = var2 + 1
                        if (var3 <= var9) {
                            val var13 = this.c
                            var10 = var13[0].toInt()
                            var6 = var13[1].toInt()
                            var2 =
                                (var6 and 255) shl 8 or ((var10 and 255) shl 16) or (var1[var2].toInt() and 255)
                            this.d = 0
                            return@run
                        }
                    }
                } else if (var2 + 2 <= var9) {
                    var6 = c[0].toInt()
                    var10 = var2 + 1
                    var14 = var1[var2]
                    var3 = var10 + 1
                    var2 =
                        var1[var10].toInt() and 255 or ((var6 and 255) shl 16) or ((var14.toInt() and 255) shl 8)
                    this.d = 0
                    return@run
                }

                var6 = -1
                var3 = var2
                var2 = var6
            }

            if (var2 != -1) {
                var11[0] = var12!![var2 shr 18 and 63]
                var11[1] = var12!![var2 shr 12 and 63]
                var11[2] = var12!![var2 shr 6 and 63]
                var11[3] = var12!![var2 and 63]
                --var5
                if (var5 == 0) {
                    if (this.h) {
                        var14 = 5
                        var11[4] = 13
                    } else {
                        var14 = 4
                    }

                    var5 = var14 + 1
                    var11[var14.toInt()] = 10
                    var2 = var5
                    var5 = 19
                } else {
                    var2 = 4
                }
            } else {
                var2 = 0
            }

            while (true) {
                var6 = var3 + 3
                if (var6 > var9) {
                    if (var4) {
                        run{
                            var10 = this.d
                            val var16: Byte
                            if (var3 - var10 == var9 - 1) {
                                val var15: Byte
                                if (var10 > 0) {
                                    var16 = c[0]
                                    var15 = 1
                                } else {
                                    var16 = var1[var3]
                                    var15 = var7.toByte()
                                }

                                var6 = (var16.toInt() and 255) shl 4
                                this.d = var10 - var15
                                var7 = var2 + 1
                                var11[var2] = var12!![var6 shr 6 and 63]
                                var3 = var7 + 1
                                var11[var7] = var12!![var6 and 63]
                                var2 = var3
                                if (this.f) {
                                    var6 = var3 + 1
                                    var11[var3] = 61
                                    var2 = var6 + 1
                                    var11[var6] = 61
                                }

                                var6 = var2
                                if (!this.g) {
                                    return@run
                                }

                                var3 = var2
                                if (this.h) {
                                    var11[var2] = 13
                                    var3 = var2 + 1
                                }

                                var2 = var3 + 1
                                var11[var3] = 10
                            } else if (var3 - var10 == var9 - 2) {
                                val var17: Byte
                                if (var10 > 1) {
                                    var17 = c[0]
                                    var16 = 1
                                } else {
                                    var17 = var1[var3]
                                    ++var3
                                    var16 = var8
                                }

                                val var19: Byte
                                if (var10 > 0) {
                                    var1 = this.c
                                    var3 = var16 + 1
                                    var19 = var1[var16.toInt()]
                                } else {
                                    val var18 = var1[var3]
                                    var3 = var16.toInt()
                                    var19 = var18
                                }

                                var6 =
                                    (var17.toInt() and 255) shl 10 or ((var19.toInt() and 255) shl 2)
                                this.d = var10 - var3
                                var3 = var2 + 1
                                var11[var2] = var12!![var6 shr 12 and 63]
                                var2 = var3 + 1
                                var11[var3] = var12!![var6 shr 6 and 63]
                                var3 = var2 + 1
                                var11[var2] = var12!![var6 and 63]
                                var2 = var3
                                if (this.f) {
                                    var11[var3] = 61
                                    var2 = var3 + 1
                                }

                                if (!this.g) {
                                    var6 = var2
                                    return@run
                                }

                                var3 = var2
                                if (this.h) {
                                    var11[var2] = 13
                                    var3 = var2 + 1
                                }

                                var2 = var3 + 1
                                var11[var3] = 10
                            } else {
                                var6 = var2
                                if (!this.g) {
                                    return@run
                                }

                                var6 = var2
                                if (var2 <= 0) {
                                    return@run
                                }

                                var6 = var2
                                if (var5 == 19) {
                                    return@run
                                }

                                var3 = var2
                                if (this.h) {
                                    var11[var2] = 13
                                    var3 = var2 + 1
                                }

                                var2 = var3 + 1
                                var11[var3] = 10
                            }
                            var6 = var2
                        }
                    } else if (var3 == var9 - 1) {
                        var11 = this.c
                        var6 = d++
                        var11[var6] = var1[var3]
                        var6 = var2
                    } else {
                        var6 = var2
                        if (var3 == var9 - 2) {
                            var11 = this.c
                            var7 = this.d
                            var6 = var7 + 1
                            this.d = var6
                            var11[var7] = var1[var3]
                            this.d = var6 + 1
                            var11[var6] = var1[var3 + 1]
                            var6 = var2
                        }
                    }

                    super.b = var6
                    this.e = var5
                    return true
                }

                val var20 = var1[var3]
                var3 =
                    (var1[var3 + 1].toInt() and 255) shl 8 or ((var20.toInt() and 255) shl 16) or (var1[var3 + 2].toInt() and 255)
                var11[var2] = var12!![var3 shr 18 and 63]
                var11[var2 + 1] = var12!![var3 shr 12 and 63]
                var11[var2 + 2] = var12!![var3 shr 6 and 63]
                var11[var2 + 3] = var12!![var3 and 63]
                var2 += 4
                --var5
                if (var5 == 0) {
                    var3 = var2
                    if (this.h) {
                        var11[var2] = 13
                        var3 = var2 + 1
                    }

                    var11[var3] = 10
                    var2 = var3 + 1
                    var3 = var6
                    var5 = 19
                } else {
                    var3 = var6
                }
            }
        }

        companion object {
            private val j = byteArrayOf(
                65, 66, 67, 68, 69, 70, 71, 72, 73, 74,
                75, 76, 77, 78, 79, 80, 81, 82, 83, 84,
                85, 86, 87, 88, 89, 90, 97, 98, 99, 100,
                101, 102, 103, 104, 105, 106, 107, 108, 109, 110,
                111, 112, 113, 114, 115, 116, 117, 118, 119, 120,
                121, 122, 48, 49, 50, 51, 52, 53, 54, 55,
                56, 57, 43, 47
            )

            private val k = byteArrayOf(
                65, 66, 67, 68, 69, 70, 71, 72, 73, 74,
                75, 76, 77, 78, 79, 80, 81, 82, 83, 84,
                85, 86, 87, 88, 89, 90, 97, 98, 99, 100,
                101, 102, 103, 104, 105, 106, 107, 108, 109, 110,
                111, 112, 113, 114, 115, 116, 117, 118, 119, 120,
                121, 122, 48, 49, 50, 51, 52, 53, 54, 55,
                56, 57, 45, 95
            )
        }
    }
}
