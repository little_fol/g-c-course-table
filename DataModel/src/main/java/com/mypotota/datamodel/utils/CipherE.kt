package com.example.coursetable_compose.utils

import java.nio.charset.Charset
import java.security.InvalidKeyException
import java.security.Key
import java.security.KeyFactory
import java.security.NoSuchAlgorithmException
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.security.spec.InvalidKeySpecException
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import javax.crypto.BadPaddingException
import javax.crypto.Cipher
import javax.crypto.IllegalBlockSizeException

class CipherE(private val a: Charset, private val b: String) {
    @Throws(Exception::class)
    private fun b(paramKey: Key?, paramInt: Int, paramArrayOfbyte: ByteArray): ByteArray {
        if (paramKey != null) try {
            val cipher = Cipher.getInstance(this.b)
            cipher.init(paramInt, paramKey)
            return cipher.doFinal(paramArrayOfbyte)
        } catch (noSuchAlgorithmException: NoSuchAlgorithmException) {
            noSuchAlgorithmException.printStackTrace()
        } catch (invalidKeyException: InvalidKeyException) {
            invalidKeyException.printStackTrace()
        } catch (illegalBlockSizeException: IllegalBlockSizeException) {
            illegalBlockSizeException.printStackTrace()
        } catch (badPaddingException: BadPaddingException) {
            badPaddingException.printStackTrace()
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
        throw Exception("错误")
    }

    @Throws(Exception::class)
    private fun d(paramString: String): RSAPrivateKey {
        try {
            val arrayOfByte = Encipher.a(paramString, 0)
            val pKCS8EncodedKeySpec = PKCS8EncodedKeySpec(arrayOfByte)
            return KeyFactory.getInstance("RSA").generatePrivate(pKCS8EncodedKeySpec) as RSAPrivateKey
        } catch (noSuchAlgorithmException: NoSuchAlgorithmException) {
            noSuchAlgorithmException.printStackTrace()
        } catch (invalidKeySpecException: InvalidKeySpecException) {
            invalidKeySpecException.printStackTrace()
        } catch (nullPointerException: NullPointerException) {
            nullPointerException.printStackTrace()
        } catch (exception: Exception) {
            throw Exception("发生错误")
        }
        throw Exception("发生错误")
    }

    @Throws(Exception::class)
    private fun e(paramString: String): RSAPublicKey {
        try {
            val arrayOfByte = Encipher.a(paramString, 0)
            val keyFactory = KeyFactory.getInstance("RSA")
            val x509EncodedKeySpec = X509EncodedKeySpec(arrayOfByte)
            return keyFactory.generatePublic(x509EncodedKeySpec) as RSAPublicKey
        } catch (noSuchAlgorithmException: NoSuchAlgorithmException) {
            noSuchAlgorithmException.printStackTrace()
        } catch (invalidKeySpecException: InvalidKeySpecException) {
            invalidKeySpecException.printStackTrace()
        } catch (nullPointerException: NullPointerException) {
            nullPointerException.printStackTrace()
        } catch (exception: Exception) {
            exception.printStackTrace()
            throw Exception("发生错误")
        }
        throw Exception("发生错误")
    }

    @Throws(Exception::class)
    fun a(paramString1: String, paramString2: String): String {
        return String(b(d(paramString1), 2, Encipher.a(paramString2, 0)), this.a)
    }

    @Throws(Exception::class)
    fun c(paramString1: String, paramString2: String): String {
//        Logger.d(A.d(b(e(paramString1), 1, paramString2.getBytes(this.a)), 0));
        return String(Encipher.d(b(e(paramString1), 1, paramString2.toByteArray(this.a)), 0))
    }
}
