package com.example.coursetable_compose.utils

import com.example.coursetable_compose.utils.CipherE
import java.nio.charset.Charset


object Key {
    var a: String =
        "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMALA8PFjJx0KI15uVlr0uO8PNuJlTW9z2ApFwb+kaNn8MmQjgeahQTav+jpJ8HlrPez4fkkPOY4xVDreEPstlAZHiTeJhacKDdSlBmRkS0W1cFexAeMpUmoKIS0r6C3HPhaeuION/578LEns4PTd7uzLk1j9qRWZg22CEpnPPqPAgMBAAECgYEAt0ILjqcWPjpOdGsC54UQEsgPXwQUgdsmaEHtunv06HUIfqBWGIVoiDBiz9SmXs5YV2utqPzerGbiTAzXkCHz+iQyUIc//cg47jHYA2I9IP+Q3+r7krjksHCT8/kn1JwzsG3dFirfcZo4wzwPj4qaF3cVUt9BHltad6OKd0NkrakCQQD0sqeC6U48rKvGwcSuRLTS3OXooAn9akgWNIwt8qsYijuXWCjsKVKySI5Twhc78DRAP+feLEBsrzOkOFKkbw9NAkEAyOnC5ctaawXwTqkcEuyBlFCOr3aTsm2oNYifPVj4RZe9taydZ3m9ybiFLo1k776igYNQ+anuTFJuc+6M/f/7SwJAa+HwVJlD8yA1w7VeX27PQkA95OfaP64CBjg4rXFV2cXoK/ukXyqA2TV/+pGR6M0oXENQmPVtmf1MiVSDqxeD6QJBAKxj76hPAUk9ckF8VythpZnGOXgSzY5XCvLMRJ5V/clw9wWwk+raid9RJW2ZQxd3QmbQbyWaCm+Qd1egQ3gaMdsCQAgxrkF2Tf2BLF/ki+qpI45v4+L6ZKiw3nDm+OKsrcZFdeLkwWE/dYbOXWcD45k8XRu97dP3amWNGJweKXtObU0="

    var b: String =
        "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBALUPuqBiS+1YbK/Rcm+cVkpQwl9SXNBTGvxhi8kWBIL9AFlouREZsF+WfG0Ii796kTiOXxaVEa0zLjFMuAn/jobRZckEhE9zJAOeSyAvU8qQGFYIKjuHR9RxHCzrecVEDWDUbALlGJxZdp7a0/hojzVZfnbfoWgm4khcrYQa76WVAgMBAAECgYEAmc0wpMia4pR4TqlF4hUVH6+WTM5z1OqjQ7vAuCGh13r+bvSMMEB4F9qG+z+FJjQBY99cWpxqFYwiMvKOar/Q2ro4ge+EdcisU0k5WXXubUpol743H/KJpnd8kCWFk9pNiLuOl7qiJlTkB5KivvX2JxDtmvRO02oFfLXgAX+y00ECQQDl/iiVb6fLJhrf9FDbzvk3qRB7eruApkGr0DYS+TDyq72GSnkHEfj5EuB23NaeChyCJJtH09euSaIHw/chnKlpAkEAyYkabsZ+CyUPquJop1UD2hFStPI+DDvniz19YTxzUcU/Fs38kTsoew82IhEm/k2rvaRkKRJLknHvm4h/PnwJTQJBALebKgz6YSrFlcjaAz8nQT+VIUpiVZPDpkOiabjF5LSmNBwkEfB6AZfd4QIjFNZ/3fhrfuddkB5cPBUU9ZKIvZkCQQCR3cPd7ZiI5HgkjN6GTkgNa4BbKwGxxSHfa8/1stUcmBEDpm9phlHUT7w0iAmbAgiNqBA+kdlU01ZDUlYWmZv5AkAhtORzWYq16KyHfM+7kgxniufX7dBcIWh4MOStl1q5OALrfsenzrf8Yzl5ss9JmmXZFuq2Am0fy9Sv6VjG7WrE"

    var c: String =
        "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDACwPDxYycdCiNeblZa9LjvDzbiZU1vc9gKRcG/pGjZ/DJkI4HmoUE2r/o6SfB5az3s+H5JDzmOMVQ63hD7LZQGR4k3iYWnCg3UpQZkZEtFtXBXsQHjKVJqCiEtK+gtxz4WnriDjf+e/CxJ7OD03e7sy5NY/akVmYNtghKZzz6jwIDAQAB"

    var d: CipherE = CipherE(Charset.forName("utf-8"), "RSA/ECB/PKCS1Padding") //
    //    public static e e = new e(Charset.forName("utf-8"), "RSA");
    //
    //    public static e f = new e(Charset.forName("utf-8"), "RSA/ECB/PKCS1Padding");
}
