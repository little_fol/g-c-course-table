package com.example.coursetable_compose.utils

import java.io.BufferedReader
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.io.InputStreamReader
import java.net.InetAddress
import java.net.NetworkInterface
import java.util.Enumeration
import java.util.Locale
import java.util.regex.Pattern

object ComputerUniqueIdentificationUtil {














    /**
     * Windows
     */
    const val WINDOWS = "windows"

    /**
     * Linux
     */
    const val LINUX = "linux"

    /**
     * Unix
     */
    const val UNIX = "unix"

    /**
     * 正则表达式
     */
    const val REGEX = "\\b\\w+:\\w+:\\w+:\\w+:\\w+:\\w+\\b"
    private val windowsMainBoardSerialNumber: String
        /**
         * 获取 Windows 主板序列号
         *
         * @return String - 计算机主板序列号
         * @author XinLau
         * @creed The only constant is change ! ! !
         * @since 2020/10/10 17:15
         */
        private get() {
            var result = ""
            try {
                val file = File.createTempFile("realhowto", ".vbs")
                file.deleteOnExit()
                val fw = FileWriter(file)
                val vbs = """Set objWMIService = GetObject("winmgmts:\\.\root\cimv2")
Set colItems = objWMIService.ExecQuery _ 
   ("Select * from Win32_BaseBoard") 
For Each objItem in colItems 
    Wscript.Echo objItem.SerialNumber 
    exit for  ' do the first cpu only! 
Next 
"""
                fw.write(vbs)
                fw.close()
                val p = Runtime.getRuntime().exec("cscript //NoLogo " + file.path)
                val input = BufferedReader(InputStreamReader(p.inputStream))
                var line: String
                while (input.readLine().also { line = it } != null) {
                    result += line
                }
                input.close()
            } catch (e: Exception) {
                System.err.println("获取 Windows 主板信息错误$e")
            }
            return result.trim { it <= ' ' }
        }
    private val linuxMainBoardSerialNumber: String?
        /**
         * 获取 Linux 主板序列号
         *
         * @return String - 计算机主板序列号
         * @author XinLau
         * @creed The only constant is change ! ! !
         * @since 2020/10/10 17:15
         */
        private get() {
            var result: String? = ""
            val maniBord_cmd = "dmidecode | grep 'Serial Number' | awk '{print $3}' | tail -1"
            val p: Process
            try {
                // 管道
                p = Runtime.getRuntime().exec(arrayOf("sh", "-c", maniBord_cmd))
                val br = BufferedReader(InputStreamReader(p.inputStream))
                var line: String?
                while (br.readLine().also { line = it } != null) {
                    result += line
                    break
                }
                br.close()
            } catch (e: IOException) {
                System.err.println("获取 Linux 主板信息错误")
            }
            return result
        }

    /**
     * 从字节获取 MAC
     *
     * @param bytes - 字节
     * @return String - MAC
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/12 8:55
     */
    private fun getMacFromBytes(bytes: ByteArray): String {
        val mac = StringBuffer()
        var currentByte: Byte
        var first = false
        for (b in bytes) {
            if (first) {
                mac.append("-")
            }
            currentByte = (b.toInt() and 240 shr 4).toByte()
            mac.append(Integer.toHexString(currentByte.toInt()))
            currentByte = (b.toInt() and 15).toByte()
            mac.append(Integer.toHexString(currentByte.toInt()))
            first = true
        }
        return mac.toString().uppercase(Locale.getDefault())
    }

    private val windowsMACAddress: String
        /**
         * 获取 Windows 网卡的 MAC 地址
         *
         * @return String - MAC 地址
         * @author XinLau
         * @creed The only constant is change ! ! !
         * @since 2020/10/10 17:15
         */
        private get() {
            var ip: InetAddress? = null
            var ni: NetworkInterface? = null
            val macList: MutableList<String> = ArrayList()
            try {
                val netInterfaces = NetworkInterface
                    .getNetworkInterfaces() as Enumeration<NetworkInterface>
                while (netInterfaces.hasMoreElements()) {
                    ni = netInterfaces.nextElement() as NetworkInterface
                    //  遍历所有 IP 特定情况，可以考虑用 ni.getName() 判断
                    val ips = ni.inetAddresses
                    while (ips.hasMoreElements()) {
                        ip = ips.nextElement() as InetAddress
                        // 非127.0.0.1
                        if (!ip!!.isLoopbackAddress && ip.hostAddress.matches("(\\d{1,3}\\.){3}\\d{1,3}".toRegex())) {
                            macList.add(getMacFromBytes(ni!!.hardwareAddress))
                        }
                    }
                }
            } catch (e: Exception) {
                System.err.println("获取 Windows MAC 错误")
            }
            return if (macList.size > 0) {
                macList[0]
            } else {
                ""
            }
        }
    private val linuxMACAddressForEth0: String?
        /**
         * 获取 Linux 网卡的 MAC 地址 （如果 Linux 下有 eth0 这个网卡）
         *
         * @return String - MAC 地址
         * @author XinLau
         * @creed The only constant is change ! ! !
         * @since 2020/10/10 17:15
         */
        private get() {
            var mac: String? = null
            var bufferedReader: BufferedReader? = null
            var process: Process? = null
            try {
                // Linux下的命令，一般取eth0作为本地主网卡
                process = Runtime.getRuntime().exec("ifconfig eth0")
                // 显示信息中包含有 MAC 地址信息
                bufferedReader = BufferedReader(InputStreamReader(process.inputStream))
                var line: String? = null
                var index = -1
                while (bufferedReader.readLine().also { line = it } != null) {
                    // 寻找标示字符串[hwaddr]
                    index = line!!.lowercase(Locale.getDefault()).indexOf("hwaddr")
                    if (index >= 0) {
                        // // 找到并取出 MAC 地址并去除2边空格
                        mac = line!!.substring(index + "hwaddr".length + 1).trim { it <= ' ' }
                        break
                    }
                }
            } catch (e: IOException) {
                System.err.println("获取 Linux MAC 信息错误")
            } finally {
                try {
                    bufferedReader?.close()
                } catch (e1: IOException) {
                    System.err.println("获取 Linux MAC 信息错误")
                }
                bufferedReader = null
                process = null
            }
            return mac
        }
    private val linuxMACAddress: String?
        /**
         * 获取 Linux 网卡的 MAC 地址
         *
         * @return String - MAC 地址
         * @author XinLau
         * @creed The only constant is change ! ! !
         * @since 2020/10/10 17:15
         */
        private get() {
            var mac: String? = null
            var bufferedReader: BufferedReader? = null
            var process: Process? = null
            try {
                // Linux下的命令 显示或设置网络设备
                process = Runtime.getRuntime().exec("ifconfig")
                // 显示信息中包含有 MAC 地址信息
                bufferedReader = BufferedReader(InputStreamReader(process.inputStream))
                var line: String? = null
                val index = -1
                while (bufferedReader.readLine().also { line = it } != null) {
                    val pat = Pattern.compile(REGEX)
                    val mat = pat.matcher(line)
                    if (mat.find()) {
                        mac = mat.group(0)
                    }
                }
            } catch (e: IOException) {
                System.err.println("获取 Linux MAC 信息错误")
            } finally {
                try {
                    bufferedReader?.close()
                } catch (e1: IOException) {
                    System.err.println("获取 Linux MAC 信息错误")
                }
                bufferedReader = null
                process = null
            }
            return mac
        }
    private val windowsProcessorIdentification: String
        /**
         * 获取 Windows 的 CPU 序列号
         *
         * @return String - CPU 序列号
         * @author XinLau
         * @creed The only constant is change ! ! !
         * @since 2020/10/10 17:15
         */
        private get() {
            var result = ""
            try {
                val file = File.createTempFile("tmp", ".vbs")
                file.deleteOnExit()
                val fw = FileWriter(file)
                val vbs = """Set objWMIService = GetObject("winmgmts:\\.\root\cimv2")
Set colItems = objWMIService.ExecQuery _ 
   ("Select * from Win32_Processor") 
For Each objItem in colItems 
    Wscript.Echo objItem.ProcessorId 
    exit for  ' do the first cpu only! 
Next 
"""
                fw.write(vbs)
                fw.close()
                val p = Runtime.getRuntime().exec("cscript //NoLogo " + file.path)
                val input = BufferedReader(InputStreamReader(p.inputStream))
                var line: String
                while (input.readLine().also { line = it } != null) {
                    result += line
                }
                input.close()
                file.delete()
            } catch (e: Exception) {
                System.err.println("获取 Windows CPU 信息错误")
            }
            return result.trim { it <= ' ' }
        }
    private val linuxProcessorIdentification: String
        /**
         * 获取 Linux 的 CPU 序列号
         *
         * @return String - CPU 序列号
         * @author XinLau
         * @creed The only constant is change ! ! !
         * @since 2020/10/10 17:15
         */
        private get() {
            var result = ""
            val CPU_ID_CMD = "dmidecode"
            var bufferedReader: BufferedReader? = null
            var p: Process? = null
            try {
                // 管道
                p = Runtime.getRuntime().exec(arrayOf("sh", "-c", CPU_ID_CMD))
                bufferedReader = BufferedReader(InputStreamReader(p.inputStream))
                var line: String? = null
                var index = -1
                while (bufferedReader.readLine().also { line = it } != null) {
                    index = line!!.lowercase(Locale.getDefault()).indexOf("uuid")
                    if (index >= 0) {
                        result = line!!.substring(index + "uuid".length + 1).trim { it <= ' ' }
                        break
                    }
                }
            } catch (e: IOException) {
                System.err.println("获取 Linux CPU 信息错误")
            }
            return result.trim { it <= ' ' }
        }
    val oSName: String
        /**
         * 获取当前计算机操作系统名称 例如:windows,Linux,Unix等.
         *
         * @return String - 计算机操作系统名称
         * @author XinLau
         * @creed The only constant is change ! ! !
         * @since 2020/10/10 17:15
         */
        get() = System.getProperty("os.name").lowercase(Locale.getDefault())
    val oSNamePrefix: String
        /**
         * 获取当前计算机操作系统名称前缀 例如:windows,Linux,Unix等.
         *
         * @return String - 计算机操作系统名称
         * @author XinLau
         * @creed The only constant is change ! ! !
         * @since 2020/10/10 17:15
         */
        get() {
            val name = oSName
            return if (name.startsWith(WINDOWS)) {
                WINDOWS
            } else if (name.startsWith(LINUX)) {
                LINUX
            } else if (name.startsWith(UNIX)) {
                UNIX
            } else {
                ""
            }
        }
    val mainBoardSerialNumber: String?
        /**
         * 获取当前计算机主板序列号
         *
         * @return String - 计算机主板序列号
         * @author XinLau
         * @creed The only constant is change ! ! !
         * @since 2020/10/10 17:15
         */
        get() = when (oSNamePrefix) {
            WINDOWS -> windowsMainBoardSerialNumber
            LINUX -> linuxMainBoardSerialNumber
            else -> ""
        }
    val mACAddress: String?
        /**
         * 获取当前计算机网卡的 MAC 地址
         *
         * @return String - 网卡的 MAC 地址
         * @author XinLau
         * @creed The only constant is change ! ! !
         * @since 2020/10/10 17:15
         */
        get() = when (oSNamePrefix) {
            WINDOWS -> windowsMACAddress
            LINUX -> {
                var macAddressForEth0 = linuxMACAddressForEth0
                macAddressForEth0 = linuxMACAddress
                macAddressForEth0
            }

            else -> ""
        }
    val cPUIdentification: String
        /**
         * 获取当前计算机的 CPU 序列号
         *
         * @return String - CPU 序列号
         * @author XinLau
         * @creed The only constant is change ! ! !
         * @since 2020/10/10 17:15
         */
        get() = when (oSNamePrefix) {
            WINDOWS -> windowsProcessorIdentification
            LINUX -> linuxProcessorIdentification
            else -> ""
        }
    val computerUniqueIdentification: ComputerUniqueIdentification
        /**
         * 获取计算机唯一标识
         *
         * @return ComputerUniqueIdentification - 计算机唯一标识
         * @author XinLau
         * @creed The only constant is change ! ! !
         * @since 2020/10/14 8:50
         */
        get() = ComputerUniqueIdentification(
            oSNamePrefix,
            mainBoardSerialNumber,
            mACAddress,
            cPUIdentification
        )
    val computerUniqueIdentificationString: String
        /**
         * 获取计算机唯一标识
         *
         * @return String - 计算机唯一标识
         * @author XinLau
         * @creed The only constant is change ! ! !
         * @since 2020/10/14 8:50
         */
        get() = computerUniqueIdentification.toString()

    /**
     * 计算机唯一标识
     */
    class ComputerUniqueIdentification(
        var namePrefix: String,
        var mainBoardSerialNumber: String?,
        var mACAddress: String?,
        var cPUIdentification: String
    ) {
        override fun toString(): String {
            return StringBuilder().append('{')
                .append("\"namePrefix=\":\"").append(namePrefix).append("\",")
                .append("\"mainBoardSerialNumber=\":\"").append(mainBoardSerialNumber).append("\",")
                .append("\"MACAddress=\":\"").append(this.mACAddress).append("\",")
                .append("\"CPUIdentification=\":\"").append(this.cPUIdentification)
                .append("\"}").toString()
        }
    }
}