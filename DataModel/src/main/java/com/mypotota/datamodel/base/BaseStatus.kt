package com.mypotota.datamodel.base

sealed class BaseStatus<out T> {
    //type=1-获取二维码成功
    data class LOADING(val msg:String="",val progress:Float=0f,val type: Int=0,val data:String=""): BaseStatus<Nothing>()
    data class ERROR(var e:String,var type:Int=0): BaseStatus<Nothing>()
    data class SUCCESS<T>(var data:T): BaseStatus<T>()
}