package com.mypotota.datamodel.bean

import java.io.Serializable

data class TokenBean(
    val openId: String,
    val refreshToken: Any,
    val token: String
): Serializable