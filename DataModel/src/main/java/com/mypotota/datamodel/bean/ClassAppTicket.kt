package com.mypotota.datamodel.bean

import java.io.Serializable

data class ClassAppTicket(
    var redirectUrl: String,
    val ticket: String,
    val visitWay: String
): Serializable
