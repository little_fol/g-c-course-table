package com.mypotota.datamodel.bean

import java.io.Serializable

data class BaseResponse<T>(
    val code: String,
    val content: T,
    val message: String,
    val otherMsg: Object
): Serializable

