package com.mypotota.datamodel.bean

import java.io.Serializable

data class MainHtmlBean(
    val redirectUrl: String
): Serializable