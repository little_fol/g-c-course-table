package com.mypotota.datamodel.bean

import java.io.Serializable

data class LoginTicketBean(
    val code: String="",
    val content: Content = Content(""),
    val message: String="",
    val otherMsg: String ="",
): Serializable
data class Content(
    val ticket: String
):Serializable