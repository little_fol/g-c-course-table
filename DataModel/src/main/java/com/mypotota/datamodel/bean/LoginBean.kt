package com.mypotota.datamodel.bean

import java.io.Serializable

data class LoginBean(
    val openId: String,
    val refreshToken: String,
    val token: String,
    val userState: String
): Serializable