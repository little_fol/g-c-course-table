plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.jetbrains.kotlin.android)
}

android {
    namespace = "com.example.coursetable_compose"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.example.coursetable_compose"
        minSdk = 26
        targetSdk = 34
        versionCode = 12
        versionName = "0.12"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
        ndk {
            // 设置支持的SO库架构
            abiFilters.add("armeabi") //, 'x86', 'armeabi-v7a', 'x86_64', 'arm64-v8a'
//            abiFilters.add("arm64-v8a") //, 'x86', 'armeabi-v7a', 'x86_64', 'arm64-v8a'
//            abiFilters.add("x86") //, 'x86', 'armeabi-v7a', 'x86_64', 'arm64-v8a'
//            abiFilters.add("x86_64") //, 'x86', 'armeabi-v7a', 'x86_64', 'arm64-v8a'
//            abiFilters.add("armeabi-v7a") //, 'x86', 'armeabi-v7a', 'x86_64', 'arm64-v8a'
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.1"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {

    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.lifecycle.runtime.ktx)
    implementation(libs.androidx.activity.compose)
    implementation(platform(libs.androidx.compose.bom))
    implementation(libs.androidx.ui)
    implementation(libs.androidx.ui.graphics)
    implementation(libs.androidx.ui.tooling.preview)
    implementation(libs.androidx.material3)
    implementation(files("libs/Baidu_Mtj_android_4.0.10.9.jar"))
    implementation(files("libs/jsoup-1.15.3.jar"))
    implementation(project(":DataModel"))

//    testImplementation(libs.junit)
//    androidTestImplementation(libs.androidx.junit)
//    androidTestImplementation(libs.androidx.espresso.core)
//    androidTestImplementation(platform(libs.androidx.compose.bom))
//    androidTestImplementation(libs.androidx.ui.test.junit4)
//    debugImplementation(libs.androidx.ui.tooling)
//    debugImplementation(libs.androidx.ui.test.manifest)


    implementation(libs.retrofit)
    implementation(libs.converter.gson)
    implementation(libs.java.security)
    implementation(libs.logger)
    implementation(libs.threeten.extra)
    implementation(libs.androidx.navigation.compose)
//    implementation ("com.jakewharton.threetenabp:threetenabp:1.4.7")

    implementation("com.taptap:lc-storage-android:8.2.24")
    implementation("io.reactivex.rxjava2:rxandroid:2.1.1")
//    runtimeOnly("moe.tlaster:precompose:1.6.0")
//    implementation("com.github.oshi:oshi-core:6.6.0")
    implementation("com.google.android.material:material:1.6.0-alpha01") // 或更高版本
    implementation ("com.github.getActivity:XXPermissions:18.63")

    // androidx版本
    implementation ("com.github.xuexiangjys:XUpdate:2.1.5")
    implementation ("com.zhy:okhttputils:2.6.2")

    implementation ("com.tencent.bugly:crashreport:4.1.9.3") //其中latest.release指代最新Bugly SDK版本号，也可以指定明确的版本号，例如4.0.3
//    implementation("com.github.bumptech.glide:glide:4.16.0")
//    implementation ("com.github.bumptech.glide:compose:1.0.0-beta01")


    implementation("com.github.bingoogolapple.BGAQRCode-Android:zxing:v1.3.7")
    implementation("com.github.bingoogolapple.BGAQRCode-Android:zbar:v1.3.7")
    // https://mvnrepository.com/artifact/androidx.glance/glance
    implementation(libs.androidx.glance.glance.appwidget.v110)
    implementation(libs.androidx.glance.glance)
    implementation(libs.androidx.glance.glance.appwidget.v110.x2)
    implementation(libs.androidx.glance.glance.material32 )


    // Kotlin用这个，支持协程。
    implementation("androidx.work:work-runtime-ktx:2.7.1")
    implementation("com.github.skydoves:colorpicker-compose:1.0.7")

//    //shiply-灰度升级
//    implementation("com.tencent.shiply:upgrade:2.0.0-RC01")
//    implementation("com.tencent.shiply:upgrade-ui:2.1.5-RC01") // 弹框ui相关，业务方如果自己自定义弹框，可以不依赖这个库
//    //shiply-灰度配置
//    implementation ("com.tencent.shiply:rdelivery:1.3.35-RC02")



}