package com.example.coursetable_compose.ui.screen

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.KeyboardArrowRight
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.desktop.screenbean.MainPageScreen
import com.example.coursetable_compose.ui.activity.baseRadius
import com.example.coursetable_compose.ui.commen.BaseToolbar
import java.io.Serializable

@Composable
fun OpenSourceSoftwareStatementScreen(navController: NavHostController, innerPadding: PaddingValues) {

    val openSourceSoftwareBeans = listOf(
        OpenSourceSoftwareBean(
            name = "Retrofit",
            desc = "一个类型安全的HTTP客户端库，用于Android和Java，基于OkHttp构建，简化网络请求的创建、执行和响应处理",
            url = "https://square.github.io/retrofit/"
        ),
        OpenSourceSoftwareBean(
            name = "Gson Converter",
            desc = "Retrofit的一个转换器插件，用于将JSON数据自动序列化和反序列化为Java对象",
            url = "https://github.com/square/retrofit/tree/master/retrofit-converters/gson"
        ),
        OpenSourceSoftwareBean(
            name = "Logging Interceptor",
            desc = "OkHttp的一个拦截器，用于在HTTP请求和响应过程中添加日志输出，便于调试网络通信",
            url = "https://github.com/square/okhttp/tree/master/okhttp-logging-interceptor"
        ),
        OpenSourceSoftwareBean(
            name = "Okio",
            desc = "一个高效的I/O库，用于简化Java NIO，被OkHttp和其它项目广泛使用",
            url = "https://square.github.io/okio/"
        ),
        OpenSourceSoftwareBean(
            name = "Java Security",
            desc = "Java官方提供的安全包，包含各种加密、认证、证书管理等安全相关的类和接口",
            url = "https://docs.oracle.com/javase/8/docs/api/java/security/package-summary.html" // Java 8文档，适用于大部分场景
        ),
        OpenSourceSoftwareBean(
            name = "Logger",
            desc = "是一个专为 Android 平台设计的轻量级、功能丰富的日志库。",
            url = "https://github.com/orhanobut/logger" // 请提供具体的库名以获取准确信息
        ),
        OpenSourceSoftwareBean(
            name = "ThreeTen Extra",
            desc = "对JSR-310（日期和时间API）的扩展库，提供额外的日期时间处理功能",
            url = "https://www.threeten.org/threeten-extra/"
        ),
        OpenSourceSoftwareBean(
            name = "AndroidX Navigation Compose",
            desc = "为Jetpack Compose应用程序提供导航支持的库",
            url = "https://developer.android.com/jetpack/compose/navigation"
        ),
        OpenSourceSoftwareBean(
            name = "ThreeTenABP",
            desc = "将JSR-310（Java 8日期和时间API）移植到Android的库，通过Android的后台服务提供更好的兼容性",
            url = "https://github.com/JakeWharton/ThreeTenABP"
        ),
        OpenSourceSoftwareBean(
            name = "LC Storage Android",
            desc = "Taptap开发的本地存储库，用于简化Android应用的数据持久化",
            url = "https://github.com/TapTap/lc-storage-android"
        ),
        OpenSourceSoftwareBean(
            name = "RxAndroid",
            desc = "RxJava在Android平台上的扩展库，提供线程调度支持，方便在Android应用中使用响应式编程",
            url = "https://github.com/ReactiveX/RxAndroid"
        ),
        OpenSourceSoftwareBean(
            name = "Precompose",
            desc = "提供一系列Jetpack Compose预置组件和实用函数的库",
            url = "https://github.com/tlaster/precompose"
        ),
        OpenSourceSoftwareBean(
            name = "Oshi",
            desc = "一个跨平台的系统信息库，允许在Java程序中获取硬件及操作系统信息",
            url = "https://github.com/oshi/oshi"
        ),
        OpenSourceSoftwareBean(
            name = "Material Design",
            desc = "Google官方提供的实现Material Design设计语言的UI组件库",
            url = "https://github.com/material-components/material-components-android"
        ),
        OpenSourceSoftwareBean(
            name = "XXPermissions",
            desc = "一个用于简化Android权限申请和管理的库",
            url = "https://github.com/getActivity/XXPermissions"
        ),
        OpenSourceSoftwareBean(
            name = "FileDownloader",
            desc = "一个轻量级、强大的多线程文件下载库，支持断点续传等功能",
            url = "https://github.com/lingochamp/FileDownloader"
        ),
        OpenSourceSoftwareBean(
            name = "XUpdate",
            desc = "一个Android应用内自动更新库，提供友好的更新提示界面和灵活的更新策略",
            url = "https://github.com/xuexiangjys/XUpdate"
        ),
        OpenSourceSoftwareBean(
            name = "OkHttpUtils",
            desc = "一个对OkHttp进行封装的网络请求库，提供更简洁的API和一些常用功能",
            url = "https://github.com/hongyangAndroid/okhttputils"
        ),
        OpenSourceSoftwareBean(
            name = "Bugly",
            desc = "腾讯提供的移动端崩溃报告和异常捕获工具，支持Crash分析、ANR监控等功能",
            url = "https://bugly.qq.com/v2/"
        ),
        OpenSourceSoftwareBean(
            name = "Glide",
            desc = "一个高效的Android图片加载库，支持图片缓存、缩放、动画等功能",
            url = "https://github.com/bumptech/glide"
        ),
        OpenSourceSoftwareBean(
            name = "Glide Compose",
            desc = "Glide针对Jetpack Compose提供的适配库，便于在Compose中使用Glide加载和显示图片",
            url = "https://github.com/bumptech/glide/tree/master/integration/compose"
        ),
        OpenSourceSoftwareBean(
            name = "BGA QR Code (ZXing)",
            desc = "基于ZXing库实现的二维码/条形码扫描与生成库",
            url = "https://github.com/bingoogolapple/BGAQRCode-Android"
        ),
        OpenSourceSoftwareBean(
            name = "BGA QR Code (ZBar)",
            desc = "基于ZBar库实现的二维码/条形码扫描与生成库",
            url = "https://github.com/bingoogolapple/BGAQRCode-Android"
        )
    )


    Column(Modifier.fillMaxSize().padding(top = innerPadding.calculateTopPadding())) {
        BaseToolbar(navController, MainPageScreen.OpenSourceSoftwareStatementScreen.label)

        LazyColumn {
            items(items = openSourceSoftwareBeans) {
                Card(modifier = Modifier.padding(horizontal = 12.dp), shape = baseRadius,
                    onClick ={
                        openBrowserByUrl(navController.context,it.url)
                    }
                ) {
                    Row(
                        modifier = Modifier.padding(12.dp),
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Column(modifier = Modifier.weight(1f)) {
                            Text(text = it.name, style = MaterialTheme.typography.labelLarge)
                            Text(text = it.desc, style = MaterialTheme.typography.labelMedium)
                        }
                        Icon(
                            imageVector = Icons.AutoMirrored.Filled.KeyboardArrowRight,
                            contentDescription = ""
                        )
                    }
                }
                Spacer(modifier = Modifier.height(12.dp))
            }
        }
    }
}

data class OpenSourceSoftwareBean(
    val name: String,
    val desc: String,
    val url: String
) : Serializable