package com.example.coursetable_compose.ui.screen

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Paint
import android.os.Build
import android.text.StaticLayout
import android.text.TextPaint
import androidx.annotation.RequiresApi
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.BasicAlertDialog
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.pulltorefresh.PullToRefreshContainer
import androidx.compose.material3.pulltorefresh.rememberPullToRefreshState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.nativeCanvas
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.example.coursetable_compose.constant.COURSE_TABLE_ITEM_HEIGHT
import com.example.coursetable_compose.ui.commen.CourseTableViewPlus
import com.example.coursetable_compose.ui.commen.KBDetail
import com.example.coursetable_compose.ui.commen.LeftSideBar
import com.example.coursetable_compose.ui.commen.OtherCourseList
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.layout.ContentScale
import com.example.coursetable_compose.R
import com.example.coursetable_compose.ui.activity.baseRadius
import com.mypotota.datamodel.base.BaseStatus
import com.example.coursetable_compose.ui.commen.CourseTopTitle
import com.example.coursetable_compose.ui.commen.TabLayoutView
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.utils.Utils
import com.example.coursetable_compose.viewmodel.BindAccountViewModel
import com.example.coursetable_compose.viewmodel.CourseTableViewModel
import com.mypotota.datamodel.bean.Kb
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.math.absoluteValue


@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class, ExperimentalFoundationApi::class,
)
@Composable
fun CourTableScreen(navController: NavHostController) {
    val courseTableViewModel = viewModel<CourseTableViewModel>()
    val userViewModel = viewModel<BindAccountViewModel>()
    val scope = rememberCoroutineScope()

    val rememberScrollState = rememberScrollState()
    val weekCount = courseTableViewModel.weekCount.collectAsState()
    val tabList = (0..weekCount.value).toList()
    val weekMonthMap = courseTableViewModel.weekMonthMap.collectAsState()
    //侧栏
    val sideBarList = courseTableViewModel.sideBarList.collectAsState()
    //其他课程
    val sjkList = courseTableViewModel.sjkList.collectAsState()
    //课表信息
    val courseTableList = courseTableViewModel.courseTableList.collectAsState()
    var showBottomSheet by remember { mutableStateOf(false) }

    val selectCurrentWeek = courseTableViewModel.selectCurrentWeek.collectAsState()
    //viewPager状态
    val pagerState = rememberPagerState(initialPage = selectCurrentWeek.value, pageCount = {
        weekCount.value + 1
    })
    var isReadCourseRefresh by remember {
        mutableStateOf(SPUtils.getIns().isReadCourseRefresh)
    }

    LaunchedEffect(key1 = selectCurrentWeek.value) {
        pagerState.scrollToPage(selectCurrentWeek.value)
    }
    LaunchedEffect(key1 = pagerState.currentPage) {
        courseTableViewModel.selectWeek(pagerState.currentPage)
    }

    var refreshIsShow by remember {
        mutableStateOf(true)
    }
    //当前选中的课表
    var currentSelectKB by remember {
        mutableStateOf<Kb?>(null)
    }

    var isShowMessageTip by remember {
        mutableStateOf(false)
    }
    var messageTip by remember {
        mutableStateOf("")
    }
   //文件背景
    val filePath by remember {
        mutableStateOf(SPUtils.getIns().getBackgroundImagePath())
    }
    //背景透明度
    val backgroundAlpha by remember {
        mutableFloatStateOf(SPUtils.getIns().getBackgroundAlpha())
    }
    //课表刷新状态
    val refreshStatus = userViewModel.courseTableStatus.collectAsState()
    //sh
    val isShowTopDate by remember {
        mutableStateOf(SPUtils.getIns().isShowTopDate)
    }
    val isShowLeftTime by remember {
        mutableStateOf(SPUtils.getIns().isShowLeftTime)
    }
    val state = rememberPullToRefreshState()
    if (state.isRefreshing) {
        LaunchedEffect(true) {
            userViewModel.getLoginTicket(
                SPUtils.getIns().getAccount().toString(),
                SPUtils.getIns().getPassword().toString()
            )
            state.endRefresh()
        }
    }
    LaunchedEffect(refreshStatus.value) {
        when (val status=refreshStatus.value) {
            is BaseStatus.ERROR -> {
                refreshIsShow = true
                //提示
                messageTip = status.e
                scope.launch {
                    delay(1000)
                    isShowMessageTip = false
                }
            }

            is BaseStatus.LOADING -> {
                isShowMessageTip = true
                messageTip = status.msg

                refreshIsShow = false
            }

            is BaseStatus.SUCCESS -> {
                SPUtils.getIns().setCourseTable(status.data)
                courseTableViewModel.selectToday()
                refreshIsShow = true
                //提示
                messageTip = "刷新成功"
                scope.launch {
                    delay(1000)
                    isShowMessageTip = false
                }
            }
            null -> {

            }
        }
    }



    Scaffold(
        floatingActionButton = {
            AnimatedVisibility(
                visible = refreshIsShow,
                enter = scaleIn(),
                exit = scaleOut()
            ) {
                Column {
//                    FloatingActionButton(onClick = {
//                    }, shape = baseRadius) {
//                        Icon(
//                            imageVector = Icons.Filled.Share,
//                            contentDescription = "location",
//                            modifier = Modifier.size(24.dp)
//                        )
//                    }
//                    Spacer(modifier = Modifier.height(8.dp))
                    FloatingActionButton(onClick = {
                        courseTableViewModel.selectToday()
                    }, shape = baseRadius) {
                        Icon(
                            painter = painterResource(R.drawable.outline_today_24),
                            contentDescription = "location",
                            modifier = Modifier.size(24.dp)
                        )
                    }
                }
            }
        }
    ) {innerPadding->
        Box(
            Modifier
                .nestedScroll(state.nestedScrollConnection)
                .clipToBounds()
        ){
            if (!filePath.isNullOrEmpty()) {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(MaterialTheme.colorScheme.background)
                        .alpha(backgroundAlpha)
                ) {
                    Image(bitmap = Utils.filePath2Bitmap(filePath?:""), contentDescription = "",contentScale = ContentScale.Crop,
                        modifier = Modifier.fillMaxSize())
                }
            }
            Column {
                AnimatedVisibility(isShowMessageTip) {
                    Column(
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier
                            .padding(12.dp)
                    ) {
                        CircularProgressIndicator()
                        Text(
                            messageTip,
                            Modifier
                                .fillMaxWidth(), textAlign = TextAlign.Center
                        )
                    }
                }


                //tab点击事件
                TabLayoutView(selectCurrentWeek, weekCount) {
                    courseTableViewModel.selectWeek(it)
                }
                AnimatedVisibility(visible = isReadCourseRefresh) {
                    Card(onClick = {
                        SPUtils.getIns().isReadCourseRefresh = false
                        isReadCourseRefresh=SPUtils.getIns().isReadCourseRefresh
                    }, modifier = Modifier
                        .padding(horizontal = 12.dp), shape =  baseRadius){
                        Row(Modifier.padding(horizontal = 12.dp, vertical = 4.dp)){
                            Text(text = "下拉可刷新课表哟",Modifier.weight(1f))
                            Icon(Icons.Default.Close, contentDescription ="close")
                        }
                    }
                }
                Column(Modifier.verticalScroll(rememberScrollState)) {

                    Spacer(Modifier.height(8.dp))
                    CourseTopTitle(
                        weekMonthMap,
                        courseTableViewModel.currentDate,
                        baseRadius,
                        isShowTopDate
                    )
                    Row {
                        LeftSideBar(sideBarList.value, isShowLeftTime)
                        HorizontalPager(
                            state = pagerState, Modifier.fillMaxSize(),
                            beyondBoundsPageCount = 2
                        ) { pageIndex ->
                            AnimatedVisibility(
                                visible = pageIndex == pagerState.currentPage,
                                exit = scaleOut(),
                                enter = scaleIn()
                            ) {
                                CourseTableViewPlus(
                                    Modifier
                                        .graphicsLayer {
                                            val pageOffset =
                                                ((pagerState.currentPage - pageIndex) + pagerState.currentPageOffsetFraction).absoluteValue
                                            alpha = 1f - pageOffset.coerceIn(0f, 1f)
                                        },
                                    courseTableList.value,
                                    courseTableViewModel.weekCountDay,
                                    SPUtils.getIns().getColorMapByCourse()
                                ) {
                                    currentSelectKB = it
                                    showBottomSheet = true
                                }
                            }
                        }
                    }

                    Text(
                        text = "其他课程",
                        modifier = Modifier.padding(12.dp),
                        color = MaterialTheme.colorScheme.primary,
                        fontWeight = FontWeight(1000)
                    )
                    OtherCourseList(sjkList.value)
                    Spacer(modifier = Modifier.height(150.dp))

                }
//            TabLayoutView(selectCurrentWeek, weekCount) {
//                courseTableViewModel.selectWeek(it)
//            }
//
//            Spacer(Modifier.height(8.dp))
//            CourseTopTitle(weekMonthMap, courseTableViewModel.currentDate)
//            Spacer(Modifier.height(8.dp))
//            Column(
//                modifier = Modifier
//                    .verticalScroll(rememberScrollState()) // invoke before the scrollable Modifier
//                    //.overScrollHorizontal() // or this
//            ) {
//                Row {
//                    LeftSideBar(sideBarList.value)
////                    CourseTableView(courseTableList.value,courseTableViewModel.weekCountDay,SPUtils.getIns().getColorMapByCourse())
//                    CourseTableViewPlus(
//                        courseTableList.value,
//                        courseTableViewModel.weekCountDay,
//                        SPUtils.getIns().getColorMapByCourse()
//                    ) {
//                        currentSelectKB = it
//                        showBottomSheet = true
//                    }
//                }
//                Text(
//                    text = "其他课程",
//                    modifier = Modifier.padding(12.dp),
//                    color = MaterialTheme.colorScheme.primary,
//                    fontWeight = FontWeight(1000)
//                )
//                OtherCourseList(sjkList.value)
//                Spacer(modifier = Modifier.height(150.dp))
//            }
            }

            PullToRefreshContainer(
                state = state,
                modifier = Modifier.align(Alignment.TopCenter)
            )

            //底部弹窗
            if (showBottomSheet) {
                BasicAlertDialog({
                    showBottomSheet = false
                }) {
                    val courseColor = Color(
                        Utils.getCourseColor(
                            currentSelectKB!!,
                            SPUtils.getIns().getColorMapByCourse()
                        )
                    )
                    KBDetail(currentSelectKB, courseColor) { text ->
                        val manager: ClipboardManager =
                            navController.context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                        val mClipData = ClipData.newPlainText("Label", text)
                        manager.setPrimaryClip(mClipData)
                        showBottomSheet = false
                    }
                }
            }
        }
    }


}


@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun CourseTableView(
    courseTableList: List<Kb>?,
    weekCountDay: Int,
    colorMapByCourse: HashMap<String, Long>
) {
    val textPaint = TextPaint()
    textPaint.setColor(android.graphics.Color.WHITE)
    textPaint.textAlign = Paint.Align.CENTER
    textPaint.color = android.graphics.Color.WHITE

    if (courseTableList == null || courseTableList.isEmpty()) {
        CircularProgressIndicator(modifier = Modifier.padding(12.dp))
        return
    }
    Canvas(modifier = Modifier.fillMaxSize()) {
        val canvasWidth = size.width
        val itemWidth = canvasWidth / weekCountDay

        drawIntoCanvas { canvas ->
            courseTableList.forEach { kb ->

                val jc = kb.jc.replace("节", "").split("-")
                val courseJc = jc[0].toInt()..jc[jc.size - 1].toInt()

                var itemTop = (courseJc.first - 1) * COURSE_TABLE_ITEM_HEIGHT.toPx()
                var itemBottom = courseJc.last * COURSE_TABLE_ITEM_HEIGHT.toPx()

                //识别是周几
                var itemRight = 0f
                var itemLeft = 0f
                when (kb.xqjmc) {
                    "星期一" -> {
                        itemRight = itemWidth * 1f
                    }

                    "星期二" -> {
                        itemRight = itemWidth * 2f
                        itemLeft = itemWidth * 1f
                    }

                    "星期三" -> {
                        itemRight = itemWidth * 3f
                        itemLeft = itemWidth * 2f
                    }

                    "星期四" -> {
                        itemRight = itemWidth * 4f
                        itemLeft = itemWidth * 3f
                    }

                    "星期五" -> {
                        itemRight = itemWidth * 5f
                        itemLeft = itemWidth * 4f
                    }

                    "星期六" -> {
                        itemRight = itemWidth * 6f
                        itemLeft = itemWidth * 5f
                    }

                    "星期日" -> {
                        itemRight = itemWidth * 7f
                        itemLeft = itemWidth * 6f
                    }
                }


                itemLeft += 1.dp.toPx()
                itemRight -= 1.dp.toPx()
                itemTop += 1.dp.toPx()
                itemBottom -= 1.dp.toPx()
                drawRoundRect(
                    Color(Utils.getCourseColor(kb, colorMapByCourse)),
                    Offset(itemLeft, itemTop),
                    Size(itemRight - itemLeft, itemBottom - itemTop),
                    CornerRadius(20.dp.value, 20.dp.value)
                )

                val content = "${kb.cdmc}\n${kb.kcmc}"
                //绘制文字
                textPaint.textSize = 10.dp.toPx()
                val staticLayout = StaticLayout.Builder.obtain(
                    content, 0, content.length, textPaint,
                    ((canvasWidth / 7f) - (1.dp.toPx()) * 2f).toInt()
                ).build()
                //移动文字绘制
                canvas.nativeCanvas.save()
                val staticLayoutX = itemLeft + (staticLayout.width) / 2
                val staticLayoutY = (itemTop + itemBottom) / 2 - (staticLayout.height) / 2
                canvas.nativeCanvas.translate(staticLayoutX, staticLayoutY)
                staticLayout.draw(canvas.nativeCanvas)
                canvas.nativeCanvas.restore()
            }


        }
    }
}
