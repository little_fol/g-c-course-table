package com.example.coursetable_compose.ui.screen

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.desktop.screenbean.MainPageScreen
import com.example.coursetable_compose.ui.commen.BaseToolbar
import com.example.coursetable_compose.utils.SPUtils

@Composable
fun AppSettingScreen(navController: NavHostController, innerPadding: PaddingValues) {
    Column(
        Modifier.fillMaxSize().padding(top = innerPadding.calculateTopPadding())
    ) {
        BaseToolbar(navController, MainPageScreen.AppSettingScreen.label)

        Column(
            Modifier
                .verticalScroll(rememberScrollState())
                .padding(horizontal = 12.dp)
        ) {
            RefreshBtn()
        }

    }
}

@Composable
private fun RefreshBtn() {
    var isAutoRefresh by remember {
        mutableStateOf(SPUtils.getIns().getAutoRefresh())
    }
    val isShowRefresh by remember {
        mutableStateOf(SPUtils.getIns().getShowAutoRefresh())
    }
    TextButton(enabled = isShowRefresh, shape = RoundedCornerShape(10.dp), onClick = {
        SPUtils.getIns().setAutoRefresh(!isAutoRefresh)
        isAutoRefresh = SPUtils.getIns().getAutoRefresh()
    }) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Column( Modifier.weight(1f)) {
                Text(text = "启动时后台静默刷新课表", style = MaterialTheme.typography.labelLarge)
                Text(text = "刷新课表间隔时间：6小时", style = MaterialTheme.typography.labelSmall)
            }
            Switch(checked = isAutoRefresh, enabled = isShowRefresh, onCheckedChange = {
                SPUtils.getIns().setAutoRefresh(it)
                isAutoRefresh = SPUtils.getIns().getAutoRefresh()
            })
        }
    }
}
