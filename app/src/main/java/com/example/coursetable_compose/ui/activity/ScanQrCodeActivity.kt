package com.example.coursetable_compose.ui.activity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Vibrator
import android.view.LayoutInflater
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.viewinterop.AndroidView
import cn.bingoogolapple.qrcode.core.QRCodeView
import cn.bingoogolapple.qrcode.zxing.ZXingView
import com.example.coursetable_compose.R
import com.example.coursetable_compose.ui.theme.CourseTablecomposeTheme


class ScanQrCodeActivity : ComponentActivity(), QRCodeView.Delegate {
    private lateinit var mZXingView:ZXingView
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            CourseTablecomposeTheme {
                Scaffold(modifier = Modifier.fillMaxSize()) { innerPadding ->
                    Column(Modifier.padding(innerPadding)) {
                        ScanQrView()
                    }
                }
            }
        }
    }

    @Composable
    private fun ScanQrView() {
        val qrCodeActivity = LocalContext.current as ScanQrCodeActivity
        val lifecycleOwner = LocalLifecycleOwner.current

        Column {
            AndroidView(
                factory = { context ->
                    val inflate = LayoutInflater.from(context)
                        .inflate(R.layout.activity_sacn_qr_code, null, false)
                    mZXingView = inflate.findViewById<ZXingView>(R.id.mZXingView)
                    mZXingView.setDelegate(this@ScanQrCodeActivity)
//                    mZXingView.startSpotAndShowRect() // 显示扫描框，并开始识别

                    mZXingView.startCamera();//打开相机
                    mZXingView.showScanRect();//显示扫描框
                    mZXingView.startSpot();//开始识别二维码

                    inflate
                }, modifier = Modifier.fillMaxSize()
            )
        }
    }

    override fun onStop() {
        super.onStop()
        mZXingView.stopCamera()
    }

    override fun onDestroy() {
        super.onDestroy()
        mZXingView.onDestroy();
    }

    override fun onScanQRCodeSuccess(result: String?) {
        setResult(102, Intent().putExtra("code",result))
        finish()
    }

    private fun vibrate() {
        val vibrator = getSystemService(VIBRATOR_SERVICE) as Vibrator
        vibrator.vibrate(200)
    }

    override fun onCameraAmbientBrightnessChanged(isDark: Boolean) {
//        vibrate()
//        Toast.makeText(this,"环境光太暗正在打开闪光灯",Toast.LENGTH_SHORT).show();
//        mZXingView.startSpot();
    }

    override fun onScanQRCodeOpenCameraError() {
    }


}
