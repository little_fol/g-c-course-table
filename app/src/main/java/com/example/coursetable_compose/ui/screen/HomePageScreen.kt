package com.example.coursetable_compose.ui.screen

import android.widget.Toast
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.KeyboardArrowRight
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Face
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.desktop.screenbean.MainPageScreen
import com.example.coursetable_compose.R
import com.example.coursetable_compose.ui.activity.baseRadius
import com.mypotota.datamodel.base.BaseStatus
import com.example.coursetable_compose.ui.commen.NoticeDialogUI
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.utils.Utils
import com.example.coursetable_compose.viewmodel.CourseTableViewModel
import com.example.coursetable_compose.viewmodel.EnergyChargeViewModel
import com.example.coursetable_compose.viewmodel.GroupCourseTableViewModel
import com.example.coursetable_compose.viewmodel.NoticeViewModel
import com.mypotota.datamodel.bean.EnergyChargeBeanSubList
import com.mypotota.datamodel.bean.Kb
import com.tapsdk.lc.LCObject
import kotlinx.coroutines.coroutineScope

@Composable
fun HomePageScreen(parentNavController: NavController) {
    val noticeViewModel = viewModel<NoticeViewModel>()
    val viewModel = viewModel<CourseTableViewModel>()
    val mainPageMode = SPUtils.getIns().getMainPageModeMap()

    viewModel.getTomorrowCourseTable()
    viewModel.getToDayCourseTable()
    noticeViewModel.getAllNotices()


    var isShowNotice by remember { mutableStateOf(false) }
    var noticeLCObject by remember { mutableStateOf<LCObject>(LCObject()) }

    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState())
            .fillMaxSize(),
    ) {
        Text(
            text = "主页",
            style = MaterialTheme.typography.headlineLarge,
            modifier = Modifier.padding(12.dp)
        )
        mainPageMode.entries.forEach {
            if (!it.value) return@forEach
            when (it.key) {
                //今日课表，明日课表
                "今明课表" -> MainCourseTableCards()
                //小组课表
                "小组课表" -> GroupCourseTable(parentNavController)
                //查电费
                "查电费" -> EnergyChargeCard()
                //查询成绩
                "我的成绩" -> MineGrades(parentNavController)
                //查电费
                "所有通知" -> {
                    AllNotice() {
                        noticeLCObject = it
                        isShowNotice = true
                    }
                }
            }
        }

        AnimatedVisibility(
            visible = mainPageModeIsEmpty(mainPageMode), modifier = Modifier
                .padding(12.dp)
                .align(Alignment.CenterHorizontally)
        ) {
            Text(text = "没有更多组件咯:)")
        }
    }

    if (isShowNotice) {
        NoticeDialogUI(noticeLCObject, {
            isShowNotice = false
        }, {
            isShowNotice = false
            SPUtils.getIns().setReadNotice(it)
        })
    }
}

@Composable
fun MineGrades(parentNavController: NavController) {
    val gradesList = SPUtils.getIns().getGradesList()
    println("加载成绩")
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        Row(
            Modifier
                .fillMaxWidth()
                .padding(horizontal = 12.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                "我的成绩",
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier.weight(1f)
            )
            TextButton(onClick = {
                parentNavController.navigate(MainPageScreen.MineGradesScreen.name)
            }, shape = baseRadius) {
                Text(text = "查看更多")
            }
        }

        AnimatedVisibility(visible = gradesList.isEmpty()) {
            Text(
                text = "暂无成绩列表", modifier = Modifier
                    .padding(12.dp)
            )
        }
        gradesList.forEachIndexed { index, item ->
                Card(
                    Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 12.dp, vertical = 6.dp),
                    shape = baseRadius
                ) {
                    Row(modifier = Modifier.padding(12.dp)) {
                        Text(text = item.kcmc, Modifier.weight(1f))
                        Text(text = item.cj, color = MaterialTheme.colorScheme.primary)
                    }
                }
        }

    }
}

fun mainPageModeIsEmpty(mainPageMode: Map<String, Boolean>): Boolean {
    mainPageMode.entries.forEach {
        if (it.value) {
            return false
        }
    }
    return true
}

@Composable
fun EnergyChargeCard() {
    val viewModel = viewModel<EnergyChargeViewModel>()
    LaunchedEffect(Unit) {
        viewModel.getRoomEnergyCharge()
    }
    val getRoomEnergyChargeStatus = viewModel.getRoomEnergyChargeStatus.collectAsState()
    var isLoading by remember { mutableStateOf(false) }
    var loadingMessage by remember { mutableStateOf("") }
    var isError by remember { mutableStateOf(false) }
    var errorMessage by remember { mutableStateOf("") }
    var energyCharge by remember { mutableStateOf<EnergyChargeBeanSubList?>(null) }

    var bindRoom by remember {
        mutableStateOf(false)
    }

    LaunchedEffect(getRoomEnergyChargeStatus.value) {
        when (val status = getRoomEnergyChargeStatus.value) {
            is BaseStatus.ERROR -> {
                isLoading = false
                isError = true
                errorMessage = status.e
            }

            is BaseStatus.LOADING -> {
                isLoading = true
                isError = false
                loadingMessage = status.msg
            }

            is BaseStatus.SUCCESS -> {
                isLoading = false
                isError = false
                energyCharge = status.data
            }
        }
    }


    Column {
        Row(
            Modifier
                .fillMaxWidth()
                .padding(horizontal = 12.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                "查电费",
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier.weight(1f)
            )
            IconButton(onClick = {
                viewModel.getRoomEnergyCharge(1)
            }) {
                Icon(imageVector = Icons.Default.Refresh, contentDescription = "")
            }
            TextButton(onClick = {
                bindRoom = true
            }, shape = baseRadius) {
                Text(text = "绑定房间")
            }
        }

        //发生错误时
        AnimatedVisibility(isError) {
            Card(
                Modifier
                    .fillMaxWidth()
                    .padding(12.dp),
                shape = baseRadius
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(12.dp)
                ) {
                    Icon(Icons.Default.Info, "")
                    Text(
                        "获取房间电费失败，错误原因：$errorMessage",
                        textAlign = TextAlign.Center
                    )
                }
            }
        }

        //加载中...
        AnimatedVisibility(visible = isLoading) {
            Card(modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp),
                shape = baseRadius, onClick = {}) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CircularProgressIndicator()
                    Text(text = loadingMessage)
                }
            }
        }

        //为空时
        AnimatedVisibility(visible = !isError && !isLoading && energyCharge == null) {
            Card(modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp),
                shape = baseRadius, onClick = {}) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Icon(
                        painterResource(id = R.drawable.outline_not_interested_24),
                        contentDescription = ""
                    )
                    Text(text = "获取房间电费失败")
                }
            }
        }
        //查到电费时
        AnimatedVisibility(visible = !isError && !isLoading && energyCharge != null) {
            Card(modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp),
                shape = baseRadius, onClick = {}) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(12.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Row {
                        Text(
                            text = "房间号",
                            modifier = Modifier.weight(1f),
                            style = MaterialTheme.typography.labelLarge
                        )
                        Text(text = energyCharge!![0], style = MaterialTheme.typography.labelLarge)
                    }
                    Row {
                        Text(
                            text = "余额",
                            modifier = Modifier.weight(1f),
                            style = MaterialTheme.typography.labelLarge
                        )
                        Text(text = energyCharge!![1], style = MaterialTheme.typography.labelLarge)
                    }
                    Row {
                        Text(
                            text = "扣款日期",
                            modifier = Modifier.weight(1f),
                            style = MaterialTheme.typography.labelLarge
                        )
                        Text(text = energyCharge!![2], style = MaterialTheme.typography.labelLarge)
                    }
                    Spacer(modifier = Modifier.height(12.dp))
                    Text(
                        text = "晚上11点后无法缴费，为了避免学校服务器压力激增，电费不会自动更新，请点击刷新按钮进行更新",
                        textAlign = TextAlign.Center,
                        style = MaterialTheme.typography.labelSmall
                    )
                }
            }
        }
    }



    if (bindRoom) {
        BindRoomDialog({
            bindRoom = false
        }) {
            bindRoom = false
            viewModel.getRoomEnergyCharge(1)
        }
    }


}

@Composable
fun BindRoomDialog(dismiss: () -> Unit, bindSucces: () -> Unit) {
    val context = LocalContext.current
    var roomNumber by remember {
        mutableStateOf(SPUtils.getIns().getRoomNumber() ?: "")
    }
    val bindClick: () -> Unit = {
        if (roomNumber.isEmpty()) {
            Toast.makeText(context, "请输入房间号码", Toast.LENGTH_SHORT).show()
        } else {
            SPUtils.getIns().setRoomNumber(roomNumber)
            bindSucces()
        }
    }
    AlertDialog(onDismissRequest = {
        dismiss()
    }, shape = baseRadius, confirmButton = {
    }, title = {
        Text(text = "绑定房间号码", style = MaterialTheme.typography.titleLarge)
    }, text = {
        Column {
            OutlinedTextField(value = roomNumber,
                onValueChange = { roomNumber = it },
                leadingIcon = {
                    Icon(Icons.Default.Edit, contentDescription = "")
                },
                shape = baseRadius,
                modifier = Modifier.fillMaxWidth(),
                label = {
                    Text(text = "请输入新版房间门牌号：Hx-xxx")
                }
            )
            Button(
                onClick = bindClick,
                modifier = Modifier.fillMaxWidth(),
                shape = baseRadius
            ) {
                Text(text = "绑定")
            }
        }
    })
}


/**
 * 小组课表
 */
@Composable
fun GroupCourseTable(navController: NavController) {
    val viewModel = viewModel<GroupCourseTableViewModel>()
    val homeShowGroupListStatus = viewModel.homeShowGroupListStatus.collectAsState()

    var isLoading by remember { mutableStateOf(false) }
    var loadingMessage by remember { mutableStateOf("") }
    var isError by remember { mutableStateOf(false) }
    var errorMessage by remember { mutableStateOf("") }
    var groupList by remember { mutableStateOf<List<LCObject>>(ArrayList()) }
    LaunchedEffect(Unit) {
        viewModel.getHomeGroup()
    }
    LaunchedEffect(homeShowGroupListStatus.value) {
        coroutineScope {
            when (val status = homeShowGroupListStatus.value) {
                is BaseStatus.ERROR -> {
                    isLoading = false
                    isError = true
                    errorMessage = status.e
                }

                is BaseStatus.LOADING -> {
                    isLoading = true
                    isError = false
                    loadingMessage = status.msg
                }

                is BaseStatus.SUCCESS -> {
                    isLoading = false
                    isError = false
                    groupList = status.data
                }
            }
        }
    }


    Column {

        Row(
            Modifier
                .fillMaxWidth()
                .padding(horizontal = 12.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                "小组课表",
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier.weight(1f)
            )
            TextButton(onClick = {
                navController.navigate(MainPageScreen.GroupCourseTable.name)
            }, shape = baseRadius) {
                Text(text = "查看更多")
            }
        }


        //加载中...
        AnimatedVisibility(visible = isLoading) {
            Card(modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp),
                shape = baseRadius, onClick = {}) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CircularProgressIndicator()
                    Text(text = loadingMessage)
                }
            }
        }

        //为空时
        AnimatedVisibility(visible = !isError && !isLoading && groupList.isEmpty()) {
            Card(modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp),
                shape = baseRadius, onClick = {}) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Icon(
                        painterResource(id = R.drawable.outline_not_interested_24),
                        contentDescription = ""
                    )
                    Text(text = "暂无加入主页显示的小组")
                }
            }
        }

        //有数据时
        AnimatedVisibility(visible = !isError && !isLoading && groupList.isNotEmpty()) {
            Column {
                groupList.forEachIndexed { index, lcObject ->
                    key(lcObject.objectId) {
                        Card(modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 12.dp),
                            shape = baseRadius,
                            onClick = {
                                navController.navigate("${MainPageScreen.GroupCourseTableDetailScreen.name}/data=${lcObject.toJSONString()}")
                            }) {
                            Row(
                                Modifier
                                    .fillMaxWidth()
                                    .padding(12.dp),
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                Text(
                                    text = lcObject.getString("name"), modifier = Modifier.weight(
                                        1f
                                    )
                                )
                                Icon(
                                    Icons.AutoMirrored.Filled.KeyboardArrowRight,
                                    contentDescription = ""
                                )
                            }
                        }
                        Spacer(modifier = Modifier.height(4.dp))
                    }
                }
            }
        }
    }
}

//所有通知
@Composable
fun AllNotice(noticeClick: (lcObject: LCObject) -> Unit) {
    val noticeViewModel = viewModel<NoticeViewModel>()
    val allNoticeStatus = noticeViewModel.allNoticeStatus.collectAsState().value
    Text(
        "所有通知",
        style = MaterialTheme.typography.titleLarge,
        modifier = Modifier.padding(12.dp)
    )
    var isLoading by remember { mutableStateOf(true) }
    var isError by remember { mutableStateOf(false) }
    var errorMessage by remember { mutableStateOf("") }
    var notices by remember { mutableStateOf<List<LCObject>>(ArrayList()) }
    LaunchedEffect(allNoticeStatus) {
        when (allNoticeStatus) {
            is BaseStatus.ERROR -> {
                isLoading = false
                isError = true
                errorMessage = allNoticeStatus.e
            }

            is BaseStatus.LOADING -> {
                isLoading = true
                isError = false
            }

            is BaseStatus.SUCCESS -> {
                isLoading = false
                isError = false
                notices = allNoticeStatus.data
            }
        }
    }
    Column {
        //正在加载中
        AnimatedVisibility(isLoading) {
            Card(
                Modifier
                    .fillMaxWidth()
                    .padding(12.dp),
                shape = baseRadius
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(12.dp)
                ) {
                    CircularProgressIndicator()
                }
            }
        }
        //发生错误时
        AnimatedVisibility(isError) {
            Card(
                Modifier
                    .fillMaxWidth()
                    .padding(12.dp),
                shape = baseRadius
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(12.dp)
                ) {
                    Icon(Icons.Default.Info, "")
                    Text(
                        "获取全部通知失败，错误原因：$errorMessage，请稍后重试吧",
                        textAlign = TextAlign.Center
                    )
                    Button({}, shape = baseRadius) {
                        Text("重试")
                    }
                }
            }
        }
        //没有通知时
        AnimatedVisibility(notices.isEmpty() && !isLoading && !isError) {
            Card(
                Modifier
                    .fillMaxWidth()
                    .padding(12.dp),
                shape = baseRadius
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(12.dp)
                ) {
                    Icon(Icons.Default.Face, "")
                    Text("暂无通知")
                }
            }
        }
        //通知列表
        AnimatedVisibility(notices.isNotEmpty() && !isLoading && !isError) {
            Column {
                notices.forEachIndexed { index, item ->
                    NoticeCard(item) {
                        noticeClick.invoke(item)
                    }
                    Spacer(Modifier.width(8.dp))
                }
            }
        }
    }
}

@Composable
fun NoticeCard(item: LCObject, click: () -> Unit) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 12.dp, vertical = 8.dp),
        shape = baseRadius
    ) {
        Column(Modifier.padding(12.dp)) {
            Text(item.getString("title"), style = MaterialTheme.typography.bodyLarge)
            Spacer(modifier = Modifier.height(12.dp))
            Text(
                item.getString("message").replace("\n", ""),
                style = MaterialTheme.typography.bodySmall,
                maxLines = 5,
                overflow = TextOverflow.Ellipsis
            )
            TextButton(click, shape = baseRadius) {
                Text("查看详情")
            }
        }
    }
}

//课表
@Composable
fun MainCourseTableCards() {
    Text(
        "课程",
        style = MaterialTheme.typography.titleLarge,
        modifier = Modifier.padding(12.dp)
    )
    Column {
        //今日课表
        ToDayCourseTable(Modifier)
        //明日课表
        TomorrowCourseTable(Modifier)
    }
}

//明日课表
@Composable
fun TomorrowCourseTable(modifier: Modifier) {
    val viewModel = viewModel<CourseTableViewModel>()
    val tomorrowCourseTable = viewModel.tomorrowCourseTable.collectAsState().value
    CourseTableCard(
        "明日课表",
        "明天没有课程啦，给自己放放假吧",
        tomorrowCourseTable,
        modifier
    )
}

//今日课表
@Composable
fun ToDayCourseTable(modifier: Modifier) {
    val viewModel = viewModel<CourseTableViewModel>()
    val todayCourseTable = viewModel.todayCourseTable.collectAsState().value
    CourseTableCard(
        "今日课表",
        "今天没有课程，也不能松懈学习哟",
        todayCourseTable,
        modifier
    )
}

@Composable
fun CourseTableCard(
    title: String,
    empty: String,
    courseTable: BaseStatus<List<Kb>>,
    modifier: Modifier
) {

    var isLoading by remember { mutableStateOf(true) }
    var kbList by rememberSaveable { mutableStateOf<List<Kb>>(ArrayList()) }

    LaunchedEffect(courseTable) {
        when (courseTable) {
            is BaseStatus.ERROR -> {}
            is BaseStatus.LOADING -> {
                isLoading = true
            }

            is BaseStatus.SUCCESS -> {
                isLoading = false
                kbList = courseTable.data
            }
        }
    }

    Card(
        modifier.padding(8.dp),
        shape = baseRadius
    ) {
        Column(
            Modifier
                .padding(12.dp)
                .fillMaxWidth()
                .animateContentSize()
        ) {
            Text(title, style = MaterialTheme.typography.bodyLarge)
            Spacer(Modifier.height(12.dp))
            AnimatedVisibility(isLoading, modifier = Modifier.align(Alignment.CenterHorizontally)) {
                CircularProgressIndicator()
            }
            if (kbList.isEmpty()) {
                Column(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Icon(Icons.Default.Face, "")
                    Spacer(Modifier.height(12.dp))
                    Text(
                        empty,
                        style = MaterialTheme.typography.bodySmall,
                        textAlign = TextAlign.Center
                    )
                }
            } else {
                ListCourseTablePlus(kbList)
            }
//            }
        }
    }
}

@Composable
fun ListCourseTablePlus(kbList: List<Kb>) {
    val density = LocalDensity.current // 获取当前环境的密度信息
    kbList.forEachIndexed { index, kb ->
        key(kb) {
            val courseColor = Utils.getCourseColor(kb, SPUtils.getIns().getColorMapByCourse())
            var boxHeight by remember {
                mutableStateOf(0.dp)
            }
            Row(modifier = Modifier
                .fillMaxHeight()
                .onSizeChanged {
                    val dpValue = with(density) { it.height.toDp() } // 利用with作用域函数和toDp转换像素值为dp值
                    boxHeight = dpValue
                }) {
                Column(
                    modifier = Modifier
                        .width(4.dp)
                        .height(boxHeight)
                        .clip(RoundedCornerShape(4.dp))
                        .background(Color(courseColor))
                ) {}
                Spacer(modifier = Modifier.width(4.dp))
                Column(
                    modifier = Modifier
                        .fillMaxWidth(),
                ) {
                    Text("${kb.kcmc}｜${kb.jc}", style = MaterialTheme.typography.labelLarge)
                    Text(
                        "${kb.cdmc}（${Utils.kbJcTime(kb.jc)}）",
                        style = MaterialTheme.typography.labelSmall
                    )
                }
            }
            Spacer(modifier = Modifier.height(12.dp))
        }
    }
}

@Composable
fun ListCourseTable(kbList: List<Kb>) {
    var amKbList by remember { mutableStateOf<List<Kb>>(ArrayList()) }
    var pmKbList by remember { mutableStateOf<List<Kb>>(ArrayList()) }
    var nightKbList by remember { mutableStateOf<List<Kb>>(ArrayList()) }
    LaunchedEffect(kbList) {
        val amList = ArrayList<Kb>()
        val pmList = ArrayList<Kb>()
        val nightList = ArrayList<Kb>()
        kbList.forEach { kb ->
            val jc = kb.jc.replace("节", "").split("-")
            val jcSp = jc[0].toInt()..jc[1].toInt()
            if (jcSp.last <= 4) {
                //上午
                amList.add(kb)
            } else if (jcSp.last <= 10) {
                //下午
                pmList.add(kb)
            } else if (jcSp.last <= 13) {
                //晚上
                nightList.add(kb)
            }
        }
        amKbList = amList
        pmKbList = pmList
        nightKbList = nightList
    }

    Column {
        if (amKbList.isNotEmpty()) {
            TimeFrameOfByCourseTable("上午", amKbList)
        }
        if (pmKbList.isNotEmpty()) {
            TimeFrameOfByCourseTable("下午", pmKbList)
        }
        if (nightKbList.isNotEmpty()) {
            TimeFrameOfByCourseTable("晚上", nightKbList)
        }
    }
}

@Composable
fun TimeFrameOfByCourseTable(title: String, timeFrameKbList: List<Kb>) {
    Column(Modifier.fillMaxWidth()) {
        Text(title, style = MaterialTheme.typography.bodyLarge)
        Spacer(Modifier.height(4.dp))
        timeFrameKbList.forEach { kb ->
            val courseColor = Utils.getCourseColor(kb, SPUtils.getIns().getColorMapByCourse())

            Card(
                Modifier.fillMaxWidth(),
                shape = baseRadius,
                colors = CardDefaults.cardColors(
                    containerColor = Color(courseColor)
                )
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically, modifier = Modifier
                        .padding(8.dp)
                ) {
                    Text(
                        text = kb.jc,
                        color = Color.White,
                        style = MaterialTheme.typography.labelSmall
                    )
                    Spacer(modifier = Modifier.width(12.dp))
                    Column(
                        modifier = Modifier
                            .fillMaxWidth(),
                    ) {
                        Text(
                            kb.kcmc, style = MaterialTheme.typography.bodyMedium,
                            color = Color.White
                        )
                        Text(
                            "${kb.cdmc}（${Utils.kbJcTime(kb.jc)}）",
                            color = Color.White,
                            style = MaterialTheme.typography.labelMedium
                        )
                    }
                }
            }
            Spacer(modifier = Modifier.height(4.dp))
        }
    }
}
