package com.example.coursetable_compose.ui.screen

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Card
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.desktop.screenbean.MainPageScreen
import com.example.coursetable_compose.ui.activity.baseRadius
import com.example.coursetable_compose.ui.commen.BaseToolbar
import com.example.coursetable_compose.utils.SPUtils

@Composable
fun MainPageSettingScreen(navController: NavHostController, innerPadding: PaddingValues) {
    var modeMap by rememberSaveable {
        mutableStateOf(SPUtils.getIns().getMainPageModeMap())
    }

    Column(
        Modifier
            .fillMaxSize()
            .padding(top = innerPadding.calculateTopPadding())
            .verticalScroll(rememberScrollState())
    ) {
        BaseToolbar(navController, MainPageScreen.MainPageSettingScreen.label)

        modeMap.entries.forEach {
            key(it.key){
                Card(Modifier.fillMaxWidth().padding(horizontal = 12.dp, vertical = 4.dp), shape = baseRadius) {
                    Row(modifier = Modifier.fillMaxWidth().padding(12.dp), verticalAlignment = Alignment.CenterVertically){
                        Text(text = it.key, modifier = Modifier.weight(1f))
                        Switch(checked = it.value, onCheckedChange = {newValue->
                            val newMap= modeMap.toMutableMap()
                            newMap[it.key]=newValue
                            SPUtils.getIns().setMainPageModeMap(newMap)
                            modeMap=SPUtils.getIns().getMainPageModeMap()
                        })
                    }
                }
            }
        }



    }
}