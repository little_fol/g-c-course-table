package com.example.coursetable_compose.ui.screen

import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContract
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.KeyboardArrowRight
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.desktop.screenbean.MainPageScreen
import com.example.coursetable_compose.R
import com.mypotota.datamodel.base.BaseStatus
import com.example.coursetable_compose.ui.activity.MainActivity
import com.example.coursetable_compose.ui.activity.ScanQrCodeActivity
import com.example.coursetable_compose.ui.activity.baseRadius
import com.example.coursetable_compose.ui.commen.BaseToolbar
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.viewmodel.GroupCourseTableViewModel
import com.example.coursetable_compose.viewmodel.UserViewModel
import com.hjq.permissions.Permission
import com.hjq.permissions.XXPermissions
import com.tapsdk.lc.LCObject
import com.tapsdk.lc.LCUser
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


@Composable
fun GroupCourseTableScreen(navController: NavHostController, innerPadding: PaddingValues) {
    var readGroupCourseTableTip by remember {
        mutableStateOf(SPUtils.getIns().isReadGroupCourseTableTip())
    }
    //显示提示
    var showTip by remember {
        mutableStateOf(readGroupCourseTableTip)
    }
    //显示登录弹窗
    var showLoginDialog by remember {
        mutableStateOf(false)
    }
    //是否显示创建小组
    var showCreateDialog by remember {
        mutableStateOf(false)
    }
    val viewModel = viewModel<GroupCourseTableViewModel>()

    val userViewModel = viewModel<UserViewModel>()
    val updateUserInfoStatus=userViewModel.updateUserInfoStatus.collectAsState()
    LaunchedEffect(updateUserInfoStatus.value) {
        when(val status=updateUserInfoStatus.value){
            is BaseStatus.ERROR -> {
                Toast.makeText(navController.context, status.e, Toast.LENGTH_SHORT).show()
                showLoginDialog=true
            }
            is BaseStatus.LOADING -> {}
            is BaseStatus.SUCCESS -> {}
        }
    }


    LaunchedEffect(readGroupCourseTableTip) {
        if (readGroupCourseTableTip) {
            if (LCUser.currentUser() == null) {
                showLoginDialog = true
            } else {
                //获取数据
                userViewModel.updateUserInfo()
            }
        }
    }


    LaunchedEffect(Unit) {
        viewModel.getCurrentCreateGroup()
        viewModel.getCurrentUserJoinGroup()
    }

    Column(Modifier.fillMaxSize().padding(top = innerPadding.calculateTopPadding())) {
        BaseToolbar(navController, MainPageScreen.GroupCourseTable.label)

        Column(
            Modifier
                .fillMaxSize()
                .verticalScroll(rememberScrollState())
        ) {
            //创建的组
            CreateGroupCourseTableContent(navController) {
                showCreateDialog = true
            }
            //加入的组
            JoinGroupCourseTableContent(navController)
        }
    }

    //登录弹窗
    if (showLoginDialog) {
        LoginDialog({
            showLoginDialog = false
            viewModel.getCurrentCreateGroup()
        }) {
            showLoginDialog = false
            navController.popBackStack()
        }
    }


    //创建小组弹窗
    if (showCreateDialog) {
        CreateGroupDialog() {
            showCreateDialog = false
        }
    }


    //提示弹窗
    if (!showTip) {
        AlertDialog(onDismissRequest = {}, confirmButton = {
            Button(onClick = {
                SPUtils.getIns().setReadGroupCourseTableTip(true)
                showTip = true
                readGroupCourseTableTip = true
            }, shape = baseRadius) {
                Text(text = "我同意")
            }
        }, shape = baseRadius, dismissButton = {
            TextButton(onClick = {
                navController.popBackStack()
                showTip = true
            }, shape = baseRadius) {
                Text(text = "我不同意（返回）")
            }
        }, title = {
            Text(text = "小组课表使用须知/协议")
        }, text = {
            Text(text = "首先：\n\n如果你开启了小组课表后您的课表的数据将会上传至云端共小组中其他成员共享，如果你不启用该功能你的信息将不会上传云端，将完全在本地运行。（信息包括：课程信息，账号）\n\n在使用小组课表您应该明确它的工作原理：软件会将你的课程信息及账号打包上传至云端（传输安全）供其他成员查找及加入。\n\n你需要注意：\n在使用过程中，非小组成员是不可见的。\n此过程会将你的账号及课程信息上传，如果你非常忌讳这一点，请退出此功能。")
        })
    }


}

@Composable
fun CreateGroupDialog(onDismiss: () -> Unit) {
    var name by remember {
        mutableStateOf("")
    }
    val viewModel = viewModel<GroupCourseTableViewModel>()
    var isError by remember {
        mutableStateOf(false)
    }
    var errorMessage by remember {
        mutableStateOf("")
    }
    var loading by remember {
        mutableStateOf(false)
    }
    var loadingMessage by remember {
        mutableStateOf("")
    }
    val scope = rememberCoroutineScope()

    val baseCourseTableStatus = viewModel.baseCourseTableStatus.collectAsState()

    LaunchedEffect(baseCourseTableStatus.value) {
        when (baseCourseTableStatus.value) {
            is BaseStatus.ERROR -> {
                isError = true
                errorMessage = (baseCourseTableStatus.value as BaseStatus.ERROR).e
                loading = false
                scope.launch {
                    delay(1000)
                    isError = false
                    viewModel.reset()
                }
            }

            is BaseStatus.LOADING -> {
                loading = true
                loadingMessage = (baseCourseTableStatus.value as BaseStatus.LOADING).msg
            }

            is BaseStatus.SUCCESS -> {
                isError = false
                loading = true
                loadingMessage = "操作成功"
                scope.launch {
                    delay(1000)
                    loading = false
                    onDismiss()
                    viewModel.reset()
                }
            }

            null -> {
                isError = false
                loading = false
            }
        }
    }

    val createClick: () -> Unit = {
        if (name.isEmpty()) {
            isError = true
            errorMessage = "请输入分组名称"
            scope.launch {
                delay(1000)
                isError = false
            }
        } else {
            viewModel.createGroup(name)
        }
    }


    AlertDialog(onDismissRequest = {
        onDismiss()
    }, shape = baseRadius, confirmButton = { /*TODO*/ }, title = {
        Text(text = "创建分组")
    }, text = {
        Column(Modifier.fillMaxWidth()) {
            OutlinedTextField(value = name,
                onValueChange = { name = it },
                leadingIcon = {
                    Icon(Icons.Default.Edit, contentDescription = "")
                }, shape = baseRadius,
                modifier = Modifier.fillMaxWidth(),
                label = {
                    Text(text = "请输入分组名称")
                })
            Spacer(modifier = Modifier.height(12.dp))
            AnimatedVisibility(visible = !loading && !isError) {
                Button(
                    onClick = createClick,
                    modifier = Modifier.fillMaxWidth(),
                    shape = baseRadius
                ) {
                    Text(text = "创建")
                }
            }

            //显示正在加载中
            AnimatedVisibility(loading) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(12.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CircularProgressIndicator()
                    Text(text = loadingMessage)
                }
            }

            AnimatedVisibility(visible = isError) {
                Text(
                    text = errorMessage,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(12.dp),
                    textAlign = TextAlign.Center,
                    color = MaterialTheme.colorScheme.error
                )
            }
        }
    })
}

//登录弹窗
@Composable
fun LoginDialog(onDismiss: () -> Unit, exit: () -> Unit) {
    //用户ViewModel
    val viewModel = viewModel<UserViewModel>()
    val loginStatus = viewModel.loginStatus.collectAsState().value
    var account by rememberSaveable {
        mutableStateOf("")
    }
    var password by rememberSaveable {
        mutableStateOf("")
    }
    var isError by remember {
        mutableStateOf(false)
    }
    var errorMessage by remember {
        mutableStateOf("")
    }
    var loading by remember {
        mutableStateOf(false)
    }
    var loadingMessage by remember {
        mutableStateOf("")
    }
    val scope = rememberCoroutineScope()

    LaunchedEffect(key1 = loginStatus) {
        when (loginStatus) {
            is BaseStatus.ERROR -> {
                isError = true
                errorMessage = loginStatus.e
                loading = false
                scope.launch {
                    delay(1000)
                    isError = false
                }
            }

            is BaseStatus.LOADING -> {
                loading = true
                loadingMessage = loginStatus.msg
            }

            is BaseStatus.SUCCESS -> {
                isError = false
                loading = true
                loadingMessage = "登录成功"
                scope.launch {
                    delay(1000)
                    loading = false
                    onDismiss()
                }
            }

            null -> {
                isError = false
                loading = false
            }
        }
    }

    val loginClick: () -> Unit = {
        if (account.isEmpty()) {
            isError = true
            errorMessage = "请输入账号"
            scope.launch {
                delay(1000)
                isError = false
            }
        } else if (password.isEmpty()) {
            isError = true
            errorMessage = "请输入密码"
            scope.launch {
                delay(1000)
                isError = false
            }
        } else {
            //登录并注册
            viewModel.loginAndRegister(account, password)
        }
    }

    AlertDialog(onDismissRequest = {
        exit()
    }, shape = baseRadius, confirmButton = { }, text = {
        Column(Modifier.verticalScroll(rememberScrollState())) {
            Text(
                text = "在此之前需要你登录一个账号", style = MaterialTheme.typography.titleLarge
            )
            Spacer(modifier = Modifier.height(12.dp))
            Text(text = "登录时会检测你是否存在账号，如果不存在自动注册。一键登录是使用本地学号和密码实现一键登录（推荐使用此方法）")
            Spacer(modifier = Modifier.height(12.dp))
            OutlinedTextField(modifier = Modifier.fillMaxWidth(),
                shape = baseRadius,
                value = account,
                onValueChange = { account = it },
                leadingIcon = {
                    Icon(
                        Icons.Default.AccountCircle, contentDescription = ""
                    )
                },
                label = {
                    Text(text = "请输入账号")
                })
            Spacer(modifier = Modifier.height(4.dp))
            OutlinedTextField(modifier = Modifier.fillMaxWidth(),
                shape = baseRadius,
                value = password,
                onValueChange = { password = it },
                leadingIcon = {
                    Icon(
                        Icons.Default.AccountCircle, contentDescription = ""
                    )
                },
                label = {
                    Text(text = "请输入密码")
                })
            Spacer(modifier = Modifier.height(12.dp))
            AnimatedVisibility(visible = !isError && !loading) {
                Column {
                    Button(
                        onClick = loginClick,
                        shape = baseRadius,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        Text(text = "登录")
                    }
                    Button(
                        onClick = {
                            viewModel.loginAndRegister(
                                SPUtils.getIns().getAccount().toString(),
                                SPUtils.getIns().getPassword().toString()
                            )
                        },  shape = baseRadius, modifier = Modifier.fillMaxWidth()
                    ) {
                        Text(text = "一键登录（推荐）")
                    }
                }
            }

            //显示正在加载中
            AnimatedVisibility(loading) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(12.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CircularProgressIndicator()
                    Text(text = loadingMessage)
                }
            }

            AnimatedVisibility(visible = isError) {
                Text(
                    text = errorMessage,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(12.dp),
                    textAlign = TextAlign.Center,
                    color = MaterialTheme.colorScheme.error
                )
            }
        }
    })
}


//加入分组
@Composable
fun JoinGroupCourseTableContent(navController: NavHostController) {
    val viewModel = viewModel<GroupCourseTableViewModel>()
    val currentUserJoinGroupListStatus = viewModel.currentUserJoinGroupListStatus.collectAsState()
    var isShowJoinDialog by remember { mutableStateOf(false) }
    val context = LocalContext.current as MainActivity
    val xxPermissions = XXPermissions.with(context)
        .permission(Permission.CAMERA)


    val scope = rememberCoroutineScope()
    var isLoading by remember { mutableStateOf(false) }
    var loadingMessage by remember { mutableStateOf("") }
    var isError by remember { mutableStateOf(false) }
    var errorMessage by remember { mutableStateOf("") }
    var joiniGroupList by remember { mutableStateOf<List<LCObject>>(ArrayList()) }

    LaunchedEffect(currentUserJoinGroupListStatus.value) {
        coroutineScope {
            when (val status = currentUserJoinGroupListStatus.value) {
                is BaseStatus.ERROR -> {
                    isLoading = false
                    isError = true
                    errorMessage = status.e
                }

                is BaseStatus.LOADING -> {
                    isLoading = true
                    isError = false
                    loadingMessage = status.msg
                }

                is BaseStatus.SUCCESS -> {
                    isLoading = false
                    isError = false
                    joiniGroupList = status.data
                }
            }
        }
    }


    val launcher =
        rememberLauncherForActivityResult(object : ActivityResultContract<String, String>() {
            override fun createIntent(context: Context, input: String): Intent {
                //创建启动页面所需的Intent对象，传入需要传递的参数
                return Intent(context, ScanQrCodeActivity::class.java)
            }

            override fun parseResult(resultCode: Int, intent: Intent?): String {
                //页面回传的数据解析，相当于原onActivityResult方法
                val code = intent?.getStringExtra("code") ?: ""
                return if (resultCode == 102) code else ""
            }
        }) {
            if (it.isEmpty()) {
                Toast.makeText(context, "未能识别", Toast.LENGTH_SHORT).show()
                return@rememberLauncherForActivityResult
            }
            viewModel.queryGroup(it)
            isShowJoinDialog = true
        }


    Row(
        Modifier
            .fillMaxWidth()
            .padding(horizontal = 12.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            "我加入的小组",
            style = MaterialTheme.typography.titleLarge,
            modifier = Modifier.weight(1f)
        )
        Row {
            IconButton(onClick = {
                isShowJoinDialog = true
            }) {
                Icon(Icons.Default.Search, contentDescription = "")
            }
            IconButton(onClick = {
                if (XXPermissions.isGranted(context, Permission.CAMERA)) {
                    launcher.launch("")
                } else {
                    xxPermissions.request { strings, b ->
                        if (b) {
                            launcher.launch("")
                        }
                    }
                }
            }) {
                Icon(
                    painterResource(id = R.drawable.outline_document_scanner_24),
                    contentDescription = ""
                )
            }
        }
    }

    Column {

        //错误时
        AnimatedVisibility(visible = isError) {
            Card(modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp), shape = baseRadius, onClick = {}) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Icon(
                        Icons.Default.Info, contentDescription = ""
                    )
                    Text(text = errorMessage)
                }
            }
        }
        //加载中...
        AnimatedVisibility(visible = isLoading) {
            Card(modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp), shape = baseRadius, onClick = {}) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CircularProgressIndicator()
                    Text(text = loadingMessage)
                }
            }
        }

        //为空时
        AnimatedVisibility(visible = !isError && !isLoading && joiniGroupList.isEmpty()) {
            Card(modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp), shape = baseRadius, onClick = {}) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Icon(
                        painterResource(id = R.drawable.outline_not_interested_24),
                        contentDescription = ""
                    )
                    Text(text = "暂未加入小组")
                }
            }
        }

        //有数据时
        AnimatedVisibility(visible = !isError && !isLoading && joiniGroupList.isNotEmpty()) {
            Column {
                joiniGroupList.forEachIndexed { index, lcObject ->
                    key(lcObject.objectId) {
                        Card(modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 12.dp),
                            shape = baseRadius,
                            onClick = {
                                navController.navigate("${MainPageScreen.GroupCourseTableDetailScreen.name}/data=${lcObject.toJSONString()}")
                            }) {
                            Row(
                                Modifier
                                    .fillMaxWidth()
                                    .padding(12.dp),
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                Text(
                                    text = lcObject.getString("name"), modifier = Modifier.weight(
                                        1f
                                    )
                                )
                                Icon(
                                    Icons.AutoMirrored.Filled.KeyboardArrowRight,
                                    contentDescription = ""
                                )
                            }
                        }
                        Spacer(modifier = Modifier.height(4.dp))
                    }
                }
            }
        }
    }

    if (isShowJoinDialog) {
        JoinGroupDialog() {msg->
            if(msg.isNotEmpty()){
                Toast.makeText(navController.context, msg, Toast.LENGTH_SHORT).show()
            }
            isShowJoinDialog = false
        }
    }

}

@Composable
fun JoinGroupDialog(onDismiss: (msg:String) -> Unit) {
    val viewModel = viewModel<GroupCourseTableViewModel>()
    val baseCourseTableStatus = viewModel.baseCourseTableStatus.collectAsState().value
    var objectId by remember {
        mutableStateOf("")
    }
    val scope = rememberCoroutineScope()
    var isLoading by remember { mutableStateOf(false) }
    var loadingMessage by remember { mutableStateOf("") }
    var isError by remember { mutableStateOf(false) }
    var errorMessage by remember { mutableStateOf("") }

    LaunchedEffect(baseCourseTableStatus) {
        when (baseCourseTableStatus) {
            is BaseStatus.ERROR -> {
                isLoading = false
                isError = true
                errorMessage = baseCourseTableStatus.e
                onDismiss("${baseCourseTableStatus.e}")
                viewModel.reset()
            }

            is BaseStatus.LOADING -> {
                isLoading = true
                isError = false
                loadingMessage = baseCourseTableStatus.msg
            }

            is BaseStatus.SUCCESS -> {
                isLoading = false
                isError = false
                loadingMessage = "加入成功"
                viewModel.reset()
                viewModel.getCurrentUserJoinGroup()
                onDismiss("加入成功")
            }

            null -> {
                isError = false
                isLoading=false
            }
        }
    }

    val queryClick: () -> Unit = {
        if (objectId.isEmpty()) {
            isError = true
            errorMessage = "请输入小组ID"
            scope.launch {
                delay(1000)
                isError = false
            }
        } else {
            viewModel.queryGroup(objectId)
        }
    }

    AlertDialog(onDismissRequest = {
        if (!isLoading && !isError) {
            onDismiss("")
        }
    }, shape = baseRadius, confirmButton = { /*TODO*/ }, title = {
        Text(text = "加入小组", style = MaterialTheme.typography.titleLarge)
    }, text = {
        Column {
            OutlinedTextField(value = objectId,
                onValueChange = { objectId = it },
                leadingIcon = {
                    Icon(Icons.Default.Edit, contentDescription = "")
                },
                shape = baseRadius,
                modifier = Modifier.fillMaxWidth(),
                label = {
                    Text(text = "请输入小组ID")
                }
            )
            Spacer(modifier = Modifier.height(12.dp))
            AnimatedVisibility(visible = !isLoading && !isError) {
                Button(
                    onClick = queryClick,
                    modifier = Modifier.fillMaxWidth(),
                    shape = baseRadius
                ) {
                    Text(text = "查找并加入")
                }
            }

            //错误时
            AnimatedVisibility(visible = isError) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Icon(
                        Icons.Default.Info, contentDescription = ""
                    )
                    Text(text = errorMessage)
                }
            }
            //加载中...
            AnimatedVisibility(visible = isLoading) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CircularProgressIndicator()
                    Text(text = loadingMessage)
                }
            }
        }
    })
}


@Composable
fun CreateGroupCourseTableContent(navController: NavHostController, add: () -> Unit) {
    val viewModel = viewModel<GroupCourseTableViewModel>()
    val userCreateGroupState = viewModel.userCreateGroupState.collectAsState().value

    val scope = rememberCoroutineScope()
    var isLoading by remember { mutableStateOf(false) }
    var loadingMessage by remember { mutableStateOf("") }
    var isError by remember { mutableStateOf(false) }
    var errorMessage by remember { mutableStateOf("") }
    var createGroupList by remember { mutableStateOf<List<LCObject>>(ArrayList()) }

    LaunchedEffect(userCreateGroupState) {
        when (userCreateGroupState) {
            is BaseStatus.ERROR -> {
                isLoading = false
                isError = true
                errorMessage = userCreateGroupState.e
            }

            is BaseStatus.LOADING -> {
                isLoading = true
                isError = false
                loadingMessage = userCreateGroupState.msg
            }

            is BaseStatus.SUCCESS -> {
                isLoading = false
                isError = false
                createGroupList = userCreateGroupState.data
            }
        }
    }

    Column {
        Row(
            Modifier
                .fillMaxWidth()
                .padding(horizontal = 12.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                "我创建的小组",
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier.weight(1f)
            )
            IconButton(onClick = add) {
                Icon(Icons.Default.Add, contentDescription = "")
            }
        }


        //错误时
        AnimatedVisibility(visible = isError) {
            Card(modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp), shape = baseRadius, onClick = {}) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Icon(
                        Icons.Default.Info, contentDescription = ""
                    )
                    Text(text = errorMessage)
                }
            }
        }
        //加载中...
        AnimatedVisibility(visible = isLoading) {
            Card(modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp), shape = baseRadius, onClick = {}) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CircularProgressIndicator()
                    Text(text = loadingMessage)
                }
            }
        }

        //为空时
        AnimatedVisibility(visible = !isError && !isLoading && createGroupList.isEmpty()) {
            Card(modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp), onClick = {}) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Icon(
                        painterResource(id = R.drawable.outline_not_interested_24),
                        contentDescription = ""
                    )
                    Text(text = "暂未创建小组")
                }
            }
        }
        //有数据时
        AnimatedVisibility(visible = !isError && !isLoading && createGroupList.isNotEmpty()) {
            Column {
                createGroupList.forEachIndexed { index, lcObject ->
                    key(lcObject.objectId) {
                        Card(modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 12.dp),
                            shape = baseRadius,
                            onClick = {
                                navController.navigate("${MainPageScreen.GroupCourseTableDetailScreen.name}/data=${lcObject.toJSONString()}")
                            }) {
                            Row(
                                Modifier
                                    .fillMaxWidth()
                                    .padding(12.dp),
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                Text(
                                    text = lcObject.getString("name"), modifier = Modifier.weight(
                                        1f
                                    )
                                )
                                Icon(
                                    Icons.AutoMirrored.Filled.KeyboardArrowRight,
                                    contentDescription = ""
                                )
                            }
                        }
                        Spacer(modifier = Modifier.height(4.dp))
                    }
                }
            }
        }
    }

}
