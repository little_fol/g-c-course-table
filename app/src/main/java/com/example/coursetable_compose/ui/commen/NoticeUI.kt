package com.example.coursetable_compose.ui.commen

import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Notifications
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import com.example.coursetable_compose.R
import com.example.coursetable_compose.ui.activity.baseRadius
import com.tapsdk.lc.LCObject

@Composable
fun NoticeDialogUI(noticeLCObject: LCObject,dismiss:()->Unit, readNotice:(objectId:String)->Unit){
    AlertDialog({}, {
        OutlinedButton(dismiss, shape = baseRadius) {
            Text("我知道了")
        }
    }, shape=baseRadius,dismissButton = {
        Button({
            readNotice.invoke(noticeLCObject.objectId)
        }, shape = baseRadius) {
            Text("不再提醒")
        }
    }, icon = {
        Icon(
            painterResource(id = R.drawable.baseline_notifications_24), "", tint =
        MaterialTheme.colorScheme.primary)
    }, title = {
        Text(noticeLCObject.getString("title"))
    }, text = {
        SelectionContainer{
            Text(noticeLCObject.getString("message"))
        }
    })
}