package com.example.coursetable_compose.ui.screen

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.desktop.screenbean.MainPageScreen
import com.example.coursetable_compose.R
import com.example.coursetable_compose.ui.activity.baseRadius
import com.example.coursetable_compose.utils.APKVersionCodeUtils
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.utils.Utils
import com.tapsdk.lc.LCUser


@Composable
fun SettingPageScreen(parentNavController: NavController) {
    val context = LocalContext.current
    var isShowLogoutDialog by remember {
        mutableStateOf(false)
    }

    Column(Modifier.verticalScroll(rememberScrollState())) {
        Text(
            "设置",
            style = MaterialTheme.typography.headlineLarge,
            modifier = Modifier.padding(12.dp)
        )
        Text(
            "个性化",
            modifier = Modifier.padding(12.dp),
            style = MaterialTheme.typography.bodyLarge,
        )
        SettingItem(R.drawable.outline_color_lens_24,MainPageScreen.PaletteScreen.label){
            parentNavController.navigate(MainPageScreen.PaletteScreen.name)
        }
        SettingItem(R.drawable.outline_home_24,MainPageScreen.MainPageSettingScreen.label){
            parentNavController.navigate(MainPageScreen.MainPageSettingScreen.name)
        }
        SettingItem(R.drawable.outline_photo_24,MainPageScreen.CourTableSetting.label){
            parentNavController.navigate(MainPageScreen.CourTableSetting.name)
        }
        SettingItem(R.drawable.outline_widgets_24,MainPageScreen.WidgetSettingScreen.label){
            parentNavController.navigate(MainPageScreen.WidgetSettingScreen.name)
        }

//        Text(
//            "账号设置",
//            modifier = Modifier.padding(12.dp),
//            style = MaterialTheme.typography.bodyLarge
//        )
//        SettingItem(R.drawable.baseline_supervised_user_circle_24,MainPageScreen.AccountManagerScreen.label){
//            parentNavController.navigate(MainPageScreen.AccountManagerScreen.name)
//        }


        Text(
            "软件设置",
            modifier = Modifier.padding(12.dp),
            style = MaterialTheme.typography.bodyLarge
        )
        SettingItem(R.drawable.outline_settings_24,MainPageScreen.AppSettingScreen.label){
            parentNavController.navigate(MainPageScreen.AppSettingScreen.name)
        }
        Text(
            "意见反馈",
            modifier = Modifier.padding(12.dp),
            style = MaterialTheme.typography.bodyLarge
        )
        SettingItem(R.drawable.outline_feedback_24,MainPageScreen.FeedBackScreen.label){
            parentNavController.navigate(MainPageScreen.FeedBackScreen.name)
        }
        SettingItem(R.drawable.baseline_groups_24,"加入用户群"){
            Utils.copy(context,SPUtils.getIns().getQQGroup())
            Toast.makeText(context, "QQ群已复制", Toast.LENGTH_SHORT).show()
        }
        Text(
            "关于版本",
            modifier = Modifier.padding(12.dp),
            style = MaterialTheme.typography.bodyLarge
        )
        SettingItem(R.drawable.outline_developer_mode_24,"进入版本更新（${APKVersionCodeUtils.getVersionName(parentNavController.context)}）"){
            parentNavController.navigate(MainPageScreen.VersionManage.name)
        }
        SettingItem(R.drawable.outline_developer_board_24,"开源地址"){
            openBrowserByUrl(context, SPUtils.getIns().getOpenSourceAddress())
        }
        SettingItem(R.drawable.baseline_share_24,"分享此软件"){
            val intent = Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT,"我发现了一款宝藏软件，无任何广告看课表，内置小组课表，自定义课表颜色等，快来下载体验吧：https://www.pgyer.com/o4NgKp");
            intent.setType("text/plain");
            context.startActivity(Intent.createChooser(intent, "分享APP"));
        }
        SettingItem(R.drawable.baseline_code_24,MainPageScreen.OpenSourceSoftwareStatementScreen.label){
            parentNavController.navigate(MainPageScreen.OpenSourceSoftwareStatementScreen.name)
        }

        Button(
            {
                isShowLogoutDialog = true
            },
            colors = ButtonDefaults.buttonColors(containerColor = MaterialTheme.colorScheme.error),
            modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp),
            shape = baseRadius
        ) {
            Text("退出绑定")
        }

        //询问是否退出绑定
        if (isShowLogoutDialog) {
            AlertDialog(onDismissRequest = {
                isShowLogoutDialog = false
            }, confirmButton = {
                Button(onClick = {
                    isShowLogoutDialog = false
                    SPUtils.getIns().setAccount("")
                    SPUtils.getIns().setPassword("")
                    SPUtils.getIns().setCourseTable(null)
                    LCUser.logOut()
                    parentNavController.navigate(MainPageScreen.BindAccountScreen.name) {
                        popUpTo(MainPageScreen.HomeScreen.name) {
                            inclusive = true
                        }
                        launchSingleTop = true // 可选，如果详情页已经是栈顶则不会重新创建
                    }
                }) {
                    Text(text = "确定")
                }
            }, dismissButton = {
                OutlinedButton(onClick = {
                    isShowLogoutDialog = false
                }) {
                    Text(text = "取消")
                }
            }, title = {
                Text(text = "温馨提醒")
            }, text = {
                Text(text = "您确定要退出绑定吗？")
            })
        }
    }
}

@Composable
fun SettingItem(icon:Int,title:String,click:()->Unit){
    TextButton(click,
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 12.dp),
        shape = baseRadius
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                painter = painterResource(icon),
                ""
            )
            Spacer(Modifier.width(8.dp))
            Text(title)
        }
    }
}

fun openBrowserByUrl(context: Context, url: String) {
    val uri = Uri.parse(url)
    val intent = Intent(Intent.ACTION_VIEW, uri)
    context.startActivity(intent)
}
