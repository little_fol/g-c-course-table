package com.example.coursetable_compose.ui.activity

import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.glance.appwidget.updateAll
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.desktop.screenbean.MainPageScreen
import com.example.coursetable_compose.ui.commen.MainUI
import com.example.coursetable_compose.ui.glancewidget.CourTableWidget
import com.example.coursetable_compose.ui.screen.AccountManagerScreen
import com.example.coursetable_compose.ui.screen.AddFeedBackScreen
import com.example.coursetable_compose.ui.screen.AppSettingScreen
import com.example.coursetable_compose.ui.screen.BindAccountScreen
import com.example.coursetable_compose.ui.screen.CourTableSetting
import com.example.coursetable_compose.ui.screen.FeedBackScreen
import com.example.coursetable_compose.ui.screen.GroupCourseTableDetailScreen
import com.example.coursetable_compose.ui.screen.GroupCourseTableScreen
import com.example.coursetable_compose.ui.screen.GroupMemberCourseTableScreen
import com.example.coursetable_compose.ui.screen.MainPageSettingScreen
import com.example.coursetable_compose.ui.screen.MainScreen
import com.example.coursetable_compose.ui.screen.MineGradesScreen
import com.example.coursetable_compose.ui.screen.OpenSourceSoftwareStatementScreen
import com.example.coursetable_compose.ui.screen.PaletteScreen
import com.example.coursetable_compose.ui.screen.VersionManageScreen
import com.example.coursetable_compose.ui.screen.WidgetSettingScreen
import com.example.coursetable_compose.ui.theme.CourseTablecomposeTheme
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.utils.SchoolDateUtils
import com.example.coursetable_compose.viewmodel.AppControlsViewModel
import com.example.coursetable_compose.viewmodel.NoticeViewModel
import com.example.coursetable_compose.viewmodel.VersionViewModel
import com.tapsdk.lc.LCObject


class MainActivity : ComponentActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        //开线程执行静默刷新课表
        setContent {
            CourseTablecomposeTheme {
                Scaffold(modifier = Modifier.fillMaxSize()) { innerPadding ->
                    AndroidMain(innerPadding)
                    RunTask(applicationContext)
                }
            }
        }
    }


}

@Composable
private fun RunTask(context: Context) {
    LaunchedEffect(key1 = Unit) {
        val widget = CourTableWidget()
        widget.updateAll(context)
    }
}


var baseRadius = RoundedCornerShape(SPUtils.getIns().getRadius().dp)

@Composable
private fun AndroidMain(innerPadding: PaddingValues) {
    val appControlsViewModel = viewModel<AppControlsViewModel>()
    val noticeViewModel = viewModel<NoticeViewModel>()
    val versionViewModel = viewModel<VersionViewModel>()

    val appControlsStatus = appControlsViewModel.appControlsStatus.collectAsState()
    val newNotice = noticeViewModel.newNotice.collectAsState()
    //根据状态分辨主页
    val navController = rememberNavController()

    val checkCurrentVersionStatus = versionViewModel.checkCurrentVersionStatus.collectAsState()
    val checkUpdateStatus = versionViewModel.checkUpdateStatus.collectAsState()

    MainUI(
        SPUtils.getIns().getAvailable(),
        appControlsStatus.value,
        newNotice.value,
        checkCurrentVersionStatus.value,
        checkUpdateStatus.value,
        innerPadding,
        SPUtils.getIns().getReadNotice(),
        { usable, privacyPolicy, feedbackNoticeForUse, openSourceAddress, autoRefresh, qqGroup ->
            SPUtils.getIns().setPrivacyPolicy(privacyPolicy)
            SPUtils.getIns().setAvailable(usable)
            SPUtils.getIns().setFeedbackNoticeForUse(feedbackNoticeForUse)
            SPUtils.getIns().setOpenSourceAddress(openSourceAddress)
            SPUtils.getIns().setShowAutoRefresh(autoRefresh)
            SPUtils.getIns().setQQGroup(qqGroup)
        },
        readNotice = {
            SPUtils.getIns().setReadNotice(it)
        }) {
        NavHost(
            navController = navController,
            startDestination = if (SPUtils.getIns()
                    .getCourseTable() == null
            ) MainPageScreen.BindAccountScreen.name else MainPageScreen.HomeScreen.name
        ) {
            composable(MainPageScreen.HomeScreen.name) { MainScreen(navController) }
            composable(MainPageScreen.BindAccountScreen.name) { BindAccountScreen(navController,innerPadding) }
            composable(MainPageScreen.PaletteScreen.name) { PaletteScreen(navController,innerPadding) }
            composable(MainPageScreen.VersionManage.name) { VersionManageScreen(navController,innerPadding) }
            composable(MainPageScreen.FeedBackScreen.name) { FeedBackScreen(navController,innerPadding) }
            composable(MainPageScreen.AddFeedBackScreen.name) { AddFeedBackScreen(navController,innerPadding) }
            composable(MainPageScreen.GroupCourseTable.name) { GroupCourseTableScreen(navController,innerPadding) }
            composable(MainPageScreen.MineGradesScreen.name) { MineGradesScreen(navController,innerPadding) }
            composable(MainPageScreen.CourTableSetting.name) { CourTableSetting(navController,innerPadding) }
            composable(MainPageScreen.AccountManagerScreen.name) { AccountManagerScreen(navController,innerPadding) }
            composable(MainPageScreen.MainPageSettingScreen.name) {
                MainPageSettingScreen(
                    navController,innerPadding
                )
            }
            composable(MainPageScreen.AppSettingScreen.name) { AppSettingScreen(navController,innerPadding) }
            composable(MainPageScreen.OpenSourceSoftwareStatementScreen.name) {
                OpenSourceSoftwareStatementScreen(
                    navController,innerPadding
                )
            }
            composable(MainPageScreen.WidgetSettingScreen.name) { WidgetSettingScreen(navController,innerPadding) }
            composable("${MainPageScreen.GroupMemberCourseTableScreen.name}/data={data}") {
                val data = it.arguments!!.getString("data")
                val lcObject = LCObject.parseLCObject(data)
                GroupMemberCourseTableScreen(navController, lcObject,innerPadding)
            }
            composable("${MainPageScreen.GroupCourseTableDetailScreen.name}/data={data}") {
                val data = it.arguments!!.getString("data")
                val lcObject = LCObject.parseLCObject(data)
                GroupCourseTableDetailScreen(navController, lcObject,innerPadding)
            }
        }
    }
}