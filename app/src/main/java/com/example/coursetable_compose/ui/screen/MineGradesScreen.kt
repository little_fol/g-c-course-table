package com.example.coursetable_compose.ui.screen

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.pulltorefresh.PullToRefreshContainer
import androidx.compose.material3.pulltorefresh.rememberPullToRefreshState
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.desktop.screenbean.MainPageScreen
import com.example.coursetable_compose.ui.activity.baseRadius
import com.example.coursetable_compose.ui.commen.BaseToolbar
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.viewmodel.GradesViewModel
import com.mypotota.datamodel.base.BaseStatus
import com.mypotota.datamodel.bean.GradesBean
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MineGradesScreen(navController: NavHostController, innerPadding: PaddingValues) {
    val viewModel = viewModel<GradesViewModel>()
    val semesterIndex by viewModel.currentSemesterPosition.collectAsState()
    val sheetState = rememberModalBottomSheetState()
    val scope = rememberCoroutineScope()
    var showBottomSheet by remember { mutableStateOf(false) }

    Column(
        Modifier
            .fillMaxSize().padding(top = innerPadding.calculateTopPadding())
    ) {
        BaseToolbar(navController, MainPageScreen.MineGradesScreen.label)
        MineGradesContent(Modifier.weight(1f))
        TextButton(onClick = {
            showBottomSheet = true
        }, modifier = Modifier.fillMaxWidth()) {
            Icon(Icons.Default.KeyboardArrowUp, contentDescription = "")
            Text(text = viewModel.allSemesterList[semesterIndex].name)
        }
    }

    if (showBottomSheet) {
        ModalBottomSheet(
            onDismissRequest = {
                showBottomSheet = false
            },
            sheetState = sheetState
        ) {
            Column(
                Modifier
                    .verticalScroll(rememberScrollState())
            ) {
                viewModel.allSemesterList.forEachIndexed { index, gradesSemester ->
                    key(index) {
                        Row(
                            Modifier
                                .fillMaxWidth()
                                .height(56.dp)
                                .selectable(
                                    selected = (index == semesterIndex),
                                    onClick =
                                    {
                                        viewModel.changeCurrentSemester(index)
                                        scope
                                            .launch { sheetState.hide() }
                                            .invokeOnCompletion {
                                                if (!sheetState.isVisible) {
                                                    showBottomSheet = false
                                                }
                                            }
                                    },
                                    role = Role.RadioButton
                                )
                                .padding(horizontal = 16.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            RadioButton(
                                selected = (index == semesterIndex),
                                onClick = null // null recommended for accessibility with screenreaders
                            )
                            Text(
                                text = gradesSemester.name,
                                style = MaterialTheme.typography.bodyLarge,
                                modifier = Modifier.padding(start = 16.dp)
                            )
                        }
                    }
                }
            }
        }
    }


}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MineGradesContent(modifier: Modifier) {
    val viewModel = viewModel<GradesViewModel>()
    val gradesState by viewModel.gradesState.collectAsState()
    var isReadGradesRefresh by remember {
        mutableStateOf(SPUtils.getIns().isReadGradesRefresh)
    }

    val refreshState = rememberPullToRefreshState()
    if (refreshState.isRefreshing) {
        LaunchedEffect(true) {
            viewModel.changeCurrentSemester(viewModel.currentSemesterPosition.value)
        }
    }

    Box(
        modifier
            .nestedScroll(refreshState.nestedScrollConnection)
            .clipToBounds()
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally, modifier = Modifier
                .fillMaxWidth()
                .verticalScroll(rememberScrollState())
        ) {
            AnimatedVisibility(visible = isReadGradesRefresh) {
                Card(
                    onClick = {
                        SPUtils.getIns().isReadGradesRefresh = false
                        isReadGradesRefresh = SPUtils.getIns().isReadGradesRefresh
                    }, modifier = Modifier
                        .padding(horizontal = 12.dp), shape = baseRadius
                ) {
                    Row(Modifier.padding(horizontal = 12.dp, vertical = 4.dp)) {
                        Text(text = "下拉可刷新成绩列表哟", Modifier.weight(1f))
                        Icon(Icons.Default.Close, contentDescription = "close")
                    }
                }
            }
            when (val state = gradesState) {
                is BaseStatus.LOADING -> {
                    refreshState.startRefresh()
                    CircularProgressIndicator()
                    Text(text = state.msg)
                }

                is BaseStatus.ERROR -> {
                    Icon(Icons.Default.Info, contentDescription = "info")
                    Text(text = state.e)
                    refreshState.endRefresh()
                }

                is BaseStatus.SUCCESS -> {
                    MineGradesSuccess(state)
                    refreshState.endRefresh()
                }
            }
        }
        PullToRefreshContainer(
            state = refreshState,
            modifier = Modifier.align(Alignment.TopCenter)
        )
    }
}

@Composable
fun MineGradesSuccess(state: BaseStatus.SUCCESS<List<GradesBean.Item>>) {
    //显示普通的列表
    MineGradesList(state.data)
    //显示折线图分布
}

@Composable
fun MineGradesList(data: List<GradesBean.Item>) {
    Text(
        text = "成绩",
        style = MaterialTheme.typography.titleLarge,
        modifier = Modifier
            .fillMaxWidth()
            .padding(12.dp)
    )
    AnimatedVisibility(visible = data.isEmpty()) {
        Text(
            text = "暂无成绩列表", modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp)
        )
    }
    data.forEachIndexed { index, item ->
        key(index) {
            Card(
                shape = baseRadius, modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 6.dp, horizontal = 12.dp)
            ) {
                Box(
                    Modifier
                        .fillMaxWidth()
                        .padding(12.dp)
                ) {
                    Column(Modifier.fillMaxWidth()) {
                        Text(
                            text = item.kcmc,
                            style = MaterialTheme.typography.bodyLarge,
                            fontWeight = FontWeight.Bold,
                            color = MaterialTheme.colorScheme.primary
                        )
                        Spacer(Modifier.height(12.dp))
                        Text(
                            text = "任教老师：${item.jsxm}",
                            style = MaterialTheme.typography.labelMedium
                        )
                        Row(Modifier.fillMaxWidth()) {
                            Column(Modifier.weight(1f)) {
                                Text(
                                    text = "课程性质：${
                                        if (item.kcgsmc == null) {
                                            "专业基础必修课"
                                        } else {
                                            item.kcgsmc
                                        }
                                    }",
                                    style = MaterialTheme.typography.labelMedium,
                                    overflow = TextOverflow.Ellipsis,
                                    maxLines = 1
                                )
                                Text(
                                    text = "课程标记：${item.kcbj}",
                                    style = MaterialTheme.typography.labelMedium,
                                    overflow = TextOverflow.Ellipsis,
                                    maxLines = 1
                                )
                                Text(
                                    text = "成绩性质：${item.ksxz}",
                                    style = MaterialTheme.typography.labelMedium,
                                    overflow = TextOverflow.Ellipsis,
                                    maxLines = 1
                                )
                                Text(
                                    text = "学分：${item.xf}",
                                    style = MaterialTheme.typography.labelMedium,
                                    overflow = TextOverflow.Ellipsis,
                                    maxLines = 1
                                )
                            }
                            Column(Modifier.weight(1f)) {
                                Text(
                                    text = "考核方式：${item.khfsmc}",
                                    style = MaterialTheme.typography.labelMedium,
                                    overflow = TextOverflow.Ellipsis,
                                    maxLines = 1
                                )
                                Text(
                                    text = "课程类别：${item.kclbmc}",
                                    style = MaterialTheme.typography.labelMedium,
                                    overflow = TextOverflow.Ellipsis,
                                    maxLines = 1
                                )
                                Text(
                                    text = "课程所属：${item.kkbmmc}",
                                    style = MaterialTheme.typography.labelMedium,
                                    overflow = TextOverflow.Ellipsis,
                                    maxLines = 1
                                )
                                Text(
                                    text = "绩点：${item.jd}",
                                    style = MaterialTheme.typography.labelMedium,
                                    overflow = TextOverflow.Ellipsis,
                                    maxLines = 1
                                )
                            }
                        }
                    }
                    Text(
                        text = item.cj, Modifier.align(Alignment.TopEnd),
                        style = MaterialTheme.typography.bodyLarge,
                        fontWeight = FontWeight.Bold,
                        color = MaterialTheme.colorScheme.primary
                    )
                }
            }
        }
    }
}
