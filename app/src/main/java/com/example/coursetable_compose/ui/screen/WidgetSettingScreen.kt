package com.example.coursetable_compose.ui.screen

import android.widget.Toast
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.glance.appwidget.updateAll
import androidx.navigation.NavHostController
import com.desktop.screenbean.MainPageScreen
import com.example.coursetable_compose.R
import com.example.coursetable_compose.ui.activity.baseRadius
import com.example.coursetable_compose.ui.commen.BaseToolbar
import com.example.coursetable_compose.ui.glancewidget.CourTableWidget
import com.hjq.permissions.OnPermissionCallback
import com.hjq.permissions.Permission
import com.hjq.permissions.XXPermissions
import kotlinx.coroutines.launch


@OptIn(ExperimentalFoundationApi::class)
@Composable
fun WidgetSettingScreen(navController: NavHostController, innerPadding: PaddingValues) {
    val context= navController.context
    val scope= rememberCoroutineScope()

    var widgetIncludeStepDialog by remember {
        mutableStateOf(false)
    }

    val stepList= listOf(
        R.drawable.widget_step1,
        R.drawable.widget_step2,
        R.drawable.widget_step3,
        R.drawable.widget_step4,
        R.drawable.widget_step5
    )


    Column(
        Modifier
            .fillMaxSize()
            .padding(top = innerPadding.calculateTopPadding())) {
        BaseToolbar(navController, MainPageScreen.WidgetSettingScreen.label)
        Column(
            Modifier
                .verticalScroll(rememberScrollState())
                .padding(12.dp)) {


            TextButton(
                onClick = {
                    widgetIncludeStepDialog=true
                },
                modifier = Modifier.fillMaxWidth(),
                shape = baseRadius
            ) {
                Text(text = "如何添加小组件？",Modifier.fillMaxWidth(), textAlign = TextAlign.Start)
            }

            TextButton(
                onClick = {
                    XXPermissions.startPermissionActivity(context)
                },
                modifier = Modifier.fillMaxWidth(),
                shape = baseRadius
            ) {
                Column() {
                    Text(
                        text = "小组件无法刷新？",
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Left,
                        style = MaterialTheme.typography.bodyLarge
                    )
                    Text(
                        text = "由于Android系统原因，软件无法在后台运行，你可以尝试关闭此软件的省电策略让软件在后台运行（通常在：应用信息>耗电与后台>允许后台运行）即可。（点击此处申请权限）",
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Left,
                        style = MaterialTheme.typography.labelMedium
                    )
                }
            }


            TextButton(
                onClick = {
                    XXPermissions.with(context)
                        .permission(Permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)
                        .request(OnPermissionCallback { permissions, allGranted ->
                            if (allGranted) {
                                Toast.makeText(
                                    context,
                                    "已开启",
                                    Toast.LENGTH_SHORT
                                ).show()
                                return@OnPermissionCallback
                            }
                            Toast.makeText(
                                context,
                                "未同意开启忽略电池优化权限",
                                Toast.LENGTH_SHORT
                            ).show()
                        })
                },
                modifier = Modifier.fillMaxWidth(),
                shape = baseRadius
            ) {
                Column() {
                    Text(
                        text = "开启上项还是无法定时刷新？",
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Left,
                        style = MaterialTheme.typography.bodyLarge
                    )
                    Text(
                        text = "如果开启上项还是无法自动刷新，你可以尝试开启忽略电池优化权限尝试，如果还是无法刷新请在意见反馈中进行反馈，并说明你的设备及Android版本。这对修复此问题至关重要（点击此处申请权限）",
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Left,
                        style = MaterialTheme.typography.labelMedium
                    )
                }
            }



            TextButton(
                onClick = {
                    scope.launch {
                        val widget = CourTableWidget()
                        widget.updateAll(context)
                        Toast.makeText(context, "已刷新，请注意小组件是否刷新", Toast.LENGTH_SHORT)
                            .show()
                    }
                },
                modifier = Modifier.fillMaxWidth(),
                shape = baseRadius
            ) {
                Column {
                    Text(
                        text = "小组件延迟？",
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Left,
                        style = MaterialTheme.typography.bodyLarge
                    )
                    Text(
                        text = "由于Android系统限制，小组件不能即时刷新，因为这很耗电，所以小组件理所应当会有刷新延迟，理论上此小组件只需要一天刷新一次就行，如果出现课程调整后小组件不能即时更新，你可以点击此处手动强制刷新，你也可以注意小组件下方刷新时间辨别出小组件是否正常刷新了。（点击此处强制刷新）",
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Left,
                        style = MaterialTheme.typography.labelMedium
                    )
                    Spacer(modifier = Modifier.height(12.dp))
                    Text(
                        text = "强制刷新后如果没有生效，尝试完全退出退出软件，重新打开，再次刷新即可。",
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Left,
                        style = MaterialTheme.typography.labelMedium
                    )
                    Spacer(modifier = Modifier.height(12.dp))
                    Text(
                        text = "如果小组件出现无法加载微件的时候移除小组件重新添加即可",
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Left,
                        style = MaterialTheme.typography.labelMedium
                    )
                    Spacer(modifier = Modifier.height(12.dp))
                    Text(
                        text = "小组件设定是每隔6小时会刷新一次，请留意小组件是否刷新。",
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Left,
                        style = MaterialTheme.typography.labelMedium
                    )

                }
            }










        }










    }
    if(widgetIncludeStepDialog){
        val pageState= rememberPagerState(pageCount = {stepList.size})
        AlertDialog(
            onDismissRequest = {
                widgetIncludeStepDialog=false
            },
            confirmButton = { 
                TextButton(onClick = {
                    widgetIncludeStepDialog=false
                }) {
                    Text(text = "我知道了")
                }
            },
            text = {
                HorizontalPager(state = pageState) {
                    Image(
                        painter = painterResource(id =stepList[it]),
                        contentDescription ="",
                        modifier = Modifier.clip(baseRadius)
                    )
                }
            }
        )
    }
}