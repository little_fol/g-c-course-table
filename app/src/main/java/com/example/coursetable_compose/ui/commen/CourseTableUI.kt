package com.example.coursetable_compose.ui.commen

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ScrollableTabRow
import androidx.compose.material3.Tab
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.unit.toSize
import androidx.compose.ui.util.fastForEachIndexed
import com.example.coursetable_compose.constant.COURSE_TABLE_ITEM_HEIGHT
import com.example.coursetable_compose.constant.COURSE_TABLE_ITEM_WIDTH
import com.example.coursetable_compose.ui.activity.baseRadius
import com.example.coursetable_compose.utils.Utils
import com.mypotota.datamodel.bean.Kb
import com.mypotota.datamodel.bean.Sjk

@Composable
fun KBDetail(currentSelectKB: Kb?, courseColor: Color, onClick: (data: String) -> Unit) {
    currentSelectKB ?: return
    Card(
        colors = CardDefaults.cardColors(containerColor = courseColor)
    ) {
        Column(Modifier.padding(24.dp)) {
            Text(
                currentSelectKB.kcmc,
                style = MaterialTheme.typography.titleLarge,
                fontWeight = FontWeight(1000),
                color = Color.White
            )
            Spacer(Modifier.height(12.dp))
            Text("授课老师：${currentSelectKB.xm}", color = Color.White)
            Spacer(Modifier.height(4.dp))
            Text(
                "教室：${currentSelectKB.cdmc}(${currentSelectKB.cdlbmc})【${currentSelectKB.jc}】",
                color = Color.White
            )
            Spacer(Modifier.height(4.dp))
            Text("教学楼：${currentSelectKB.lh}", color = Color.White)
            Spacer(Modifier.height(4.dp))
            Text("周次：${currentSelectKB.xqjmc}", color = Color.White)
            Spacer(Modifier.height(4.dp))
            Text("方向：${currentSelectKB.zyfxmc}", color = Color.White)
            Spacer(Modifier.height(4.dp))
            Text("课程性质：${currentSelectKB.kcxz}", color = Color.White)
            Spacer(Modifier.height(4.dp))
            Text("考核方式：${currentSelectKB.khfsmc}", color = Color.White)
            Spacer(Modifier.height(4.dp))
            Text("上课班级：${currentSelectKB.jxbzc}", color = Color.White)
            Spacer(Modifier.height(4.dp))
            Row {
                Spacer(Modifier.weight(1f))
                Button({
                    val stringBuilder = StringBuilder().apply {
                        append("${currentSelectKB.kcmc}  ")
                        append("授课老师：${currentSelectKB.xm}  ")
                        append("教室：${currentSelectKB.cdmc}(${currentSelectKB.cdlbmc})【${currentSelectKB.jc}】  ")
                        append("教学楼：${currentSelectKB.lh}  ")
                        append("周次：${currentSelectKB.xqjmc}  ")
                        append("方向：${currentSelectKB.zyfxmc}  ")
                        append("课程性质：${currentSelectKB.kcxz}")
                        append("考核方式：${currentSelectKB.khfsmc}  ")
                        append("上课班级：${currentSelectKB.jxbzc}  ")
                    }
                    onClick.invoke(stringBuilder.toString())
                }) {
                    Text("复制")
                }
            }
        }
    }
}

//其他课程
@Composable
fun OtherCourseList(sjkList: List<Sjk>?) {
    if (sjkList.isNullOrEmpty()) {
        Column {
            Text(
                text = "暂无其他课程需要学习",
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center
            )
        }
    } else {
        Column {
            sjkList.forEach {
                OtherCourseCard(it)
            }
        }
    }
}

//其他课程卡片
@Composable
fun OtherCourseCard(it: Sjk) {
    Card(modifier = Modifier.padding(horizontal = 12.dp, vertical = 4.dp), shape = baseRadius) {
        SelectionContainer {
            Column(Modifier.padding(12.dp)) {
                Text(text = it.kcmc, fontWeight = FontWeight(1000), fontSize = 16.sp)
                Text(text = "讲师：${it.jsxm}", fontSize = 12.sp)
                Text(
                    text = it.qtkcgs,
                    color = Color(0xff888888),
                    fontSize = 12.sp,
                    lineHeight = 12.sp
                )
            }
        }
    }
}


//课表侧栏
@Composable
fun LeftSideBar(sideBarList: List<String>?, isShowLeftTime: Boolean) {
    Column {
        (sideBarList ?: ArrayList()).fastForEachIndexed { index, item ->
            key(index) {
                Column(
                    Modifier
                        .height(COURSE_TABLE_ITEM_HEIGHT)
                        .width(COURSE_TABLE_ITEM_WIDTH),
                    verticalArrangement = Arrangement.Center
                ) {
                    Text(
                        text = (index + 1).toString(),
                        fontWeight = FontWeight(1000),
                        fontSize = 12.sp,
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Center
                    )
                    AnimatedVisibility(visible = isShowLeftTime) {
                        Text(
                            lineHeight = 11.sp,
                            modifier = Modifier.fillMaxWidth(),
                            text = item,
                            fontSize = 10.sp,
                            color = Color(0xff888888),
                            textAlign = TextAlign.Center,
                        )
                    }
                }
            }
        }
    }
}


@Composable
fun TabLayoutView(
    selectCurrentWeek: State<Int>,
    weekCount: State<Int>,
    tabClick: (i: Int) -> Unit
) {
    if (weekCount.value == 0) {
        return
    }
    //tab点击事件
    ScrollableTabRow(selectedTabIndex = selectCurrentWeek.value, containerColor=Color(0x00000000)) {
        for (i in 0..weekCount.value) {
            key(i) {
                Tab(
                    selected = selectCurrentWeek.value == i,
                    onClick = { tabClick.invoke(i) },
                    Modifier.background(Color(0x00000000)),
                    text = {
                        Text(
                            text = "第${(i + 1)}周",
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis
                        )
                    }
                )
            }
        }
    }
}


@Composable
fun CourseTopTitle(
    weekMonthMap: State<Map<String, String>?>,
    currentDate: String,
    radius: RoundedCornerShape,
    isShowTopDate: Boolean
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        if (weekMonthMap.value == null) {
//            CircularProgressIndicator(modifier = Modifier.padding(12.dp))
        } else {
            Text(
                text = weekMonthMap.value!!["monthValue"].toString(), modifier = Modifier.width(
                    COURSE_TABLE_ITEM_WIDTH
                ), color = MaterialTheme.colorScheme.primary,
                textAlign = TextAlign.Center
            )
            val monday = weekMonthMap.value!!["monday"].toString()
            WeekTitleCard(
                "周一",
                monday,
                monday.equals(currentDate),
                Modifier.weight(1f),
                radius,
                isShowTopDate
            )

            val tuesday = weekMonthMap.value!!["tuesday"].toString()
            WeekTitleCard(
                "周二",
                tuesday,
                tuesday.equals(currentDate),
                Modifier.weight(1f),
                radius,
                isShowTopDate
            )

            val wednesday = weekMonthMap.value!!["wednesday"].toString()
            WeekTitleCard(
                "周三",
                wednesday,
                wednesday.equals(currentDate),
                Modifier.weight(1f),
                radius,
                isShowTopDate
            )

            val thursday = weekMonthMap.value!!["thursday"].toString()
            WeekTitleCard(
                "周四",
                thursday,
                thursday.equals(currentDate),
                Modifier.weight(1f),
                radius,
                isShowTopDate
            )

            val friday = weekMonthMap.value!!["friday"].toString()
            WeekTitleCard(
                "周五",
                friday,
                friday.equals(currentDate),
                Modifier.weight(1f),
                radius,
                isShowTopDate
            )

            val saturday = weekMonthMap.value!!["saturday"].toString()
            WeekTitleCard(
                "周六",
                saturday,
                saturday.equals(currentDate),
                Modifier.weight(1f),
                radius,
                isShowTopDate
            )

            val sunday = weekMonthMap.value!!["sunday"].toString()
            WeekTitleCard(
                "周日",
                sunday,
                sunday.equals(currentDate),
                Modifier.weight(1f),
                radius,
                isShowTopDate
            )


        }
    }


}

@Composable
fun WeekTitleCard(
    title: String,
    content: String,
    isCheck: Boolean,
    modifier: Modifier,
    radius: RoundedCornerShape,
    isShowTopDate: Boolean
) {
    Column(
        modifier
            .clip(radius)
            .fillMaxWidth()
            .background(
                if (!isCheck) Color.Unspecified else MaterialTheme.colorScheme.primary
            ),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(modifier = Modifier.padding(4.dp)) {
            Text(
                text = title,
                style = MaterialTheme.typography.labelSmall,
                color = if (!isCheck) Color.Unspecified else Color.White
            )
            AnimatedVisibility(visible = isShowTopDate) {
                Text(
                    text = content,
                    style = MaterialTheme.typography.labelSmall,
                    color = if (!isCheck) Color.Unspecified else Color.White
                )
            }
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CourseTableViewPlus(
    modifier: Modifier,
    value: List<Kb>?,
    weekCountDay: Int,
    map: HashMap<String, Long>,
    onClick: (kb: Kb) -> Unit
) {
    if (value.isNullOrEmpty()) {
        return
    }
    var boxSize by remember { mutableStateOf(Size.Zero) }
    Box(
        modifier
            .fillMaxSize()
            .height(COURSE_TABLE_ITEM_HEIGHT * 13)
            .onGloballyPositioned { coordinates ->
                boxSize = coordinates.size.toSize()
            }) {
        value.forEachIndexed { index, kb ->
            val itemWidth = boxSize.width / weekCountDay

            val jc = kb.jc.replace("节", "").split("-")
            val courseJc = jc[0].toInt()..jc[jc.size - 1].toInt()

            var itemTop = (courseJc.first - 1) * COURSE_TABLE_ITEM_HEIGHT.value
            var itemBottom = courseJc.last * COURSE_TABLE_ITEM_HEIGHT.value

            //识别是周几
            var itemRight = 0f
            var itemLeft = 0f
            when (kb.xqjmc) {
                "星期一" -> {
                    itemRight = itemWidth * 1f
                }

                "星期二" -> {
                    itemRight = itemWidth * 2f
                    itemLeft = itemWidth * 1f
                }

                "星期三" -> {
                    itemRight = itemWidth * 3f
                    itemLeft = itemWidth * 2f
                }

                "星期四" -> {
                    itemRight = itemWidth * 4f
                    itemLeft = itemWidth * 3f
                }

                "星期五" -> {
                    itemRight = itemWidth * 5f
                    itemLeft = itemWidth * 4f
                }

                "星期六" -> {
                    itemRight = itemWidth * 6f
                    itemLeft = itemWidth * 5f
                }

                "星期日" -> {
                    itemRight = itemWidth * 7f
                    itemLeft = itemWidth * 6f
                }
            }


            itemLeft += 1.dp.value
            itemRight -= 1.dp.value
            itemTop += 1.dp.value
            itemBottom -= 1.dp.value
            val color = Utils.getCourseColor(kb, map)
            val childWidth = itemRight - itemLeft
            val childHeight = itemBottom - itemTop

            val newChildWidth = with(LocalDensity.current) { childWidth.toDp() }
            val newChildLeft = with(LocalDensity.current) { itemLeft.toDp() }

            Card(
                modifier = Modifier
                    .offset(newChildLeft, itemTop.dp)
                    .width(newChildWidth)
                    .padding(1.dp)
                    .height(childHeight.dp),
                colors = CardDefaults.cardColors(containerColor = Color(color)),
                shape = baseRadius,
                onClick = { onClick.invoke(kb) }
            ) {
                Column(
                    modifier = Modifier.fillMaxSize(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        "${kb.cdmc}\n${kb.kcmc}",
                        color = Color.White,
                        textAlign = TextAlign.Center,
                        style = MaterialTheme.typography.labelSmall,
                        overflow = TextOverflow.Ellipsis
                    )
                }
            }

        }
    }
}

