package com.example.coursetable_compose.ui.commen

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Warning
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.mypotota.datamodel.base.BaseStatus
import com.tapsdk.lc.LCObject
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.system.exitProcess

@Composable
fun MainUI(
    spIsAvailable: Boolean,
    appControlsStatus: BaseStatus<LCObject>,
    newNotice: BaseStatus<LCObject>,
    checkCurrentVersionStatus: BaseStatus<LCObject>?,
    checkUpdateStatus: BaseStatus<List<LCObject>>?,
    innerPadding:PaddingValues,
    readNoticeList: ArrayList<String>,
    updateAppControls: (usable: Boolean, privacyPolicy: String, feedbackNoticeForUse: String,openSourceAddress:String,autoRefresh:Boolean,qqGroup:String) -> Unit,
    readNotice: (objectId: String) -> Unit,
    content: @Composable () -> Unit
) {
    var isAvailable by remember {
        mutableStateOf(spIsAvailable)
    }
    val scope = rememberCoroutineScope()
    var availableMessage by remember { mutableStateOf("") }

    var noticeLCObject by remember { mutableStateOf(LCObject()) }
    var isShowNotice by remember { mutableStateOf(false) }
    //新版本提示
    var newVersionShow by remember {
        mutableStateOf(false)
    }
    var newVersionMessage by remember {
        mutableStateOf("")
    }
    //是否有Bug
    var hasBugShow by remember {
        mutableStateOf(false)
    }

    //查询当前版本是否存在问题
    LaunchedEffect(key1 = checkCurrentVersionStatus) {
        when (checkCurrentVersionStatus) {
            is BaseStatus.ERROR -> {}
            is BaseStatus.LOADING -> {}
            is BaseStatus.SUCCESS -> {
                val lcObject = checkCurrentVersionStatus.data
                val hasBug = lcObject.getBoolean("hasBug")
                hasBugShow = hasBug
                scope.launch {
                    delay(5000)
                    hasBugShow = false
                }
            }

            null -> {}
        }
    }
    //查询是否有更新
    LaunchedEffect(checkUpdateStatus) {
        when (val status=checkUpdateStatus) {
            is BaseStatus.ERROR -> {}
            is BaseStatus.LOADING -> {}
            is BaseStatus.SUCCESS -> {
                val data = status.data
                if (data.isEmpty()) {
                    return@LaunchedEffect
                }
                val lcObject = data[0]
                val newVersion = lcObject.getString("name")
                newVersionMessage = "发现新版本：$newVersion，请前往设置中更新"
                newVersionShow = true
                scope.launch {
                    delay(10000)
                    newVersionShow = false
                }
            }

            null -> {}
        }
    }


    //newNotice-通知
    LaunchedEffect(newNotice) {
        when (newNotice) {
            is BaseStatus.ERROR -> {}
            is BaseStatus.LOADING -> {}
            is BaseStatus.SUCCESS -> {
                val lcObject = newNotice.data
                noticeLCObject = lcObject
                isShowNotice = !readNoticeList.contains(lcObject.objectId)
            }
        }
    }

    //app配置-控制
    LaunchedEffect(appControlsStatus) {
        when (appControlsStatus) {
            is BaseStatus.ERROR -> {
                availableMessage = appControlsStatus.e
            }

            is BaseStatus.LOADING -> {

            }

            is BaseStatus.SUCCESS -> {
                val lcObject = appControlsStatus.data
                val usable = lcObject.getBoolean("usable")
                val privacyPolicy = lcObject.getString("privacyPolicy")
                val feedbackNoticeForUse = lcObject.getString("feedbackNoticeForUse")
                val openSourceAddress = lcObject.getString("openSourceAddress")
                val qqGroup = lcObject.getString("qqGroup")
                val autoRefresh = lcObject.getBoolean("autoRefresh")
                isAvailable = usable
                updateAppControls.invoke(usable, privacyPolicy, feedbackNoticeForUse,openSourceAddress,autoRefresh,qqGroup)
            }
        }
    }





    Column(
        modifier = Modifier.padding()
    ) {
        AnimatedVisibility(visible = newVersionShow) {
            Card(modifier = Modifier.padding(12.dp)) {
                Row(modifier = Modifier.padding(12.dp)) {
                    Icon(Icons.Outlined.Info, contentDescription = "")
                    Spacer(modifier = Modifier.width(12.dp))
                    Text(
                        text = newVersionMessage,
                        modifier = Modifier.fillMaxWidth(),
                        color = MaterialTheme.colorScheme.primary,
                        style = MaterialTheme.typography.labelLarge
                    )
                }
            }
        }
        AnimatedVisibility(visible = hasBugShow) {
            Card(modifier = Modifier.padding(12.dp)) {
                Row(modifier = Modifier.padding(12.dp)) {
                    Icon(Icons.Filled.Warning, contentDescription = "")
                    Spacer(modifier = Modifier.width(12.dp))
                    Text(
                        text = "当前版本存在重大Bug，请尽快升级至最新版本",
                        modifier = Modifier.fillMaxWidth(),
                        color = MaterialTheme.colorScheme.error,
                        style = MaterialTheme.typography.labelLarge
                    )
                }
            }
        }
        AnimatedVisibility(!isAvailable) {
            Column(
                Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Card(
                    Modifier
                        .padding(24.dp)
                ) {
                    Column(
                        Modifier.padding(12.dp),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Icon(
                            Icons.Filled.Info,
                            "",
                            tint = if (isSystemInDarkTheme()) Color.White else Color.Unspecified
                        )
                        Spacer(Modifier.height(12.dp))
                        Text(
                            "此应用已暂停运行，对你带来的不便为此感到非常抱歉",
                            textAlign = TextAlign.Center,
                            color = if (isSystemInDarkTheme()) Color.White else Color.Unspecified
                        )
                        Spacer(Modifier.height(12.dp))
                        Button({
                            exitProcess(0)
                        }) {
                            Text("我知道了")
                        }
                    }
                }
            }
        }
        AnimatedVisibility(isAvailable) {
            Box(modifier = Modifier) {
                content()
            }
        }
    }

    //显示通知
    if (isShowNotice) {
        NoticeDialogUI(noticeLCObject, {
            isShowNotice = false
        }, {
            isShowNotice = false
            readNotice.invoke(noticeLCObject.objectId)
        })
    }


}