package com.example.coursetable_compose.ui.screen

import android.annotation.SuppressLint
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.outlined.Add
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.desktop.screenbean.MainPageScreen
import com.example.coursetable_compose.ui.activity.baseRadius
import com.mypotota.datamodel.base.BaseStatus
import com.example.coursetable_compose.ui.commen.BaseToolbar
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.viewmodel.FeedBackViewModel
import com.tapsdk.lc.LCObject

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun FeedBackScreen(navController: NavController, innerPadding: PaddingValues) {
    val viewModel =viewModel<FeedBackViewModel>()
    val feedbackNoticeForUse by remember {
        mutableStateOf(
            SPUtils.getIns().getFeedbackNoticeForUse()
        )
    }
    var isShowAddFeedBackUI by remember { mutableStateOf(false) }

    Scaffold(
        modifier = Modifier.padding(top = innerPadding.calculateTopPadding()),
        floatingActionButton = {
            FloatingActionButton(onClick = {
                navController.navigate(MainPageScreen.AddFeedBackScreen.name)
            }, shape = baseRadius) {
                Icon(Icons.Outlined.Add, contentDescription = "")
            }
        }
    ) {
        Column(
            Modifier
                .fillMaxSize()) {
            BaseToolbar(navController,MainPageScreen.FeedBackScreen.label)
            FeedBackList()
        }
    }




//    //使用须知弹窗
//    if (!readFeedBackNotice) {
//        AlertDialog({}, icon = {
//            Icon(Icons.Default.Notifications, "")
//        }, confirmButton = {
//            Button({
//                SPUtils.getIns().setReadFeedBackNotice(true)
//                readFeedBackNotice = true
//            }) {
//                Text("我同意")
//            }
//        }, dismissButton = {
//            OutlinedButton({
//                readFeedBackNotice = true
//                navController.popBackStack()
//            }) {
//                Text("我不同意")
//            }
//        }, title = {
//            Text("意见反馈使用须知", style = MaterialTheme.typography.titleLarge)
//        }, text = {
//            SelectionContainer {
//                Text(
//                    feedbackNoticeForUse ?: "意见反馈使用须知未能成功加载，请稍后重试吧",
//                    modifier = Modifier.verticalScroll(rememberScrollState())
//                )
//            }
//        })
//    }



}



@Composable
fun FeedBackList() {
    val viewModel = viewModel<FeedBackViewModel>()
    val feedBackByDevicesStatus = viewModel.feedBackByDevicesStatus.collectAsState().value
    var isLoading by remember { mutableStateOf(true) }
    var isError by remember { mutableStateOf(false) }
    var errorMessage by remember { mutableStateOf("") }
    var feedback by remember { mutableStateOf<List<LCObject>>(ArrayList()) }

    LaunchedEffect(feedBackByDevicesStatus) {
        when (feedBackByDevicesStatus) {
            is BaseStatus.ERROR -> {
                isLoading = false
                isError = true
                errorMessage = feedBackByDevicesStatus.e
            }

            is BaseStatus.LOADING -> {
                isLoading = true
                isError = false
            }

            is BaseStatus.SUCCESS -> {
                isLoading = false
                isError = false
                feedback = feedBackByDevicesStatus.data
            }
        }
    }
    LazyColumn {
        item {
            AnimatedVisibility(isLoading) {
                Card(
                    Modifier
                        .fillMaxWidth()
                        .padding(12.dp), shape = baseRadius) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(12.dp)
                    ) {
                        CircularProgressIndicator()
                    }
                }
            }
        }
        item {
            //没有反馈时
            AnimatedVisibility(feedback.isEmpty() && !isLoading && !isError) {
                Card(
                    Modifier
                        .fillMaxWidth()
                        .padding(12.dp), shape = baseRadius) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(12.dp)
                    ) {
                        Icon(Icons.Default.Info, "")
                        Text("暂无提交任何反馈")
                    }
                }
            }
        }
        items(items=feedback){lcObject->
            key(lcObject.objectId) {
                FeedBackCard(lcObject)
            }
        }
    }
}

@Composable
fun FeedBackCard(lcObject: LCObject) {
    val status = lcObject.getInt("status")
    Card(
        Modifier
            .padding(horizontal = 12.dp, vertical = 4.dp)
            .fillMaxWidth(), shape = baseRadius) {
        Row(Modifier.padding(12.dp)){
            Column(modifier = Modifier.weight(1f)) {
                Text(lcObject.getString("title"), style = MaterialTheme.typography.bodyLarge)
                Spacer(Modifier.height(12.dp))
                Text(lcObject.getString("content"), style = MaterialTheme.typography.bodySmall)
            }
            Column {
                when(status){
                    0->{
                        Text("待处理..", style = MaterialTheme.typography.labelSmall)
                    }
                    1->{
                        Text("正在处理中..", color = Color(0xffF57C00), style = MaterialTheme.typography.labelSmall)
                    }
                    2->{
                        Text("已处理", color = Color(0xff388E3C), style = MaterialTheme.typography.labelSmall)
                    }
                    3->{
                        Text("已采纳", color = Color(0xff1976D2), style = MaterialTheme.typography.labelSmall)
                    }
                }
            }
        }
    }
}
