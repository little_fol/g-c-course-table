package com.example.coursetable_compose.ui.screen

import android.content.Context
import android.widget.Toast
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Face
import androidx.compose.material.icons.filled.Info
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.OutlinedCard
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.baidu.mobstat.StatService
import com.desktop.screenbean.MainPageScreen
import com.example.coursetable_compose.R
import com.example.coursetable_compose.application.CourseTableApplication
import com.example.coursetable_compose.ui.activity.baseRadius
import com.mypotota.datamodel.base.BaseStatus
import com.example.coursetable_compose.ui.commen.BaseToolbar
import com.example.coursetable_compose.utils.APKVersionCodeUtils
import com.example.coursetable_compose.utils.Utils
import com.example.coursetable_compose.viewmodel.DownLoadViewModel
import com.example.coursetable_compose.viewmodel.VersionViewModel
import com.hjq.permissions.OnPermissionCallback
import com.hjq.permissions.Permission
import com.hjq.permissions.Permission.REQUEST_INSTALL_PACKAGES
import com.hjq.permissions.XXPermissions
import com.tapsdk.lc.LCObject
import com.xuexiang.xupdate._XUpdate
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.File
import java.text.SimpleDateFormat

@Composable
fun VersionManageScreen(parentNavController: NavController, innerPadding: PaddingValues) {
    val viewModel = viewModel<VersionViewModel>()
    var isShowDownLoadDialog by remember {
        mutableStateOf(false)
    }
    var isShowDownLoadDialogSure by remember {
        mutableStateOf(false)
    }
    var downloadLCObject by remember {
        mutableStateOf(LCObject())
    }
    val downLoadViewModel = viewModel<DownLoadViewModel>()
    val context = LocalContext.current
    val xxPermissions = XXPermissions.with(context)
        .permission(Permission.REQUEST_INSTALL_PACKAGES)

    viewModel.getAllVersion()
    Column(Modifier.fillMaxSize().padding(top = innerPadding.calculateTopPadding())) {
        BaseToolbar(parentNavController,MainPageScreen.VersionManage.label)
        VersionManageUI() {
            isShowDownLoadDialogSure = true
            downloadLCObject = it
        }
    }


    if (isShowDownLoadDialogSure) {
        AlertDialog(onDismissRequest = {
            isShowDownLoadDialogSure = false
        }, confirmButton = {
            OutlinedButton(onClick = {
                //将网址复制到剪切板
                Utils.copy(context,downloadLCObject.getString("url"))
                //复制后关闭弹窗
                isShowDownLoadDialogSure = false
                //直接跳转网页执行下载
                openBrowserByUrl(context, downloadLCObject.getString("url"))
            }) {
                Text(text = "浏览器下载")
            }
        }, dismissButton = {
            Button(shape = baseRadius,onClick = {
                if (XXPermissions.isGranted(
                        context,
                        REQUEST_INSTALL_PACKAGES
                    )
                ) {
                    isShowDownLoadDialog = true
                    downLoadViewModel.startDownLoad(downloadLCObject)
                } else {
                    Toast.makeText(context, "软件升级需要安装应用权限", Toast.LENGTH_SHORT).show()
                    xxPermissions.request(object : OnPermissionCallback {
                        override fun onGranted(
                            permissions: MutableList<String>,
                            allGranted: Boolean
                        ) {
                            if (!allGranted) {
                                Toast.makeText(context, "部分权限未授予", Toast.LENGTH_SHORT).show()
                                return
                            }
                            Toast.makeText(
                                context,
                                "安装权限已授予，请在次点击下载",
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }

                        override fun onDenied(
                            permissions: MutableList<String>,
                            doNotAskAgain: Boolean
                        ) {
                            if (doNotAskAgain) {
                                Toast.makeText(
                                    context,
                                    "已被永久拒绝安装权限，请手动授予",
                                    Toast.LENGTH_SHORT
                                ).show()
                                // 如果是被永久拒绝就跳转到应用权限系统设置页面
                                XXPermissions.startPermissionActivity(context, permissions)
                            } else {
                                Toast.makeText(context, "安装权限授予失败", Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }
                    })
                }
            }) {
                Text(text = "本地下载")
            }
        },
            title = {
                Text(text = "温馨提醒")
            }, text = {
                Text(text = "如果本地下载无法安装，请复制链接到浏览器下载安装")
            })
    }



    if (isShowDownLoadDialog) {
        DownLoadDialog(downloadLCObject) {
            isShowDownLoadDialog = false
        }
    }

}

@Composable
fun VersionManageUI(onClickDownCallback: (downloadLCObject: LCObject) -> Unit) {
    val viewModel = viewModel<VersionViewModel>()
    val allVersionStatus = viewModel.allVersion.collectAsState().value

    var isLoading by remember { mutableStateOf(true) }
    var isError by remember { mutableStateOf(false) }
    var errorMessage by remember { mutableStateOf("") }
    var allVersion by remember { mutableStateOf<List<LCObject>>(ArrayList()) }

    LaunchedEffect(allVersionStatus) {
        when (allVersionStatus) {
            is BaseStatus.ERROR -> {
                isLoading = false
                isError = true
                errorMessage = allVersionStatus.e
            }

            is BaseStatus.LOADING -> {
                isLoading = true
                isError = false
            }

            is BaseStatus.SUCCESS -> {
                isLoading = false
                isError = false
                allVersion = allVersionStatus.data
            }
        }
    }

    LazyColumn(contentPadding = PaddingValues(12.dp)) {
        item {
            CurrentVersion(onClickDownCallback)
        }
        item {
            Text(
                "其他版本",
                style = MaterialTheme.typography.bodyLarge,
                modifier = Modifier.padding(vertical = 12.dp)
            )
        }
        item {
            //正在加载中
            AnimatedVisibility(isLoading) {
                Card(Modifier.fillMaxWidth(), shape = baseRadius) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(12.dp)
                    ) {
                        CircularProgressIndicator()
                    }
                }
            }
        }
        item {
            //发生错误时
            AnimatedVisibility(isError) {
                Card(
                    Modifier
                        .fillMaxWidth()
                        .padding(vertical = 12.dp), shape = baseRadius
                ) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(12.dp)
                    ) {
                        Icon(Icons.Default.Info, "")
                        Text(
                            "获取全部版本，错误原因：$errorMessage，请稍后重试吧",
                            textAlign = TextAlign.Center
                        )
                        Button({},shape = baseRadius) {
                            Text("重试")
                        }
                    }
                }
            }
        }
        item {
            //更新
            AnimatedVisibility(allVersion.isEmpty() && !isLoading && !isError) {
                Card(Modifier.fillMaxWidth(), shape = baseRadius) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(12.dp)
                    ) {
                        Icon(Icons.Default.Face, "")
                        Text("暂无其他版本")
                    }
                }
            }
        }
        itemsIndexed(items = allVersion) { index, item ->
            VersionCard(item, onClickDownCallback)
            Spacer(Modifier.height(12.dp))
        }
    }

}

@Composable
fun DownLoadDialog(downloadLCObject: LCObject, onDismiss: () -> Unit) {
    val downLoadViewModel = viewModel<DownLoadViewModel>()
    val downloadStatus = downLoadViewModel.downloadStatus.collectAsState().value
    val context = LocalContext.current
    val scope = rememberCoroutineScope()
    var isShowProgress by remember {
        mutableStateOf(true)
    }
    var loadingMessage by remember {
        mutableStateOf("")
    }
    var isError by remember {
        mutableStateOf(false)
    }
    var downloadSuccess by remember {
        mutableStateOf(false)
    }
    var errorMessage by remember {
        mutableStateOf("")
    }
    var progress by remember {
        mutableStateOf(0f)
    }
    LaunchedEffect(key1 = downloadStatus) {
        when (downloadStatus) {
            is BaseStatus.ERROR -> {
                isShowProgress = false
                downloadSuccess = false
                errorMessage = downloadStatus.e
                isError = true
                scope.launch {
                    delay(3000)
                    isError = false
                    onDismiss()
                }
            }

            is BaseStatus.LOADING -> {
                isShowProgress = true
                isError = false
                downloadSuccess = false
                loadingMessage = downloadStatus.msg
                progress = downloadStatus.progress
            }

            is BaseStatus.SUCCESS -> {
                isShowProgress = false
                isError = false
                downloadSuccess=true
                installApk(context,downloadStatus.data)
            }
        }
    }

    AlertDialog(onDismissRequest = { }, confirmButton = {
        AnimatedVisibility(visible = downloadSuccess && !isError) {
            Button(onClick = {
                installApk(context,(downloadStatus as BaseStatus.SUCCESS).data)
            }) {
                Text(text = "安装")
            }
        }
    }, dismissButton = {
        AnimatedVisibility(visible = isError||downloadSuccess) {
            OutlinedButton(onClick = onDismiss) {
                Text(text = "取消")
            }
        }
    }, title = {
        Text(
            text = downloadLCObject.getString("name"),
            style = MaterialTheme.typography.titleLarge
        )
    }, text = {
        Column {
            Text(
                text = downloadLCObject.getString("desc"),
                style = MaterialTheme.typography.bodyMedium
            )
            Spacer(modifier = Modifier.height(12.dp))
            AnimatedVisibility(visible = isShowProgress) {
                Column {
                    Text(text = loadingMessage)
                    Spacer(modifier = Modifier.height(12.dp))
                    LinearProgressIndicator(progress = {
                        progress
                    })
                }
            }
            //发生错误时
            AnimatedVisibility(isError) {
                Card(Modifier.fillMaxWidth(), shape = baseRadius) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(12.dp)
                    ) {
                        Icon(Icons.Default.Info, "")
                        Text(
                            "获取全部版本，错误原因：$errorMessage，请稍后重试吧",
                            textAlign = TextAlign.Center
                        )
                    }
                }
            }
        }
    })
}

//第四部: 安装apk
private fun installApk(context: Context, file: File) {
    _XUpdate.startInstallApk(context, file) //填写文件所在的路径
//    val authority =context.applicationContext.packageName + ".fileProvider";
//    //确保authority 与AndroidManifest.xml中android:authorities="包名.fileProvider"所有字符一致
//    val apkUri = FileProvider.getUriForFile(context, authority, file);
//    val intent = Intent(Intent.ACTION_VIEW);
//    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//    //判读版本是否在7.0以上
//    if (Build.VERSION.SDK_INT >= 24) {
//        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
//    } else {
//        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
//    }
//    Log.i("DOWNLOAD", "installApk() startActivity(intent)");
//    context.startActivity(intent);
//    (context as ComponentActivity).finish()
}


@Composable
fun VersionCard(item: LCObject, download: (downloadLCObject: LCObject) -> Unit) {
    Card(modifier = Modifier.fillMaxWidth(), shape = baseRadius) {
        Row(Modifier.padding(12.dp)) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f)
            ) {
                Text(
                    "${item.getString("name")}(${item.getNumber("versionNumber")})",
                    style = MaterialTheme.typography.bodyLarge
                )
                Spacer(Modifier.height(12.dp))
                Text("更新内容：", style = MaterialTheme.typography.bodySmall)
                Text(item.getString("desc"), style = MaterialTheme.typography.bodySmall)
                Spacer(Modifier.height(12.dp))
                Text(
                    "更新时间：${SimpleDateFormat("yyyy-MM-dd").format(item.getDate("createdAt"))}",
                    style = MaterialTheme.typography.bodySmall
                )
                if (APKVersionCodeUtils.getVersionNumber(rememberNavController().context) == item.getNumber(
                        "versionNumber"
                    ).toInt()
                ) {
                    Text(
                        "当前版本",
                        modifier = Modifier.padding(top = 12.dp),
                        color = MaterialTheme.colorScheme.primary,
                        style = MaterialTheme.typography.bodyMedium
                    )
                }
            }
            IconButton({
                //下载
                download.invoke(item)
            }) {
                Icon(painterResource(id = R.drawable.outline_file_download_24), "")
            }
        }
    }
}

//显示当前版本并检测更新
@Composable
fun CurrentVersion(onClickDownCallback: (downloadLCObject: LCObject) -> Unit) {
    val testDeviceId = StatService.getTestDeviceId(CourseTableApplication.context)
    val viewModel = viewModel<VersionViewModel>()
    val currentVersionCode = APKVersionCodeUtils.getVersionNumber(
        rememberNavController().context
    )
    val currentVersionName = APKVersionCodeUtils.getVersionName(
        rememberNavController().context
    )
    val scope = rememberCoroutineScope()

    val checkUpdateStatus = viewModel.checkUpdateStatus.collectAsState().value
    var updateList by remember {
        mutableStateOf<List<LCObject>>(ArrayList())
    }

    var isLoading by remember {
        mutableStateOf(false)
    }
    var isError by remember {
        mutableStateOf(false)
    }
    var errorMessage by remember {
        mutableStateOf("")
    }
    var isInit by remember {
        mutableStateOf(true)
    }
    LaunchedEffect(key1 = checkUpdateStatus) {
        when (checkUpdateStatus) {
            is BaseStatus.ERROR -> {
                isInit = false
                isLoading = false
                isError = true
                errorMessage = checkUpdateStatus.e
                scope.launch {
                    delay(3000)
                    isError = false
                }
            }

            is BaseStatus.LOADING -> {
                isInit = false
                isLoading = true
                isError = false
            }

            is BaseStatus.SUCCESS -> {
                isInit = false
                isLoading = false
                isError = false
                updateList = checkUpdateStatus.data
            }

            null -> {
                isInit = true
                isLoading = false
                isError = false
            }
        }
    }


    Card(modifier = Modifier.fillMaxWidth(), shape = baseRadius) {
        Column(Modifier.padding(12.dp)) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                Column(Modifier.weight(1f)) {
                    Text(
                        APKVersionCodeUtils.getSystemName(),
                        style = MaterialTheme.typography.headlineLarge
                    )
                    Spacer(Modifier.height(12.dp))
                    Text(
                        "版本：${currentVersionName}(${currentVersionCode})"
                    )
                    
                }
                Button({
                    viewModel.checkUpdate(currentVersionCode)
                }, shape = baseRadius) {
                    Text("检查更新")
                }
            }
            Spacer(modifier = Modifier.height(12.dp))
            AnimatedVisibility(visible = isLoading, modifier = Modifier.fillMaxWidth()) {
                LinearProgressIndicator(modifier = Modifier.fillMaxWidth())
            }
            AnimatedVisibility(visible = isError, modifier = Modifier.fillMaxWidth()) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(12.dp)
                ) {
                    Icon(Icons.Default.Info, "")
                    Text(
                        "获取全部版本，错误原因：$errorMessage，请稍后重试吧",
                        textAlign = TextAlign.Center
                    )
                    Button({}, shape = baseRadius) {
                        Text("重试")
                    }
                }
            }
            //暂无更新
            AnimatedVisibility(
                visible = (!isInit && !isLoading && !isError) && updateList.isEmpty(),
                modifier = Modifier.fillMaxWidth()
            ) {
                OutlinedCard(shape = baseRadius) {
                    Text(
                        "当前无可用更新",
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(12.dp),
                        textAlign = TextAlign.Center,
                        style = MaterialTheme.typography.labelSmall
                    )
                }
            }
            //有更新
            AnimatedVisibility(
                visible = (!isInit && !isLoading && !isError) && updateList.isNotEmpty(),
                modifier = Modifier.fillMaxWidth()
            ) {
                Column {
                    OutlinedCard(shape = baseRadius){
                        Column {
                            updateList.forEach {
                                VersionCard(item = it) { lc ->
                                    onClickDownCallback.invoke(lc)
                                }
                            }
                        }
                    }
                }
            }
            Spacer(Modifier.height(12.dp))
            TextButton(shape = baseRadius,onClick = {
                Utils.copy(CourseTableApplication.context, testDeviceId)
            }) {
                Text("设备号：${testDeviceId}", style = MaterialTheme.typography.labelSmall)
            }
        }
    }
}
