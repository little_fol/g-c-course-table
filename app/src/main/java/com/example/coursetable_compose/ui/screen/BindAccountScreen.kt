package com.example.coursetable_compose.ui.screen

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CheckCircle
import androidx.compose.material.icons.filled.Info
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.desktop.screenbean.MainPageScreen
import com.example.coursetable_compose.bean.UserBean
import com.example.coursetable_compose.ui.activity.baseRadius
import com.mypotota.datamodel.base.BaseStatus
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.viewmodel.BindAccountViewModel
import com.mypotota.datamodel.bean.CourseTable
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.system.exitProcess

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BindAccountScreen(navController: NavHostController, innerPadding: PaddingValues) {

    LoginUI(
        padding=innerPadding,
        onBindSuccess = {
            SPUtils.getIns().setCourseTable(it)
            navController.navigate(MainPageScreen.HomeScreen.name) {
                popUpTo(MainPageScreen.BindAccountScreen.name) {
                    inclusive = true
                }
                launchSingleTop = true // 可选，如果详情页已经是栈顶则不会重新创建
            }
        })


}


@Composable
fun LoginUI(
    isDark: Boolean = isSystemInDarkTheme(),
    padding:PaddingValues,
    onBindSuccess: (data: CourseTable) -> Unit,
) {
    val viewModel = viewModel<BindAccountViewModel>()
    //账号密码绑定状态
    val loginState by viewModel.courseTableStatus.collectAsState()
    //是否显示隐私策略
    var privacyPolicyShowed by remember { mutableStateOf(false) }
    var account by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }

    var tips by remember { mutableStateOf("") }
    var tipsShow by remember { mutableStateOf(false) }

    var onError by remember { mutableStateOf(false) }
    var errorMessage by remember { mutableStateOf("") }

    var onLoading by remember { mutableStateOf(false) }
    var loadingMessage by remember { mutableStateOf("") }

    var onSuccess by remember { mutableStateOf(true) }

    val scope = rememberCoroutineScope()
    //绑定点击事件
    val click: () -> Unit = {
        if (account.isEmpty()) {
            tips = "请输入账号"
            tipsShow = true
            scope.launch {
                delay(1000)
                tipsShow = false
            }
        } else if (password.isEmpty()) {
            tips = "请输入密码"
            tipsShow = true
            scope.launch {
                delay(1000)
                tipsShow = false
            }
        } else {
            SPUtils.getIns().setAccount(account)
            SPUtils.getIns().setPassword(password)
            viewModel.getLoginTicket(account, password)
        }
    }
    LaunchedEffect(loginState) {
        //管理绑定状态
        when (val status=loginState) {
            is BaseStatus.ERROR -> {
                onError = true
                onLoading = false
                onSuccess = false
                errorMessage = status.e
            }

            is BaseStatus.LOADING -> {
                onError = false
                onSuccess = false
                onLoading = true
                loadingMessage = status.msg
            }

            is BaseStatus.SUCCESS -> {
                onError = false
                onLoading = false
                onSuccess = true
                //添加用户
                val userList = SPUtils.getIns().userList
                userList.add(UserBean(account,password))
                SPUtils.getIns().userList=userList
                //绑定成功
                scope.launch {
                    delay(1000)
                    onBindSuccess(status.data)
                }
            }

            null -> {
                onError = false
                onLoading = false
                onSuccess = false
            }
        }
    }
    //取消
    val cancel = {
        onError = false
        onLoading = false
        onSuccess = false
    }


    Column(Modifier.animateContentSize().padding(top = padding.calculateTopPadding())) {
        AnimatedVisibility(!privacyPolicyShowed) {
            Column(Modifier.padding(12.dp)) {
                Text(
                    "绑定账号",
                    style = MaterialTheme.typography.titleLarge,
                    fontWeight = FontWeight(1000),
                    color = if (isDark) Color(0xffeaeaea) else Color.Unspecified
                )
                Spacer(Modifier.height(12.dp))
                Text(
                    "绑定智慧工程账号密码。", style = MaterialTheme.typography.bodyLarge,
                    color = if (isDark) Color(0xffeaeaea) else Color.Unspecified
                )
                Spacer(Modifier.height(12.dp))
                //判断是扫码UI 还是账号密码绑定UI
                Column {
                    OutlinedTextField(
                        account, { account = it }, modifier = Modifier.fillMaxWidth(),
                        label = {
                            Text("输入智慧工程账号")
                        }, shape = baseRadius
                    )
                    Spacer(Modifier.height(4.dp))
                    OutlinedTextField(
                        password, { password = it }, modifier = Modifier.fillMaxWidth(),
                        label = {
                            Text("输入智慧工程密码")
                        }, shape = baseRadius
                    )
                    Spacer(Modifier.height(8.dp))
                    //初始状态
                    AnimatedVisibility((!onError && !onLoading && !onSuccess)) {
                        Button(
                            click,
                            modifier = Modifier.fillMaxWidth(),
                            shape = baseRadius
                        ) {
                            Text("绑定")
                        }
                    }

                }
                //显示错误
                AnimatedVisibility(onError) {
                    Column(
                        Modifier
                            .fillMaxWidth()
                            .padding(12.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        SelectionContainer {
                            Text(
                                text = errorMessage,
                                color = Color.Red,
                            )
                        }
                        Button(onClick = click) {
                            Text(text = "重试")
                        }
                    }
                }

                //绑定成功
                AnimatedVisibility(onSuccess) {
                    Column(
                        Modifier
                            .fillMaxWidth()
                            .padding(12.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Icon(
                            imageVector = Icons.Filled.CheckCircle,
                            "",
                            tint = MaterialTheme.colorScheme.primary
                        )
                        Text("绑定成功")
                    }
                }

                //显示正在加载中
                AnimatedVisibility(onLoading) {
                    Column(
                        Modifier
                            .fillMaxWidth()
                            .padding(12.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        CircularProgressIndicator()
                        Text(text = loadingMessage)
                        Button(onClick = {
                            viewModel.cancelBind()
                        }) {
                            Text(text = "取消")
                        }
                    }
                }

                //显示错误信息提示
                AnimatedVisibility(tipsShow) {
                    Spacer(Modifier.height(8.dp))
                    Text(
                        tips,
                        color = MaterialTheme.colorScheme.error,
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Center
                    )
                }


                TextButton({
                    privacyPolicyShowed = true
                }, modifier = Modifier.align(Alignment.CenterHorizontally)) {
                    Text("隐私策略")
                }
            }
        }





        AnimatedVisibility(privacyPolicyShowed) {
            Column(
                Modifier
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState())
                    .padding(12.dp),
            ) {
                Icon(
                    Icons.Filled.Info, "", modifier = Modifier.align(Alignment.CenterHorizontally),
                    tint = if (isDark) Color.White else Color.Unspecified
                )
                Spacer(Modifier.height(8.dp))
                Text(
                    "隐私策略",
                    style = MaterialTheme.typography.titleLarge,
                    modifier = Modifier.align(
                        Alignment.CenterHorizontally
                    ),
                    color = if (isDark) Color(0xffeaeaea) else Color.Unspecified
                )
                Spacer(Modifier.height(8.dp))
                Text(
                    SPUtils.getIns().getPrivacyPolicy()
                        ?: "隐私策略未能成功加载，可能遇到网络故障了，请稍后重试吧",
                    color = if (isDark) Color(0xffeaeaea) else Color.Unspecified
                )
                Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End) {
                    TextButton({
                        exitProcess(0)
                    }) {
                        Text("我不同意（退出）")
                    }
                    TextButton({
                        privacyPolicyShowed = false
                    }) {
                        Text("关闭（我已知晓）")
                    }
                }
            }
        }
    }
}