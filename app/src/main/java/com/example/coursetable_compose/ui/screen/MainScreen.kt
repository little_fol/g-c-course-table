package com.example.coursetable_compose.ui.screen

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Icon
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDrawerState
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.coursetable_compose.screenbean.HomePageScreen
import kotlinx.coroutines.launch

fun NavHostController.navigateSingleTopTo(route: String) =
    this.navigate(route) {
        popUpTo(
            this@navigateSingleTopTo.graph.findStartDestination().id
        ) {
            saveState = true
        }
        launchSingleTop = true
        restoreState = true
    }

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun MainScreen(parentNavController: NavHostController) {
    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val scope = rememberCoroutineScope()
    val homePageScreens = HomePageScreen.entries.toList()
    //根据状态分辨主页
    val childNavController = rememberNavController()
    val currentBackStack by childNavController.currentBackStackEntryAsState()
    val currentDestination = currentBackStack?.destination


    Scaffold(
        bottomBar = {
            NavigationBar(Modifier){
                homePageScreens.forEachIndexed { index, item ->
                    NavigationBarItem(
                        icon = { Icon(painterResource(id = item.icon), contentDescription = item.label) },
                        label = { Text(item.label) },
                        selected = currentDestination?.route.equals(item.name),
                        onClick = {
                            childNavController.navigateSingleTopTo(item.name)
                            scope.launch {
                                drawerState.close()
                            }
                        }
                    )
                }
            }
        }
    ) {padding->
        NavHost(navController = childNavController,
            startDestination = HomePageScreen.HomePage.name,
            modifier = Modifier.padding(padding)
        ) {
            composable(HomePageScreen.HomePage.name) { HomePageScreen(parentNavController) }
            composable(HomePageScreen.CourTablePage.name) { CourTableScreen(parentNavController) }
            composable(HomePageScreen.SettingPage.name) { SettingPageScreen(parentNavController) }
        }
    }

}