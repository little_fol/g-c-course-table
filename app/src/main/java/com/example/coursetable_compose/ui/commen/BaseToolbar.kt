package com.example.coursetable_compose.ui.commen

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import com.desktop.screenbean.MainPageScreen

@Composable
fun BaseToolbar(navController: NavController,title:String) {
    Row(Modifier.padding(vertical = 12.dp), verticalAlignment = Alignment.CenterVertically) {
        IconButton({
            navController.popBackStack()
        }) {
            Icon(imageVector = Icons.AutoMirrored.Filled.ArrowBack, "")
        }
        Text(title, style = MaterialTheme.typography.titleLarge)
    }
}