package com.example.coursetable_compose.ui.screen

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.desktop.screenbean.MainPageScreen
import com.example.coursetable_compose.ui.activity.baseRadius
import com.example.coursetable_compose.ui.commen.BaseToolbar
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.viewmodel.CourseTableViewModel


@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun PaletteScreen(navController: NavController, innerPadding: PaddingValues) {
    val viewModel = viewModel<CourseTableViewModel>()

    viewModel.getCourseTableColor()
    viewModel.getColorList()

    Column(
        Modifier
            .fillMaxSize()
            .padding(top = innerPadding.calculateTopPadding())) {
        BaseToolbar(navController,MainPageScreen.PaletteScreen.label)

        PaletterList()

    }
}

@RequiresApi(Build.VERSION_CODES.O)
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PaletterList() {
    val viewModel = viewModel<CourseTableViewModel>()
    val colorMapByCourse = viewModel.colorMapByCourse.collectAsState()

    val colorList = viewModel.colorList.collectAsState()
    println(colorMapByCourse)
    println(colorList)

    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
    ) {

        AnimatedVisibility(colorMapByCourse.value.isEmpty()) {
            CircularProgressIndicator(modifier = Modifier.align(Alignment.CenterHorizontally))
        }
        AnimatedVisibility(colorMapByCourse.value.isNotEmpty()) {
            Column(modifier = Modifier) {
                colorMapByCourse.value.entries.forEachIndexed { index, entry ->
                    key(entry.key) {
                        var isShowSide by remember {
                            mutableStateOf(false)
                        }
                        Card(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = 12.dp, vertical = 8.dp),
                            colors = CardDefaults.cardColors(containerColor = Color(entry.value)),
                            shape = baseRadius
                        ) {
                            Column {
                                Row(
                                    verticalAlignment = Alignment.CenterVertically,
                                    modifier = Modifier.padding(12.dp)
                                ) {
                                    Text(entry.key,
                                        Modifier
                                            .fillMaxWidth()
                                            .weight(1f), color = Color.White)
                                    Button({
                                        isShowSide = !isShowSide
                                    }, shape = baseRadius) {
                                        Text("更改")
                                    }
                                }

                                //选择颜色的侧栏
                                AnimatedVisibility(isShowSide) {
                                    ColorList(colorList.value,entry.value){
                                        isShowSide=false
                                        val hashMap = SPUtils.getIns().getColorMapByCourse()
                                        hashMap[entry.key]=it
                                        SPUtils.getIns().setColorMapByCourse(hashMap)
                                        viewModel.getColorList()
                                        viewModel.getCourseTableColor()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ColorList(colorList: List<Long>, select: Long,onClick:(color:Long)->Unit) {
    LazyRow(contentPadding = PaddingValues(8.dp)) {
        items(items = colorList) {
            OutlinedCard(
                shape = RoundedCornerShape(20.dp),
                border = BorderStroke(3.dp,
                    if(select==it)
                        Color.White
                    else
                        Color(it)
                    ),
                modifier = Modifier.padding(horizontal = 8.dp),
                onClick = {onClick.invoke(it)}
            ) {
                Column(
                    Modifier
                        .clip(RoundedCornerShape(20.dp))
                        .background(Color(it)),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text("     ", color = Color.White, modifier = Modifier.padding(8.dp))
                }
            }
        }
    }
}
