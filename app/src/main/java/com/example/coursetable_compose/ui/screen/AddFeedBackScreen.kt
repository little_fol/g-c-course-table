package com.example.coursetable_compose.ui.screen

import android.annotation.SuppressLint
import android.widget.Toast
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.outlined.Send
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material3.Card
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.desktop.screenbean.MainPageScreen
import com.mypotota.datamodel.base.BaseStatus
import com.example.coursetable_compose.ui.commen.BaseToolbar
import com.example.coursetable_compose.viewmodel.FeedBackViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun AddFeedBackScreen(navController: NavHostController, innerPadding: PaddingValues) {
    val viewModel = viewModel<FeedBackViewModel>()
    var title by rememberSaveable { mutableStateOf("") }
    var content by rememberSaveable { mutableStateOf("") }
    var isShowDemo by remember { mutableStateOf(false) }
    var isShowError by remember { mutableStateOf(false) }
    var errorMessage by remember { mutableStateOf("") }
    val scope = rememberCoroutineScope()

    var isLoading by remember { mutableStateOf(false) }
    var isInitStatus by remember { mutableStateOf(false) }

    var submitFeedBackStatus = viewModel.submitFeedBackStatus.collectAsState().value

    LaunchedEffect(submitFeedBackStatus) {
        when (submitFeedBackStatus) {
            is BaseStatus.ERROR -> {
                isShowError = true
                isInitStatus = true
                isLoading = false
                errorMessage = "提交反馈时发生错误，原因：${submitFeedBackStatus.e}"
                scope.launch {
                    delay(3000)
                    isShowError = false
                }
            }

            is BaseStatus.LOADING -> {
                isLoading = true
                isInitStatus = false
                isShowError = false
            }

            is BaseStatus.SUCCESS -> {
                isLoading = false
                isInitStatus = true
                isShowError = false
                Toast.makeText(navController.context, "新增成功", Toast.LENGTH_SHORT).show()
                navController.popBackStack()
            }

            null -> {
                isInitStatus = true
            }
        }
    }

    Scaffold(
        modifier = Modifier.padding(top = innerPadding.calculateTopPadding()),
        floatingActionButton = {
            FloatingActionButton(onClick = {
                if (title.isEmpty()) {
                    isShowError = true
                    errorMessage = "请输入反馈主题"
                    scope.launch {
                        delay(1000)
                        isShowError = false
                    }
                    return@FloatingActionButton
                }
                if (content.isEmpty()) {
                    isShowError = true
                    errorMessage = "请输入反馈内容"
                    scope.launch {
                        delay(1000)
                        isShowError = false
                    }
                    return@FloatingActionButton
                }
                viewModel.submitFeedBack(title, content)
            }) {
                Icon(Icons.AutoMirrored.Outlined.Send, contentDescription = "")
            }
        }
    ) {
        Column(
            Modifier
                .fillMaxSize()
        ) {
            BaseToolbar(navController,MainPageScreen.AddFeedBackScreen.label)

            //反馈内容编辑
            Column(
                modifier = Modifier
                    .verticalScroll(rememberScrollState())
                    .padding(horizontal = 12.dp)
            ) {
                OutlinedTextField(title, { title = it }, label = {
                    Text("请输入反馈主题")
                }, modifier = Modifier.fillMaxWidth(), shape = RoundedCornerShape(10.dp))
                OutlinedTextField(
                    content, { content = it },
                    minLines = 7, label = {
                        Text("请输入反馈内容")
                    }, modifier = Modifier.fillMaxWidth(),
                    shape = RoundedCornerShape(10.dp)
                )
                Spacer(Modifier.height(12.dp))

                AnimatedVisibility(isShowError) {
                    Text(
                        errorMessage,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(12.dp),
                        textAlign = TextAlign.Center,
                        color = MaterialTheme.colorScheme.error
                    )
                }

                Card(onClick = {
                    isShowDemo = !isShowDemo
                }) {
                    Column(
                        modifier = Modifier.padding(12.dp)
                    ) {
                        Row {
                            Text("例如", modifier = Modifier.weight(1f))
                            if (isShowDemo) {
                                Icon(Icons.Default.KeyboardArrowUp, "")
                            } else {
                                Icon(Icons.Default.KeyboardArrowDown, "")
                            }
                        }
                        Spacer(Modifier.height(12.dp))
                        AnimatedVisibility(isShowDemo) {
                            Text(
                                "主题：关于XXX产品功能优化的反馈\n" +
                                        "\n" +
                                        "尊敬的开发团队，\n" +
                                        "\n" +
                                        "我在使用您的XXX产品（版本：1.2.3）时遇到了一个问题，具体表现为当我在执行YYY操作时，ZZZ功能无法正常响应。我已将问题发生时的屏幕截图附在邮件附件中（请参考截图1.png）。\n" +
                                        "\n" +
                                        "步骤复现：\n" +
                                        "1. 打开主界面...\n" +
                                        "2. 点击YYY按钮...\n" +
                                        "3. 观察到ZZZ区域没有正确加载数据...\n" +
                                        "\n" +
                                        "我建议你们能够优化ZZZ功能的数据加载逻辑，或许可以增加进度提示或错误处理机制，以便用户更好地了解当前状态。\n" +
                                        "\n" +
                                        "期待您们能尽快查看并修复这个问题，如有需要进一步的信息，请随时告知。感谢你们一直以来的努力工作和对产品质量的关注！\n" +
                                        "\n" +
                                        "[您的昵称]\n" +
                                        "[联系方式/邮箱]\n",
                                style = MaterialTheme.typography.labelSmall
                            )
                        }
                    }
                }
            }


        }
    }
}