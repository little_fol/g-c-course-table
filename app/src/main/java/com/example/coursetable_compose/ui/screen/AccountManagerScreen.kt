package com.example.coursetable_compose.ui.screen

import android.annotation.SuppressLint
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddCircle
import androidx.compose.material.icons.filled.CheckCircle
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.FabPosition
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.desktop.screenbean.MainPageScreen
import com.example.coursetable_compose.bean.UserBean
import com.example.coursetable_compose.ui.activity.baseRadius
import com.example.coursetable_compose.ui.commen.BaseToolbar
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.viewmodel.BindAccountViewModel
import com.mypotota.datamodel.base.BaseStatus
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun AccountManagerScreen(navController: NavHostController, innerPadding: PaddingValues) {
    var userList by remember {
        mutableStateOf(SPUtils.getIns().userList)
    }
    var addUserDialog by remember {
        mutableStateOf(false)
    }
    val viewModel = viewModel<BindAccountViewModel>()


    Scaffold(
        modifier = Modifier.padding(top = innerPadding.calculateTopPadding()),
        floatingActionButton = {
            FloatingActionButton(onClick = { addUserDialog = true }, shape = baseRadius) {
                Icon(Icons.Default.AddCircle, contentDescription = "")
            }
        },
        floatingActionButtonPosition = FabPosition.Center
    ) {
        Column(
            Modifier
                .fillMaxSize()
        ) {
            BaseToolbar(navController, MainPageScreen.AccountManagerScreen.label)


            userList.forEach {
                Card(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 12.dp, vertical = 6.dp),
                    shape = baseRadius,
                    onClick = {
                        viewModel.getLoginTicket(it.username,it.password)
                    }
                ) {
                    Row(Modifier.padding(12.dp), verticalAlignment = Alignment.CenterVertically) {
                        Column(Modifier.weight(1f)) {
                            Text(text = "账号：${it.username}", textAlign = TextAlign.Center)
                            Text(text = "密码：**********", textAlign = TextAlign.Center)
                        }
                        if (SPUtils.getIns().getAccount().toString() == it.username) {
                            Icon(Icons.Default.CheckCircle, contentDescription = "", tint = MaterialTheme.colorScheme.primary)
                        }
                        IconButton(onClick = { /*TODO*/ }) {
                            Icon(Icons.Default.Delete, contentDescription ="")
                        }
                    }
                }
            }


        }
    }


    if (addUserDialog) {
        AddAccountDialog() {
            userList = SPUtils.getIns().userList
            addUserDialog = false
        }
    }


}

@Composable
fun AddAccountDialog(cancel: () -> Unit) {
    val viewModel = viewModel<BindAccountViewModel>()

    var account by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }
    var tips by remember { mutableStateOf("") }
    var tipsShow by remember { mutableStateOf(false) }

    var onError by remember { mutableStateOf(false) }
    var errorMessage by remember { mutableStateOf("") }

    var onLoading by remember { mutableStateOf(false) }
    var loadingMessage by remember { mutableStateOf("") }

    var onSuccess by remember { mutableStateOf(true) }

    //账号密码绑定状态
    val loginState by viewModel.courseTableStatus.collectAsState()

    val scope = rememberCoroutineScope()


    LaunchedEffect(loginState) {
        //管理绑定状态
        when (val status = loginState) {
            is BaseStatus.ERROR -> {
                onError = true
                onLoading = false
                onSuccess = false
                errorMessage = status.e
            }

            is BaseStatus.LOADING -> {
                onError = false
                onSuccess = false
                onLoading = true
                loadingMessage = status.msg
            }

            is BaseStatus.SUCCESS -> {
                onError = false
                onLoading = false
                onSuccess = true
                //添加用户
                val userList = SPUtils.getIns().userList
                userList.add(UserBean(account, password))
                SPUtils.getIns().userList = userList
                //绑定成功
                scope.launch {
                    delay(1000)
                    cancel()
                }
            }

            null -> {
                onError = false
                onLoading = false
                onSuccess = false
            }
        }
    }



    fun checkAccountIsInclude(account: String): Boolean {
        SPUtils.getIns().userList.forEach {
            if (it.username == account) {
                return true
            }
        }
        return false
    }

    val click: () -> Unit = {
        if (account.isEmpty()) {
            tips = "请输入账号"
            tipsShow = true
            scope.launch {
                delay(1000)
                tipsShow = false
            }
        } else if (password.isEmpty()) {
            tips = "请输入密码"
            tipsShow = true
            scope.launch {
                delay(1000)
                tipsShow = false
            }
        } else if (checkAccountIsInclude(account)) {
            tips = "你不能添加重复的账号"
            tipsShow = true
            scope.launch {
                delay(1000)
                tipsShow = false
            }
        } else {
            SPUtils.getIns().setAccount(account)
            SPUtils.getIns().setPassword(password)
            viewModel.getLoginTicket(account, password)
        }
    }

    AlertDialog(
        onDismissRequest = cancel,
        confirmButton = { /*TODO*/ },
        title = {
            Text(text = "添加新的账号")
        },
        text = {
            Column {
                OutlinedTextField(
                    account, { account = it }, modifier = Modifier.fillMaxWidth(),
                    label = {
                        Text("输入智慧工程账号")
                    }, shape = baseRadius
                )
                Spacer(Modifier.height(4.dp))
                OutlinedTextField(
                    password, { password = it }, modifier = Modifier.fillMaxWidth(),
                    label = {
                        Text("输入智慧工程密码")
                    }, shape = baseRadius
                )
                Spacer(Modifier.height(8.dp))
                //初始状态
                AnimatedVisibility((!onError && !onLoading && !onSuccess)) {
                    Button(
                        click,
                        modifier = Modifier.fillMaxWidth(),
                        shape = baseRadius
                    ) {
                        Text("绑定")
                    }
                }


                //显示错误
                AnimatedVisibility(onError) {
                    Column(
                        Modifier
                            .fillMaxWidth()
                            .padding(12.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        SelectionContainer {
                            Text(
                                text = errorMessage,
                                color = Color.Red,
                            )
                        }
                        Button(onClick = click) {
                            Text(text = "重试")
                        }
                    }
                }

                //绑定成功
                AnimatedVisibility(onSuccess) {
                    Column(
                        Modifier
                            .fillMaxWidth()
                            .padding(12.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Icon(
                            imageVector = Icons.Filled.CheckCircle,
                            "",
                            tint = MaterialTheme.colorScheme.primary
                        )
                        Text("绑定成功")
                    }
                }

                //显示正在加载中
                AnimatedVisibility(onLoading) {
                    Column(
                        Modifier
                            .fillMaxWidth()
                            .padding(12.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        CircularProgressIndicator()
                        Text(text = loadingMessage)
                        Button(onClick = {
                            viewModel.cancelBind()
                        }) {
                            Text(text = "取消")
                        }
                    }
                }

                //显示错误信息提示
                AnimatedVisibility(tipsShow) {
                    Spacer(Modifier.height(8.dp))
                    Text(
                        tips,
                        color = MaterialTheme.colorScheme.error,
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Center
                    )
                }

            }
        }
    )
}
