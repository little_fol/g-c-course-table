package com.example.coursetable_compose.ui.screen

import android.annotation.SuppressLint
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.example.coursetable_compose.R
import com.example.coursetable_compose.ui.activity.baseRadius
import com.mypotota.datamodel.base.BaseStatus
import com.example.coursetable_compose.ui.commen.BaseToolbar
import com.example.coursetable_compose.ui.commen.CourseTableViewPlus
import com.example.coursetable_compose.ui.commen.CourseTopTitle
import com.example.coursetable_compose.ui.commen.LeftSideBar
import com.example.coursetable_compose.ui.commen.TabLayoutView
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.viewmodel.BindAccountViewModel
import com.example.coursetable_compose.viewmodel.MemberCourseTableViewModel
import com.mypotota.datamodel.bean.Kb
import com.tapsdk.lc.LCObject
import kotlin.math.absoluteValue

@OptIn(ExperimentalFoundationApi::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun GroupMemberCourseTableScreen(
    navController: NavHostController,
    lcObject: LCObject,
    innerPadding: PaddingValues
) {

    val account = lcObject.getString("account")
    val password = lcObject.getString("password")

    val bindUserViewModel = viewModel<BindAccountViewModel>()
    val viewModel = viewModel<MemberCourseTableViewModel>()
    val courseTableStatus = bindUserViewModel.courseTableStatus.collectAsState()

    var isLoading by remember { mutableStateOf(false) }
    var loadingMessage by remember { mutableStateOf("") }
    var isError by remember { mutableStateOf(false) }
    var errorMessage by remember { mutableStateOf("") }
    LaunchedEffect(Unit) {
        bindUserViewModel.getLoginTicket(account, password)
    }

    //其他课程
    val selectCurrentWeek = viewModel.selectCurrentWeek.collectAsState()
    val weekCount = viewModel.weekCount.collectAsState()
    val weekMonthMap = viewModel.weekMonthMap.collectAsState()
    //侧栏
    val sideBarList = viewModel.sideBarList.collectAsState()
    //viewPager状态
    val pagerState = rememberPagerState(initialPage = selectCurrentWeek.value, pageCount = {
        weekCount.value + 1
    })
    //课表信息
    val courseTableList = viewModel.courseTableList.collectAsState()
    var showBottomSheet by remember { mutableStateOf(false) }
    //当前选中的课表
    var currentSelectKB by remember {
        mutableStateOf<Kb?>(null)
    }

    val isShowTopDate by remember {
        mutableStateOf(SPUtils.getIns().isShowTopDate)
    }
    val isShowLeftTime by remember {
        mutableStateOf(SPUtils.getIns().isShowLeftTime)
    }

    LaunchedEffect(key1 = selectCurrentWeek.value) {
        pagerState.scrollToPage(selectCurrentWeek.value)
    }
    LaunchedEffect(key1 = pagerState.currentPage) {
        viewModel.selectWeek(pagerState.currentPage)
    }


    LaunchedEffect(courseTableStatus.value) {
        when (val status = courseTableStatus.value) {
            is BaseStatus.ERROR -> {
                isLoading = false
                isError = true
                errorMessage = status.e
            }

            is BaseStatus.LOADING -> {
                isLoading = true
                isError = false
                loadingMessage = status.msg
            }

            is BaseStatus.SUCCESS -> {
                isLoading = false
                isError = false
                viewModel.setCourseTable(status.data)
            }
            null -> {}
        }
    }


    Scaffold(modifier = Modifier.fillMaxSize().padding(top = innerPadding.calculateTopPadding()),
        floatingActionButton = {
            Column {
                FloatingActionButton(onClick = {
                    bindUserViewModel.getLoginTicket(account, password)
                },shape = baseRadius) {
                    Icon(
                        imageVector = Icons.Filled.Refresh,
                        contentDescription = "location",
                        modifier = Modifier.size(24.dp)
                    )
                }
                Spacer(modifier = Modifier.height(8.dp))
                FloatingActionButton(onClick = {
                    viewModel.selectToday()
                },shape = baseRadius) {
                    Icon(
                        painter = painterResource(R.drawable.outline_today_24),
                        contentDescription = "location",
                        modifier = Modifier.size(24.dp)
                    )
                }
            }
        }
    ) {
        Column(Modifier.fillMaxSize()) {
            BaseToolbar(navController, "${lcObject.getString("name")}的课表")
            Column() {
                Column {
                    //错误时
                    AnimatedVisibility(visible = isError) {
                        Card(modifier = Modifier
                            .fillMaxWidth()
                            .padding(12.dp), onClick = {},shape = baseRadius) {
                            Column(
                                Modifier
                                    .fillMaxWidth()
                                    .padding(24.dp),
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                Icon(
                                    Icons.Default.Info, contentDescription = ""
                                )
                                Text(text = errorMessage)
                            }
                        }
                    }
                    //加载中...
                    AnimatedVisibility(visible = isLoading) {
                        Card(modifier = Modifier
                            .fillMaxWidth()
                            .padding(12.dp), onClick = {},shape = baseRadius) {
                            Column(
                                Modifier
                                    .fillMaxWidth()
                                    .padding(24.dp),
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                CircularProgressIndicator()
                                Text(text = loadingMessage)
                            }
                        }
                    }


                }


            }
            AnimatedVisibility(visible = !isLoading && !isError) {
                Column{
                    //tab点击事件
                    TabLayoutView(selectCurrentWeek, weekCount) {
                        viewModel.selectWeek(it)
                    }
                    Column(Modifier.verticalScroll(rememberScrollState())) {
                        Spacer(Modifier.height(8.dp))
                        CourseTopTitle(
                            weekMonthMap,
                            viewModel.currentDate,
                            baseRadius,
                            isShowTopDate
                        )
                        Row {
                            LeftSideBar(sideBarList.value, isShowLeftTime)
                            HorizontalPager(
                                state = pagerState, Modifier.fillMaxSize(),
                                beyondBoundsPageCount = 2
                            ) { pageIndex ->
                                AnimatedVisibility(
                                    visible = pageIndex == pagerState.currentPage,
                                    exit = scaleOut(),
                                    enter = scaleIn()
                                ) {
                                    CourseTableViewPlus(
                                        Modifier
                                            .graphicsLayer {
                                                val pageOffset =
                                                    ((pagerState.currentPage - pageIndex) + pagerState.currentPageOffsetFraction).absoluteValue
                                                alpha = 1f - pageOffset.coerceIn(0f, 1f)
                                            },
                                        courseTableList.value,
                                        viewModel.weekCountDay,
                                        SPUtils.getIns().getColorMapByCourse()
                                    ) {
                                        currentSelectKB = it
                                        showBottomSheet = true
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun MemberCourseTable(refresh: () -> Unit) {
//    var courseTable by remember {
//        mutableStateOf<CourseTable?>(null)
//    }


}
