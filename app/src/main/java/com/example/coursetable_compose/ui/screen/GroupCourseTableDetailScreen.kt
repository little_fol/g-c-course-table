package com.example.coursetable_compose.ui.screen

import android.graphics.Bitmap
import android.widget.Toast
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.KeyboardArrowRight
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.outlined.Home
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.FilledTonalIconButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import cn.bingoogolapple.qrcode.zxing.QRCodeEncoder
import com.desktop.screenbean.MainPageScreen
import com.example.coursetable_compose.R
import com.example.coursetable_compose.ui.activity.baseRadius
import com.mypotota.datamodel.base.BaseStatus
import com.example.coursetable_compose.ui.commen.BaseToolbar
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.utils.Utils
import com.example.coursetable_compose.viewmodel.GroupCourseTableViewModel
import com.tapsdk.lc.LCObject
import com.tapsdk.lc.LCUser
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay

@Composable
fun GroupCourseTableDetailScreen(
    navController: NavHostController,
    lcObject: LCObject,
    innerPadding:PaddingValues,
    isDark: Boolean = isSystemInDarkTheme()
) {
    val viewModel = viewModel<GroupCourseTableViewModel>()
    val isRoot = lcObject.getLCObject<LCUser>("root").objectId.equals(LCUser.currentUser().objectId)
    viewModel.getGroupJoinList(lcObject)

    var showJoinDialog by remember {
        mutableStateOf(false)
    }
    var dissolveDialog by remember {
        mutableStateOf(false)
    }
    var showExitGroup by remember {
        mutableStateOf(false)
    }
    var scanCodeBitmap by remember {
        mutableStateOf<Bitmap?>(null)
    }
    var currentLcObject by remember {
        mutableStateOf<LCObject?>(null)
    }
    var showKickOutDialog by remember {
        mutableStateOf(false)
    }

    Column(Modifier.fillMaxSize().padding(top = innerPadding.calculateTopPadding())) {
        BaseToolbar(navController, MainPageScreen.GroupCourseTableDetailScreen.label)
        Column(
            Modifier
                .fillMaxSize()
                .verticalScroll(rememberScrollState())
        ) {
            GroupIntoMenuCard(lcObject, isRoot, exit = {
                //退出
                showExitGroup = true
            }, dissolve = {
                //解散
                dissolveDialog = true
            }, joinBtn = {
                scanCodeBitmap = QRCodeEncoder.syncEncodeQRCode(lcObject.objectId, 200)
                showJoinDialog = true
            })
            Text(
                text = "小组成员",
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier.padding(12.dp)
            )
            GroupMember(navController, isRoot) { lcObject ->
                currentLcObject = lcObject
                showKickOutDialog = true
            }
        }
    }

    if (showExitGroup) {
        ExitGroupDialog(lcObject, {
            showExitGroup = false
        }) {
            showExitGroup = false
            navController.popBackStack()
        }
    }

    //踢出成员
    if (showKickOutDialog) {
        KickOutDialog(currentLcObject!!, {
            showKickOutDialog = false
        }) {
            showKickOutDialog = false
            viewModel.getGroupJoinList(lcObject)
        }
    }

    //解散小组
    if (dissolveDialog) {
        DissolveDialog(lcObject, {
            dissolveDialog = false
            navController.popBackStack()
            Toast.makeText(navController.context, "解散成功", Toast.LENGTH_SHORT).show()
        }) {
            dissolveDialog = false
        }
    }

    //为小组添加成员
    if (showJoinDialog) {
        AlertDialog(
            onDismissRequest = { showJoinDialog = false },
            confirmButton = { /*TODO*/ },
            shape = baseRadius,
            title = {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Text(
                        text = "添加成员",
                        style = MaterialTheme.typography.titleLarge,
                        modifier = Modifier.weight(
                            1f
                        )
                    )
                    FilledTonalIconButton(onClick = { showJoinDialog = false },shape = baseRadius) {
                        Icon(imageVector = Icons.Filled.Close, contentDescription = "")
                    }
                }
            },
            text = {
                Column(
                    Modifier.fillMaxWidth(),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Card(
                        colors = CardDefaults.cardColors(containerColor = Color.White),
                        modifier = Modifier.size(200.dp),
                        shape = baseRadius
                    ) {
                        Image(bitmap = scanCodeBitmap!!.asImageBitmap(), contentDescription = "",
                            modifier = Modifier.fillMaxSize())
//                        GlideImage(
//                            model = scanCodeBitmap,
//                            contentDescription = "",
//                            modifier = Modifier
//                                .fillMaxSize()
//                        )
                    }
                    TextButton(modifier = Modifier.fillMaxWidth(),shape = baseRadius, onClick = {
                        Utils.copy(navController.context, lcObject.objectId)
                    }) {
                        Text(
                            text = "小组ID：${lcObject.objectId}",
                            style = MaterialTheme.typography.labelSmall
                        )
                    }
                    Text(text = "复制小组ID也能加入小组哟")
                }
            }
        )
    }
}

@Composable
fun ExitGroupDialog(lcObject: LCObject, dismiss: () -> Unit, exit: () -> Unit) {
    val viewModel = viewModel<GroupCourseTableViewModel>()
    val exitGroupStatus = viewModel.exitGroupStatus.collectAsState()
    val context = LocalContext.current


    LaunchedEffect(exitGroupStatus.value) {
        when (val status = exitGroupStatus.value) {
            is BaseStatus.ERROR -> {
                Toast.makeText(context, "${status.e}", Toast.LENGTH_SHORT).show()
                viewModel.cancelStatus()
                dismiss()
            }

            is BaseStatus.LOADING -> {
            }

            is BaseStatus.SUCCESS -> {
                Toast.makeText(context, "退出成功", Toast.LENGTH_SHORT).show()
                exit()
            }

            null -> {
            }
        }
    }
    AlertDialog(onDismissRequest = {
        dismiss()
    },shape = baseRadius, confirmButton = {
        Button(
            onClick = {
                viewModel.exitGroup(lcObject)
            },shape = baseRadius) {
            Text(text = "确定")
        }
    }, dismissButton = {
        OutlinedButton(
            onClick = {
                dismiss()
            },shape = baseRadius) {
            Text(text = "取消")
        }
    }, title = {
        Text(text = "退出该小组", style = MaterialTheme.typography.titleLarge)
    }, text = {
        Text(text = "你确定要退出${lcObject.getString("name")}小组吗，此操作不可逆！")
    })
}

@Composable
fun KickOutDialog(lcObject: LCObject, dismiss: () -> Unit, exit: () -> Unit) {
    val viewModel = viewModel<GroupCourseTableViewModel>()
    val kickOutStatus = viewModel.kickOutStatus.collectAsState()
    val context = LocalContext.current

    LaunchedEffect(kickOutStatus.value) {
        when (val status = kickOutStatus.value) {
            is BaseStatus.ERROR -> {
                Toast.makeText(context, "踢出成员失败", Toast.LENGTH_SHORT).show()
                viewModel.cancelStatus()
                dismiss()
            }

            is BaseStatus.LOADING -> {
            }

            is BaseStatus.SUCCESS -> {
                Toast.makeText(context, "踢出成员成功", Toast.LENGTH_SHORT).show()
                viewModel.cancelStatus()
                exit()
            }

            null -> {
            }
        }
    }

    AlertDialog(onDismissRequest = {
        dismiss()
    },shape = baseRadius, confirmButton = {
        Button(
            onClick = {
                viewModel.kickOut(lcObject.objectId)
            },shape = baseRadius) {
            Text(text = "确定")
        }
    }, dismissButton = {
        OutlinedButton(
            onClick = {
                dismiss()
            },shape = baseRadius) {
            Text(text = "取消")
        }
    }, title = {
        Text(text = "踢出成员", style = MaterialTheme.typography.titleLarge)
    }, text = {
        Text(text = "你确定要踢出${lcObject.getString("name")}成员吗，此操作不可逆！")
    })
}

@Composable
fun DissolveDialog(lcObject: LCObject, exit: () -> Unit, dismiss: () -> Unit) {
    val viewModel = viewModel<GroupCourseTableViewModel>()

    val dissolveGroupStatus = viewModel.dissolveGroupStatus.collectAsState()
    var isLoading by remember { mutableStateOf(true) }
    var loadingMessage by remember { mutableStateOf("") }
    var isError by remember { mutableStateOf(false) }
    var errorMessage by remember { mutableStateOf("") }

    LaunchedEffect(dissolveGroupStatus.value) {
        coroutineScope {
            when (val status = dissolveGroupStatus.value) {
                is BaseStatus.ERROR -> {
                    isLoading = false
                    isError = true
                    errorMessage = status.e
                    delay(2000)
                    isError = false
                    viewModel.cancelStatus()
                    dismiss()
                }

                is BaseStatus.LOADING -> {
                    isLoading = true
                    isError = false
                    loadingMessage = status.msg
                }

                is BaseStatus.SUCCESS -> {
                    isLoading = false
                    isError = false
                    isError = false
                    viewModel.cancelStatus()
                    exit()
                }

                null -> {
                    isLoading = false
                    isError = false
                }
            }
        }
    }

    AlertDialog(onDismissRequest = {
        if (!isError && !isLoading) {
            dismiss()
        }
    },shape = baseRadius, confirmButton = {
        Button(
            enabled = !isError && !isLoading,
            onClick = {
                viewModel.dissolveGroup(lcObject)
            },shape = baseRadius) {
            Text(text = "解散")
        }
    }, dismissButton = {
        OutlinedButton(
            enabled = !isError && !isLoading,
            onClick = {
                dismiss()
            },shape = baseRadius) {
            Text(text = "取消")
        }
    }, title = {
        Text(text = "解散小组？", style = MaterialTheme.typography.titleLarge)
    }, text = {
        Column {
            AnimatedVisibility(visible = !isLoading && !isError) {
                Text(
                    text = "你确定要解散这个小组？该操作不可逆，请三思而后行，解散后所有成员将会被清除，\n你确定？\n你确定？\n你确定？",
                    style = MaterialTheme.typography.bodyMedium
                )
            }

            //错误时
            AnimatedVisibility(visible = isError) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Icon(
                        Icons.Default.Info, contentDescription = ""
                    )
                    Text(text = errorMessage)
                }
            }
            //加载中...
            AnimatedVisibility(visible = isLoading) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CircularProgressIndicator()
                    Text(text = loadingMessage)
                }
            }
        }
    })
}

@Composable
fun GroupMember(
    navController: NavHostController,
    isRoot: Boolean,
    kickOutListener: (lcObject: LCObject) -> Unit
) {
    val viewModel = viewModel<GroupCourseTableViewModel>()
    val groupCourseTableListStatus = viewModel.groupCourseTableListStatus.collectAsState()


    val scope = rememberCoroutineScope()
    var isLoading by remember { mutableStateOf(true) }
    var loadingMessage by remember { mutableStateOf("") }
    var isError by remember { mutableStateOf(false) }
    var errorMessage by remember { mutableStateOf("") }
    var createGroupList by remember { mutableStateOf<List<LCObject>>(ArrayList()) }


    LaunchedEffect(groupCourseTableListStatus.value) {
        coroutineScope {
            when (val status = groupCourseTableListStatus.value) {
                is BaseStatus.ERROR -> {
                    isLoading = false
                    isError = true
                    errorMessage = status.e
                }

                is BaseStatus.LOADING -> {
                    isLoading = true
                    isError = false
                    loadingMessage = status.msg
                }

                is BaseStatus.SUCCESS -> {
                    isLoading = false
                    isError = false
                    createGroupList = status.data
                }
            }
        }
    }

    Column {
        //错误时
        AnimatedVisibility(visible = isError) {
            Card(modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp), onClick = {},shape = baseRadius) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Icon(
                        Icons.Default.Info, contentDescription = ""
                    )
                    Text(text = errorMessage)
                }
            }
        }
        //加载中...
        AnimatedVisibility(visible = isLoading) {
            Card(modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp), onClick = {},shape = baseRadius) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CircularProgressIndicator()
                    Text(text = loadingMessage)
                }
            }
        }

        //为空时
        AnimatedVisibility(visible = !isError && !isLoading && createGroupList.isEmpty()) {
            Card(modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp), onClick = {},shape = baseRadius) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(24.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Icon(
                        painterResource(id = R.drawable.outline_not_interested_24),
                        contentDescription = ""
                    )
                    Text(text = "小组中暂无成员")
                }
            }
        }


        //有数据时-显示成员
        AnimatedVisibility(visible = !isError && !isLoading && createGroupList.isNotEmpty()) {
            Column {
                createGroupList.forEachIndexed { index, lcObject ->
                    key(lcObject.objectId) {
                        Card(modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 12.dp),
                            shape = baseRadius,
                            onClick = {
                                //进入小组课表详情
                                navController.navigate("${MainPageScreen.GroupMemberCourseTableScreen.name}/data=${lcObject.toJSONString()}")
                            }) {
                            Row(
                                Modifier
                                    .fillMaxWidth()
                                    .padding(12.dp),
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                Column(
                                    modifier = Modifier.weight(
                                        1f
                                    )
                                ) {
                                    Text(
                                        text = lcObject.getString("name"),
                                        style = MaterialTheme.typography.labelLarge
                                    )
                                    Text(
                                        text = lcObject.getString("class"),
                                        style = MaterialTheme.typography.labelMedium
                                    )
                                    Text(
                                        text = lcObject.getString("account"),
                                        style = MaterialTheme.typography.labelSmall
                                    )
                                }
                                if (isRoot) {
                                    TextButton(onClick = {
                                        kickOutListener(lcObject)
                                    },shape = baseRadius) {
                                        Text(text = "踢出")
                                    }
                                }
                                Icon(
                                    Icons.AutoMirrored.Filled.KeyboardArrowRight,
                                    contentDescription = ""
                                )
                            }
                        }
                        Spacer(modifier = Modifier.height(4.dp))
                    }
                }
            }
        }


    }
}

@Composable
fun GroupIntoMenuCard(
    lcObject: LCObject,
    isRoot: Boolean,
    exit: () -> Unit,
    dissolve: () -> Unit,
    joinBtn: () -> Unit
) {
    var isShowHome by remember {
        mutableStateOf(
            SPUtils.getIns().getHomeShowGroupList()
            .contains(lcObject.objectId))
    }
    //显示小组卡片
    Card(
        Modifier
            .padding(12.dp)
            .fillMaxWidth(),shape = baseRadius
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = lcObject.getString("name"),
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier.weight(1f)
            )
            IconButton(onClick = {
                isShowHome = if (isShowHome){
                    SPUtils.getIns().removeHomeShowGroupList(lcObject.objectId)
                    SPUtils.getIns().getHomeShowGroupList()
                        .contains(lcObject.objectId)
                }else{
                    SPUtils.getIns().addHomeShowGroupList(lcObject.objectId)
                    SPUtils.getIns().getHomeShowGroupList()
                        .contains(lcObject.objectId)
                }
            }) {
                Icon(
                    if (isShowHome) Icons.Default.Home else Icons.Outlined.Home,
                    ""
                )
            }
            //判断是否时创建者如果是就显示操作按钮
            if (isRoot) {
                TextButton(onClick = joinBtn,shape = baseRadius) {
                    Text(text = "添加成员")
                }
                TextButton(onClick = dissolve,shape = baseRadius) {
                    Text(text = "解散")
                }
            } else {
                TextButton(onClick = exit,shape = baseRadius) {
                    Text(text = "退出")
                }
            }
        }
    }
}
