package com.example.coursetable_compose.ui.screen

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.FileUtils
import android.provider.MediaStore
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContract
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Slider
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.desktop.screenbean.MainPageScreen
import com.example.coursetable_compose.constant.COURSE_TABLE_ITEM_HEIGHT
import com.example.coursetable_compose.ui.activity.baseRadius
import com.example.coursetable_compose.ui.commen.BaseToolbar
import com.example.coursetable_compose.ui.commen.CourseTableViewPlus
import com.example.coursetable_compose.ui.commen.CourseTopTitle
import com.example.coursetable_compose.ui.commen.LeftSideBar
import com.example.coursetable_compose.ui.commen.TabLayoutView
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.utils.Utils
import com.example.coursetable_compose.viewmodel.CourseTableViewModel
import com.hjq.permissions.Permission
import com.hjq.permissions.XXPermissions
import com.orhanobut.logger.Logger
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.util.UUID

@Composable
fun CourTableSetting(navController: NavHostController, innerPadding: PaddingValues) {
    val context = LocalContext.current
    val courseTableViewModel = viewModel<CourseTableViewModel>()


    val xxPermission = XXPermissions.with(context)
        .permission(Permission.MANAGE_EXTERNAL_STORAGE)

    var filePath by remember {
        mutableStateOf(SPUtils.getIns().getBackgroundImagePath())
    }
    //
    var backgroundAlpha by remember {
        mutableFloatStateOf(SPUtils.getIns().getBackgroundAlpha())
    }
    var radius by remember {
        mutableIntStateOf(SPUtils.getIns().getRadius())
    }
    var courseTableHeight by remember {
        mutableIntStateOf(SPUtils.getIns().courseTableHeight)
    }
    var isShowTopDate by remember {
        mutableStateOf(SPUtils.getIns().isShowTopDate)
    }
    var isShowLeftTime by remember {
        mutableStateOf(SPUtils.getIns().isShowLeftTime)
    }

    val selectCurrentWeek = courseTableViewModel.selectCurrentWeek.collectAsState()
    val weekCount = courseTableViewModel.weekCount.collectAsState()
    val weekMonthMap = courseTableViewModel.weekMonthMap.collectAsState()
    //侧栏
    val sideBarList = courseTableViewModel.sideBarList.collectAsState()
    //课表信息
    val courseTableList = courseTableViewModel.courseTableList.collectAsState()
    var backgroundAlphaDialog by remember {
        mutableStateOf(false)
    }
    var radiusDialog by remember {
        mutableStateOf(false)
    }
    var courseTableHeightDialog by remember {
        mutableStateOf(false)
    }


    val launcher = rememberLauncherForActivityResult(SelectPhotoContract()) { uri ->
        if (uri == null) {
        } else {
            val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
            val cursor = context.contentResolver.query(uri, filePathColumn, null, null, null);
            cursor?.moveToFirst();
            val columnIndex = cursor?.getColumnIndex(filePathColumn[0]);
            val picturePath = cursor?.getString(columnIndex!!)
            //把文件移动到软件目录下
            val backgroundDir = File(context.filesDir, "background")
            if (!backgroundDir.exists()) {
                backgroundDir.mkdirs()
            }
            val backgroundFile = File(backgroundDir, "${UUID.randomUUID()}-backImage.jpg")
            if (backgroundFile.exists()) {
                println("文件删除")
                backgroundFile.delete()
            }
            val fileOutputStream = FileOutputStream(backgroundFile)
            val file = File(picturePath.toString())
            val fileInputStream = FileInputStream(file)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                FileUtils.copy(fileInputStream, fileOutputStream)
            } else {
                //-移动出粗了
                Toast.makeText(context, "无法更换背景", Toast.LENGTH_SHORT).show()
            }
            fileInputStream.close()
            fileOutputStream.close()
            if (SPUtils.getIns().getBackgroundImagePath() != null) {
                val oldFile = File(SPUtils.getIns().getBackgroundImagePath().toString())
                if (oldFile.exists()) {
                    oldFile.delete()
                }
            }
            SPUtils.getIns().setBackgroundImagePath(backgroundFile.path)
            filePath = SPUtils.getIns().getBackgroundImagePath()
        }
    }


    Column(
        Modifier
            .fillMaxSize()
            .padding(top = innerPadding.calculateTopPadding())) {
        BaseToolbar(navController, MainPageScreen.CourTableSetting.label)
        Column(Modifier) {
            Box(modifier = Modifier.weight(4f)) {
                if (!filePath.isNullOrEmpty()) {
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(MaterialTheme.colorScheme.background)
                            .alpha(backgroundAlpha)
                    ) {
                        Image(bitmap = Utils.filePath2Bitmap(filePath?:""), contentDescription = "",contentScale = ContentScale.Crop,
                            modifier = Modifier.fillMaxSize())
                    }
                }
                Column(Modifier.verticalScroll(rememberScrollState())) {
                    //tab点击事件
                    TabLayoutView(selectCurrentWeek, weekCount) {}
                    Column(Modifier) {
                        Spacer(Modifier.height(8.dp))
                        CourseTopTitle(weekMonthMap, courseTableViewModel.currentDate, baseRadius,isShowTopDate)
                        Row {
                            LeftSideBar(sideBarList.value,isShowLeftTime)
                            CourseTableViewPlus(
                                Modifier,
                                courseTableList.value,
                                courseTableViewModel.weekCountDay,
                                SPUtils.getIns().getColorMapByCourse()
                            ) {}
                        }
                    }
                }

            }
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(6f)
                    .verticalScroll(rememberScrollState())
                    .padding(12.dp)
            ) {

                TextButton(
                    onClick = {
                        if (XXPermissions.isGranted(context, Permission.MANAGE_EXTERNAL_STORAGE)) {
                            launcher.launch(null)
                        } else {
                            xxPermission.request { strings, b ->
                                if (b) {
                                    //获取成功
                                    launcher.launch(null)
                                } else {
                                    Toast.makeText(
                                        context,
                                        "这些权限是必要权限，请授予",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                        }
                    },
                    modifier = Modifier.fillMaxWidth(),
                    shape = RoundedCornerShape(10.dp)
                ) {
                    Column(Modifier.fillMaxWidth()) {
                        Text(text = "背景图片", style = MaterialTheme.typography.labelLarge)
                    }
                }
                TextButton(
                    onClick = { backgroundAlphaDialog = true },
                    modifier = Modifier.fillMaxWidth(),
                    shape = RoundedCornerShape(10.dp)
                ) {
                    Text(
                        text = "背景透明度",
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Left
                    )
                }
                TextButton(
                    onClick = { radiusDialog = true },
                    modifier = Modifier.fillMaxWidth(),
                    shape = RoundedCornerShape(10.dp)
                ) {
                    Text(
                        text = "圆角调节",
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Left
                    )
                }
                TextButton(
                    onClick = { courseTableHeightDialog = true },
                    modifier = Modifier.fillMaxWidth(),
                    shape = RoundedCornerShape(10.dp)
                ) {
                    Text(
                        text = "课表高度调节",
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Left
                    )
                }

                TextButton(
                    onClick = {
                        SPUtils.getIns().isShowLeftTime=!isShowLeftTime
                        isShowLeftTime=SPUtils.getIns().isShowLeftTime
                    },
                    modifier = Modifier.fillMaxWidth(),
                    shape = RoundedCornerShape(10.dp)
                ) {
                    Text(
                        text = "侧栏时间显示",
                        modifier = Modifier.weight(1f),
                        textAlign = TextAlign.Left
                    )
                    Switch(checked = isShowLeftTime, onCheckedChange = null)
                }

                TextButton(
                    onClick = {
                        SPUtils.getIns().isShowTopDate=!isShowTopDate
                        isShowTopDate=SPUtils.getIns().isShowTopDate
                    },
                    modifier = Modifier.fillMaxWidth(),
                    shape = RoundedCornerShape(10.dp)
                ) {
                    Text(
                        text = "顶栏日期显示",
                        modifier = Modifier.weight(1f),
                        textAlign = TextAlign.Left
                    )
                    Switch(checked = isShowTopDate, onCheckedChange = null)
                }



                TextButton(
                    onClick = {
                        SPUtils.getIns().setBackgroundImagePath(null)
                        filePath = null
                    },
                    modifier = Modifier.fillMaxWidth(),
                    shape = RoundedCornerShape(10.dp)
                ) {
                    Text(
                        text = "背景恢复默认",
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Left
                    )
                }
            }


        }
    }


    if (backgroundAlphaDialog) {
        AlertDialog(onDismissRequest = {
            backgroundAlphaDialog = false
        }, confirmButton = { },
            title = {
                Text(text = "背景不透明度", style = MaterialTheme.typography.titleLarge)
            }, text = {
                Column {
                    Slider(
                        value = backgroundAlpha,
                        onValueChange = {
                            SPUtils.getIns().setBackgroundAlpha(it)
                            backgroundAlpha = SPUtils.getIns().getBackgroundAlpha()
                        }
                    )
                }
            }
        )
    }


    if (radiusDialog) {
        val maxRadius=30
        AlertDialog(onDismissRequest = {
            radiusDialog = false
        }, confirmButton = { },
            title = {
                Text(text = "圆角度数", style = MaterialTheme.typography.titleLarge)
            }, text = {
                Column {
                    Slider(
                        value = (radius/maxRadius.toFloat()),
                        onValueChange = {
                            val newRadius = (it * maxRadius).toInt()
                            SPUtils.getIns().setRadius(newRadius)
                            radius = SPUtils.getIns().getRadius()
                            baseRadius=RoundedCornerShape(newRadius)
                        }
                    )
                }
            }
        )
    }

    if (courseTableHeightDialog) {
        val maxHeight=65
        AlertDialog(onDismissRequest = {
            courseTableHeightDialog = false
        }, confirmButton = { },
            title = {
                Text(text = "课表高度调节", style = MaterialTheme.typography.titleLarge)
            }, text = {
                Column {
                    Slider(
                        value = (courseTableHeight/maxHeight.toFloat()),
                        onValueChange = {
                            val newHeight = (it * maxHeight).toInt()
                            SPUtils.getIns().courseTableHeight=newHeight
                            courseTableHeight = SPUtils.getIns().courseTableHeight
                            COURSE_TABLE_ITEM_HEIGHT=courseTableHeight.dp
                        }
                    )
                }
            }
        )
    }
//    if (courseTableRoundDialog) {
//        AlertDialog(onDismissRequest = {
//            courseTableRoundDialog = false
//        }, confirmButton = { },
//            title = {
//                Text(text = "课表圆角度数", style = MaterialTheme.typography.titleLarge)
//            }, text = {
//                Column {
//                    Slider(
//                        value = backgroundAlpha,
//                        onValueChange = {
//                            SPUtils.getIns().setBackgroundAlpha(it)
//                            backgroundAlpha = SPUtils.getIns().getBackgroundAlpha()
//                        }
//                    )
//                }
//            }
//        )
//    }
}

class SelectPhotoContract : ActivityResultContract<Unit?, Uri?>() {
    companion object {
        private const val TAG = "SelectPhotoContract"
    }

    override fun createIntent(context: Context, input: Unit?): Intent {
        return Intent(Intent.ACTION_PICK).setType("image/*")
    }

    override fun parseResult(resultCode: Int, intent: Intent?): Uri? {
        Logger.d(TAG, "Select photo uri: ${intent?.data}")
        return intent?.data
    }
}