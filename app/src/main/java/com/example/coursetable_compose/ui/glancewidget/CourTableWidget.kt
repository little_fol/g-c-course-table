package com.example.coursetable_compose.ui.glancewidget

import android.appwidget.AppWidgetManager
import android.content.Context
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.glance.GlanceId
import androidx.glance.GlanceModifier
import androidx.glance.GlanceTheme
import androidx.glance.Image
import androidx.glance.ImageProvider
import androidx.glance.action.actionStartActivity
import androidx.glance.action.clickable
import androidx.glance.appwidget.GlanceAppWidget
import androidx.glance.appwidget.GlanceAppWidgetReceiver
import androidx.glance.appwidget.lazy.LazyColumn
import androidx.glance.appwidget.lazy.items
import androidx.glance.appwidget.provideContent
import androidx.glance.background
import androidx.glance.layout.Alignment
import androidx.glance.layout.Box
import androidx.glance.layout.Column
import androidx.glance.layout.ContentScale
import androidx.glance.layout.Row
import androidx.glance.layout.Spacer
import androidx.glance.layout.fillMaxHeight
import androidx.glance.layout.fillMaxSize
import androidx.glance.layout.fillMaxWidth
import androidx.glance.layout.height
import androidx.glance.layout.padding
import androidx.glance.layout.size
import androidx.glance.layout.width
import androidx.glance.material3.ColorProviders
import androidx.glance.text.FontWeight
import androidx.glance.text.Text
import androidx.glance.text.TextAlign
import androidx.glance.text.TextStyle
import androidx.glance.unit.ColorProvider
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.example.coursetable_compose.R
import com.example.coursetable_compose.ui.activity.MainActivity
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.utils.Utils
import com.example.coursetable_compose.workmanager.WidgetUpdaterWorker
import com.mypotota.datamodel.model.CourseTableModel
import com.mypotota.datamodel.bean.CourseTable
import com.mypotota.datamodel.bean.Kb
import com.orhanobut.logger.Logger
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.Date
import java.util.HashMap
import java.util.concurrent.TimeUnit

object MyAppWidgetGlanceColorScheme {
    val colors = ColorProviders(
        light = lightColorScheme(),
        dark = darkColorScheme()
    )
}

class GlanceDemoReceiver : GlanceAppWidgetReceiver() {
    private val AppWidgetTAG = "AppWidgetTAG"
    override val glanceAppWidget: GlanceAppWidget = CourTableWidget()


    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)
        Logger.d("触发更新了")
    }

    override fun onEnabled(context: Context) {
        super.onEnabled(context)
        //每隔3小时刷新一次
        val periodicUpdateRequest =
            PeriodicWorkRequestBuilder<WidgetUpdaterWorker>(
                6, TimeUnit.HOURS,
                5, TimeUnit.MINUTES
            ).build()
        //添加UniqueWork，不用怕会重复添加，这里的TAG也可以用来取消任务
        WorkManager.getInstance(context)
            .enqueueUniquePeriodicWork(
                AppWidgetTAG,
                ExistingPeriodicWorkPolicy.KEEP,
                periodicUpdateRequest
            )
    }

    override fun onDisabled(context: Context?) {
        super.onDisabled(context)
        //根据TAG取消掉任务
        WorkManager.getInstance(context!!).cancelUniqueWork(AppWidgetTAG)
    }


}


class CourTableWidget : GlanceAppWidget() {
    override suspend fun provideGlance(context: Context, id: GlanceId) {
        provideContent {
            GlanceTheme(colors = MyAppWidgetGlanceColorScheme.colors) {
                WidgetUI()
            }
        }
    }
}


@Composable
fun WidgetUI() {
    val courseTable = SPUtils.getIns().getCourseTable()
    val colorMapByCourse = SPUtils.getIns().getColorMapByCourse()
    if (courseTable == null) {
        Row(
            GlanceModifier.fillMaxSize().clickable {
                actionStartActivity<MainActivity>()
            },
            verticalAlignment = Alignment.CenterVertically,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Text(
                text = "你未有绑定课表，无法使用小组件\n请绑定后删除组件，重新添加组件",
                style = TextStyle(color = GlanceTheme.colors.secondary)
            )
        }
        return
    }
    val courseTableModel by lazy {
        CourseTableModel()
    }
    val date = SimpleDateFormat("MM.dd").format(Date())
    val updateTime = SimpleDateFormat("HH:mm").format(Date())
    val currentWeekNumber by remember {
        mutableIntStateOf(
            Utils.currentWeekNumber(
                SPUtils.getIns().getStartFirstWeekDate().toString()
            )
        )
    }
    //获取今天周几
    val nowWeekNumber by remember {
        mutableStateOf(Utils.weekNumber2String(LocalDate.now().dayOfWeek.value))
    }
    var toDayCourseTable by remember {
        mutableStateOf<List<Kb>>(ArrayList())
    }
    var tomorrowCourseTable by remember {
        mutableStateOf<List<Kb>>(ArrayList())
    }
    LaunchedEffect(key1 = Unit) {
        toDayCourseTable = courseTableModel.getToDayCourseTable(currentWeekNumber, courseTable)
        tomorrowCourseTable =
            courseTableModel.getTomorrowCourseTable(currentWeekNumber, courseTable)
    }
    Logger.d("更新了：$updateTime")

        Column(
            GlanceModifier.fillMaxSize().background(GlanceTheme.colors.background)
                .padding(top = 12.dp, start = 12.dp, end = 12.dp)
        ) {
            TopTitleBar(courseTable, date, currentWeekNumber, nowWeekNumber, updateTime)
            Spacer(modifier = GlanceModifier.height(8.dp))
            Row(modifier = GlanceModifier.fillMaxWidth().defaultWeight()) {
                LazyColumn(modifier = GlanceModifier.defaultWeight()) {
                    item {
                        Text(
                            text = "今天", style = TextStyle(
                                fontSize = 12.sp,
                                color = GlanceTheme.colors.secondary
                            )
                        )
                    }
                    item {
                        if (toDayCourseTable.isEmpty()) {
                            EmptyText("╰(●’◡’●)╮\n今天没有课程啦")
                        }
                    }
                    items(items = toDayCourseTable) {
                        CourseTableItem(it, colorMapByCourse)
                    }
                }
                LazyColumn(modifier = GlanceModifier.defaultWeight()) {
                    item {
                        Text(
                            text = "明天", style = TextStyle(
                                fontSize = 12.sp,
                                color = GlanceTheme.colors.secondary
                            )
                        )
                    }
                    item {
                        if (tomorrowCourseTable.isEmpty()) {
                            EmptyText("(๑•́ ₃ •̀๑)ｴｰ\n明天也可以休息啦～")
                        }
                    }
                    items(items = tomorrowCourseTable) {
                        CourseTableItem(it, colorMapByCourse)
                    }
                }
            }
            Row(verticalAlignment = Alignment.CenterVertically, modifier = GlanceModifier.fillMaxWidth().padding(3.dp)){
                Image(
                    provider = ImageProvider(R.drawable.app_logo),
                    contentDescription ="",
                    modifier = GlanceModifier.width(30.dp).height(15.dp),
                    contentScale = ContentScale.Crop
                )
                Text(
                    text = "刷新时间：$updateTime",
                    style = TextStyle(
                        fontSize = 8.sp, textAlign = TextAlign.End,
                        color = GlanceTheme.colors.secondary, fontWeight = FontWeight.Bold
                    ),
                    modifier = GlanceModifier.defaultWeight()
                )
            }
        }
}

@Composable
fun EmptyText(msg: String) {
    Text(
        text = msg,
        style = TextStyle(
            fontSize = 12.sp, textAlign = TextAlign.Center,
            color = GlanceTheme.colors.primary, fontWeight = FontWeight.Bold
        ),
        modifier = GlanceModifier.fillMaxWidth()
    )
}

@Composable
fun TopTitleBar(
    courseTable: CourseTable,
    date: String,
    currentWeekNumber: Int,
    nowWeekNumber: String,
    updateTime: String
) {
    Row(modifier = GlanceModifier.fillMaxWidth()) {
        Text(
            text = "${courseTable.xsxx.XM}的课表",
            style = TextStyle(
                fontSize = 12.sp,
                color = GlanceTheme.colors.secondary
            ),
            modifier = GlanceModifier.defaultWeight()
        )
        Spacer(GlanceModifier.width(8.dp))
        Text(
            text = date, style = TextStyle(
                fontSize = 12.sp,
                color = GlanceTheme.colors.secondary
            )
        )
        Spacer(GlanceModifier.width(8.dp))
        Text(
            text = "第${currentWeekNumber}周", style = TextStyle(
                fontSize = 12.sp,
                color = GlanceTheme.colors.secondary
            )
        )
        Spacer(GlanceModifier.width(8.dp))
        Text(
            text = nowWeekNumber,
            style = TextStyle(
                fontSize = 12.sp,
                color = GlanceTheme.colors.primary, fontWeight = FontWeight.Bold
            )
        )
    }
}

@Composable
fun CourseTableItem(it: Kb, colorMapByCourse: HashMap<String, Long>) {
    Row(
        GlanceModifier.padding(horizontal = 2.dp, vertical = 4.dp).fillMaxWidth()
            .clickable(actionStartActivity<MainActivity>())
    ) {
        Column(
            GlanceModifier.width(6.dp).fillMaxHeight()
                .background(Color(Utils.getCourseColor(it, colorMapByCourse)))
        ) {}
        Spacer(GlanceModifier.width(8.dp))
        Column {
            Text(
                text = it.kcmc,
                style = TextStyle(
                    color = GlanceTheme.colors.secondary,
                    fontWeight = FontWeight.Bold,
                    fontSize = 12.sp
                ),
                maxLines = 1,
            )
            Text(
                text = "${it.cdmc}  ${it.xm}",
                style = TextStyle(
                    fontSize = 12.sp,
                    color = ColorProvider(Color(0xff999999))
                ),
                maxLines = 1
            )
            Text(
                text = Utils.kbJcTime(it.jc),
                style = TextStyle(
                    fontSize = 10.sp,
                    color = ColorProvider(Color(0xff999999))
                )
            )
        }
    }
}
