package com.example.coursetable_compose.screenbean

import com.example.coursetable_compose.R

enum class HomePageScreen(
    val label: String,
    val icon: Int
) {
    HomePage("主页", R.drawable.outline_home_24),
    CourTablePage("课表", R.drawable.outline_table_chart_24),
    SettingPage("设置", R.drawable.outline_settings_24);


    companion object {
        fun getLabel(name:String): HomePageScreen {
            return when(name){
                HomePage.name-> HomePage
                CourTablePage.name-> CourTablePage
                SettingPage.name-> SettingPage
                else-> HomePage
            }
        }
    }
}