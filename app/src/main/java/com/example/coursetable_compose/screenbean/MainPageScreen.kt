package com.desktop.screenbean

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Menu
import androidx.compose.ui.graphics.vector.ImageVector

enum class MainPageScreen(
    val label: String,
    val icon: ImageVector
) {
    HomeScreen(
        label = "home",
        icon = Icons.Filled.Home
    ),
    BindAccountScreen(
        label = "绑定账号",
        icon = Icons.Filled.Home
    ),
    VersionManage(
        label = "关于此版本",
        icon = Icons.Filled.Menu
    ),
    PaletteScreen(
        label = "课表配色",
        icon = Icons.Filled.AccountCircle
    ),
    MainPageSettingScreen(
        label = "主页组件显示设置",
        icon = Icons.Filled.AccountCircle
    ),
    MineGradesScreen(
        label = "我的成绩",
        icon = Icons.Filled.AccountCircle
    ),
    GroupCourseTable(
        label = "小组课表",
        icon = Icons.Filled.AccountCircle
    ),
    GroupCourseTableDetailScreen(
        label = "小组详情",
        icon = Icons.Filled.AccountCircle
    ),
    FeedBackScreen(
        label = "意见反馈",
        icon = Icons.Filled.AccountCircle
    ),
    AddFeedBackScreen(
        label = "新增意见反馈",
        icon = Icons.Filled.AccountCircle
    ),
    OpenSourceSoftwareStatementScreen(
        label = "开源软件声明",
        icon = Icons.Filled.AccountCircle
    ),
    GroupMemberCourseTableScreen(
        label = "成员课表",
        icon = Icons.Filled.AccountCircle
    ),
    CourTableSetting(
        label = "课表设置",
        icon = Icons.Filled.AccountCircle
    ),
    WidgetSettingScreen(
        label = "小组件设置",
        icon = Icons.Filled.AccountCircle
    ),
    AppSettingScreen(
        label = "软件设置",
        icon = Icons.Filled.AccountCircle
    ),
    AccountManagerScreen(
        label = "账号管理",
        icon = Icons.Filled.AccountCircle
    )
}