package com.example.coursetable_compose.viewmodel

import androidx.lifecycle.ViewModel
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.utils.SchoolDateUtils
import com.mypotota.datamodel.base.BaseStatus
import com.mypotota.datamodel.bean.GradesBean
import com.mypotota.datamodel.model.GradesNetWork
import kotlinx.coroutines.flow.MutableStateFlow

class GradesViewModel:ViewModel(), GradesNetWork.IOnGetGradesStatus {
    data class GradesSemester(
        var name:String,
        var key:String
    )
    private val gradesNetWork by lazy {
        GradesNetWork(this)
    }
    private val _gradesState= MutableStateFlow<BaseStatus<List<GradesBean.Item>>>(BaseStatus.LOADING())
    val gradesState=_gradesState
    val allSemesterList=loadSemesterList()
    val currentSemesterPosition = MutableStateFlow(0)

    init {
        val gradesList = SPUtils.getIns().getGradesList()
        if (gradesList.isEmpty()) {
            loadGrades(SchoolDateUtils.currentSchoolSemester())
        }else{
            _gradesState.value=BaseStatus.SUCCESS(gradesList)
        }
    }

    private fun loadGrades(key: String) {
        gradesNetWork.getLoginTicket(
            SPUtils.getIns().getAccount().toString(),
            SPUtils.getIns().getPassword().toString(),
            key
        )
    }


    fun changeCurrentSemester(index: Int) {
        currentSemesterPosition.value=index
        loadGrades(
            allSemesterList[index].key
        )
    }

    private fun loadSemesterList(): ArrayList<GradesSemester> {
        val semesters = ArrayList<GradesSemester>()
        var currentYear=SchoolDateUtils.currentYear
        for (i in 1 until 11) {
            val tempMonth=if(i % 2 == 0) 12 else 6
            val tempYear=if(i % 2 == 0){
                currentYear--
            }else{
                currentYear
            }
            val name=
                SchoolDateUtils.currentYear(tempYear) + "第${
                if (SchoolDateUtils.currentSemester(
                        tempMonth
                    ) == "1"
                ) "一" else "二"
            }学期"

            val key=
                "xnm=${tempYear-1}&xqm=${
                    if (SchoolDateUtils.currentSemester(
                            tempMonth
                        ) == "1"
                    ) "3" else "12"
                }&xqh_id=3"

            val semester = GradesSemester(
                name,key
            )
            semesters.add(semester)
        }
        return semesters
    }

    override fun onLoading(msg: String) {
        gradesState.value=BaseStatus.LOADING(msg)
    }

    override fun error(e: String) {
        gradesState.value=BaseStatus.ERROR(e)
    }

    override fun success(gradesBean: GradesBean) {
        if(allSemesterList[currentSemesterPosition.value].key==SchoolDateUtils.currentSchoolSemester()){
            SPUtils.getIns().setGradesList(gradesBean.items)
        }
        gradesState.value=BaseStatus.SUCCESS(gradesBean.items)
    }
}