package com.example.coursetable_compose.viewmodel

import android.app.Application
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.mypotota.datamodel.base.BaseStatus
import com.example.coursetable_compose.constant.REFRESH_DATE
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.utils.SchoolDateUtils
import com.mypotota.datamodel.bean.CourseTable
import com.mypotota.datamodel.model.CourseTableNetWork
import com.tapsdk.lc.LCObject
import com.tapsdk.lc.LCQuery
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AppControlsViewModel(private val application: Application) : AndroidViewModel(application),
    CourseTableNetWork.OnGetCourseTableStatus {
    private val _appControlsStatus = MutableStateFlow<BaseStatus<LCObject>>(BaseStatus.LOADING())
    val appControlsStatus = _appControlsStatus.asStateFlow()
    private val mainHandler = Handler(Looper.getMainLooper())
    private val courseTableNetWork by lazy {
        CourseTableNetWork(this)
    }

    init {
        viewModelScope.launch {
            getAppControls()
            autoRefresh()
        }
    }


    private fun autoRefresh() {
        val ins = SPUtils.getIns()
        if (!ins.getShowAutoRefresh()) {
            return
        }
        if (!ins.getAutoRefresh()) {
            return
        }
        //判断是否达到刷新间隔时间
        val currentTimeMillis = System.currentTimeMillis()
        if(SPUtils.getIns().getAutoRefreshDate()!=0L){
            if(currentTimeMillis<SPUtils.getIns().getAutoRefreshDate()+ REFRESH_DATE){
                return
            }
        }
        if (!ins.getAccount().isNullOrEmpty() && !ins.getPassword().isNullOrEmpty()) {
            //刷新课表
            courseTableNetWork.getLoginTicket(
                ins.getAccount().toString(),
                ins.getPassword().toString(),
                SchoolDateUtils.currentSchoolSemester()
            )
        } else {
            Toast.makeText(
                application,
                "请先登录再执行自动同步课表吧",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    suspend fun getAppControls() {
        withContext(Dispatchers.IO) {
            val query = LCQuery<LCObject>("AppControls");
            query.getInBackground("662124e43cfdad0f9257ce5b")
                .subscribe(object : Observer<LCObject> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onError(e: Throwable) {
                        _appControlsStatus.value = BaseStatus.ERROR(e.message.toString())
                    }

                    override fun onComplete() {
                    }

                    override fun onNext(t: LCObject) {
                        _appControlsStatus.value = BaseStatus.SUCCESS(t)
                    }
                })
        }
    }

    override fun onLoading(msg: String) {
    }

    override fun error(e: String) {
        mainHandler.post{
            Toast.makeText(application, e, Toast.LENGTH_SHORT).show()
        }
    }

    override fun success(courseTable: CourseTable) {
        mainHandler.post {
            Toast.makeText(application, "静默刷新课表成功", Toast.LENGTH_SHORT).show()
        }
        SPUtils.getIns().setCourseTable(courseTable)
        SPUtils.getIns().setAutoRefreshDate(System.currentTimeMillis())
    }
}