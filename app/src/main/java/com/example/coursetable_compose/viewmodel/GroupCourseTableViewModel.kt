package com.example.coursetable_compose.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mypotota.datamodel.base.BaseStatus
import com.example.coursetable_compose.utils.SPUtils
import com.tapsdk.lc.LCObject
import com.tapsdk.lc.LCQuery
import com.tapsdk.lc.LCUser
import com.tapsdk.lc.types.LCNull
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class GroupCourseTableViewModel : ViewModel() {
    private val _baseCourseTableStatus = MutableStateFlow<BaseStatus<LCObject>?>(null)
    val baseCourseTableStatus=_baseCourseTableStatus.asStateFlow()
    private val _userCreateGroupState= MutableStateFlow<BaseStatus<List<LCObject>>>(BaseStatus.LOADING())
    val userCreateGroupState=_userCreateGroupState.asStateFlow()
    //获取小组成员回调
    private val _groupMemberListStatus= MutableStateFlow<BaseStatus<List<LCObject>>>(BaseStatus.LOADING())
    val groupCourseTableListStatus=_groupMemberListStatus.asStateFlow()
    //获取当前用户加入小组列表回调
    private val _currentUserJoinGroupListStatus= MutableStateFlow<BaseStatus<List<LCObject>>>(
        BaseStatus.LOADING())
    val currentUserJoinGroupListStatus=_currentUserJoinGroupListStatus.asStateFlow()
    //解散时回调
    private val _dissolveGroupStatus= MutableStateFlow<BaseStatus<Boolean>?>(null)
    val dissolveGroupStatus=_dissolveGroupStatus.asStateFlow()
    //踢出时回调
    private val _kickOutStatus= MutableStateFlow<BaseStatus<String>?>(null)
    val kickOutStatus=_kickOutStatus.asStateFlow()
    //退出时回调
    private val _exitGroupStatus= MutableStateFlow<BaseStatus<String>?>(null)
    val exitGroupStatus=_exitGroupStatus.asStateFlow()
    //获取主页显示小组回调
    private val _homeShowGroupListStatus= MutableStateFlow<BaseStatus<List<LCObject>>>(BaseStatus.LOADING())
    val homeShowGroupListStatus=_homeShowGroupListStatus.asStateFlow()

     fun getCurrentUserJoinGroup() {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                val lcQuery = LCQuery<LCObject>("GroupCourseTableBind")
                lcQuery.whereEqualTo("user",LCUser.currentUser())
                lcQuery.findInBackground().subscribe(object:Observer<List<LCObject>>{
                    override fun onSubscribe(d: Disposable) {
                    }
                    override fun onError(e: Throwable) {
                        _currentUserJoinGroupListStatus.value= BaseStatus.ERROR("${e.message}")
                    }
                    override fun onComplete() {
                    }
                    override fun onNext(t: List<LCObject>) {
                        //拿到绑定的数据-获取小组列表
                        var objectIds=ArrayList<String>()
                        t.forEach {
                            objectIds.add(it.getLCObject<LCObject>("group").objectId)
                        }
                        getGroupFromBind(objectIds)
                    }
                })
            }
        }
    }

    private fun getGroupFromBind(t: List<String>) {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                _currentUserJoinGroupListStatus.value= BaseStatus.LOADING("正在获取小组列表")
                val lcQuery=LCQuery<LCObject>("GroupCourseTable")
                lcQuery.whereContainedIn(LCObject.KEY_OBJECT_ID,t)
                lcQuery.whereNotEqualTo("root",LCUser.currentUser())
                lcQuery.findInBackground().subscribe(object:Observer<List<LCObject>>{
                    override fun onSubscribe(d: Disposable) {
                    }
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        _currentUserJoinGroupListStatus.value= BaseStatus.ERROR("获取小组列表失败，因为：${e.message}")
                    }
                    override fun onComplete() {
                    }
                    override fun onNext(t: List<LCObject>) {
                        _currentUserJoinGroupListStatus.value= BaseStatus.SUCCESS(t)
                    }
                })

            }
        }
    }

    fun getCurrentCreateGroup() {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                _userCreateGroupState.value= BaseStatus.LOADING("正在获取创建的小组")
                val lcQuery = LCQuery<LCObject>("GroupCourseTable")
                lcQuery.whereEqualTo("root",LCUser.currentUser())
                lcQuery.findInBackground().subscribe(object:Observer<List<LCObject>>{
                    override fun onSubscribe(d: Disposable) {
                    }
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        _userCreateGroupState.value= BaseStatus.ERROR("获取用户创建小组失败，因为：${e.message}")
                    }
                    override fun onComplete() {
                    }
                    override fun onNext(t: List<LCObject>) {
                        println(t)
                        _userCreateGroupState.value= BaseStatus.SUCCESS(t)
                    }
                })
            }
        }
    }

    fun createGroup(name: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _baseCourseTableStatus.value = BaseStatus.LOADING("获取用户创建数量中...")
                //创建新的组并将当前用户加入组
                val query = LCQuery<LCObject>("GroupCourseTable")
                query.whereEqualTo("root", LCUser.currentUser())
                query.countInBackground().subscribe(object : Observer<Int> {
                    override fun onSubscribe(disposable: Disposable) {}
                    override fun onNext(count: Int) {
                        if(count>=LCUser.getCurrentUser().getInt("groupMax")){
                            _baseCourseTableStatus.value = BaseStatus.ERROR("由于服务器压力，每个用户只能创建${LCUser.getCurrentUser().getInt("groupMax")}个分组")
                            return
                        }
                        _baseCourseTableStatus.value = BaseStatus.LOADING("创建小组中")
                        val groupCourseTable = LCObject("GroupCourseTable")
                        groupCourseTable.put("root", LCUser.currentUser())
                        groupCourseTable.put("name", name)
                        groupCourseTable.saveInBackground().subscribe(object : Observer<LCObject> {
                            override fun onSubscribe(d: Disposable) {
                            }

                            override fun onError(e: Throwable) {
                                _baseCourseTableStatus.value = BaseStatus.ERROR(e.message.toString())
                            }

                            override fun onComplete() {
                            }

                            override fun onNext(t: LCObject) {
                                //创建小组，加入小组
                                joinGroup(t)
                            }
                        })
                    }
                    override fun onError(throwable: Throwable) {
                        _baseCourseTableStatus.value = BaseStatus.ERROR("获取用户创建数量失败，因为：${throwable.message}")
                    }
                    override fun onComplete() {}
                })
            }
        }
    }

    fun queryGroup(objectId: String?) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _baseCourseTableStatus.value = BaseStatus.LOADING("查找小组中")
                //查找小组是否存在
                val query = LCQuery<LCObject>("GroupCourseTable");
                query.getInBackground(objectId).subscribe(object : Observer<LCObject> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onError(e: Throwable) {
                        //查找小组失败，没有找到该小组，或网络错误
                        _baseCourseTableStatus.value =
                            BaseStatus.ERROR("加入小组失败，因为：${e.message}")
                    }

                    override fun onComplete() {
                    }

                    override fun onNext(t: LCObject) {
                        _baseCourseTableStatus.value =
                            BaseStatus.LOADING("小组查找成功，正在加入中...")
                        joinGroupIsJoin(t)
                    }
                })
            }
        }
    }

    private fun joinGroupIsJoin(groupCourseTable: LCObject) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _baseCourseTableStatus.value =
                    BaseStatus.LOADING("正在获取绑定数量")
                //创建新的组并将当前用户加入组
                val countquery = LCQuery<LCObject>("GroupCourseTableBind")
                countquery.whereEqualTo("group", groupCourseTable)
                countquery.countInBackground().subscribe(object : Observer<Int> {
                    override fun onSubscribe(disposable: Disposable) {}
                    override fun onNext(count: Int) {
                        if(count>=LCUser.getCurrentUser().getInt("groupBindMax")){
                            _baseCourseTableStatus.value = BaseStatus.ERROR("由于服务器压力，每个分组只能加入${LCUser.getCurrentUser().getInt("groupMax")}个成员")
                            return
                        }
                        _baseCourseTableStatus.value =
                            BaseStatus.LOADING("小组查找成功，获取小组列表中...")
                        //先查找是否已经加入该小组，没有则加入
                        val query = LCQuery<LCObject>("GroupCourseTableBind")
                        query.whereEqualTo("user", LCUser.currentUser())
                        query.whereEqualTo("account", SPUtils.getIns().getAccount())
                        query.whereEqualTo("password", SPUtils.getIns().getPassword())
                        query.whereEqualTo("group", groupCourseTable)
                        query.findInBackground().subscribe(object : Observer<List<LCObject>> {
                            override fun onSubscribe(d: Disposable) {
                            }

                            override fun onError(e: Throwable) {
                                _baseCourseTableStatus.value =
                                    BaseStatus.ERROR("加入小组失败，获取小组列表失败")
                            }

                            override fun onComplete() {
                            }

                            override fun onNext(t: List<LCObject>) {
                                if (t.isNotEmpty()) {
                                    _baseCourseTableStatus.value =
                                        BaseStatus.ERROR("小组加入失败，你已在该小组中")
                                } else {
                                    //加入小组
                                    joinGroup(groupCourseTable)
                                }
                            }
                        })
                    }
                    override fun onError(throwable: Throwable) {
                        _baseCourseTableStatus.value = BaseStatus.ERROR("获取绑定数量失败，因为：${throwable.message}")
                    }
                    override fun onComplete() {}
                })
            }
        }
    }

    private fun joinGroup(groupCourseTable: LCObject) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _baseCourseTableStatus.value =
                    BaseStatus.LOADING("小组列表获取成功，正在加入中...")
                val groupCourseTableBind = LCObject("GroupCourseTableBind")
                groupCourseTableBind.put("user", LCUser.currentUser())
                groupCourseTableBind.put("account", SPUtils.getIns().getAccount())
                groupCourseTableBind.put("password", SPUtils.getIns().getPassword())
                groupCourseTableBind.put("name",SPUtils.getIns().getCourseTable()!!.xsxx.XM)
                groupCourseTableBind.put("class",SPUtils.getIns().getCourseTable()!!.xsxx.BJMC)
                groupCourseTableBind.put("group", groupCourseTable)
                groupCourseTableBind.saveInBackground().subscribe(object : Observer<LCObject> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onError(e: Throwable) {
                        _baseCourseTableStatus.value =
                            BaseStatus.ERROR("小组加入失败，因为：${e.message}")
                    }

                    override fun onComplete() {
                    }

                    override fun onNext(t: LCObject) {
                        _baseCourseTableStatus.value = BaseStatus.SUCCESS(t)
                        refreshData()
                    }
                })
            }
        }
    }

    private fun refreshData() {
        getCurrentCreateGroup()
    }

    fun reset() {
        _baseCourseTableStatus.value=null
    }

    fun getGroupJoinList(lcObject: LCObject) {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
//                _groupMemberListStatus.value=BaseStatus.LOADING("正在努力加载小组成员列表")
                val query = LCQuery<LCObject>("GroupCourseTableBind")
                query.whereEqualTo("group",lcObject)
                query.whereNotEqualTo("user",LCUser.currentUser())
                query.findInBackground().subscribe(object:Observer<List<LCObject>>{
                    override fun onSubscribe(d: Disposable) {
                    }
                    override fun onError(e: Throwable) {
                        _groupMemberListStatus.value= BaseStatus.ERROR("加载小组成员失败，原因：${e.message}")
                    }
                    override fun onComplete() {
                    }
                    override fun onNext(t: List<LCObject>) {
                        _groupMemberListStatus.value= BaseStatus.SUCCESS(t)
                    }
                })
            }
        }
    }

    fun dissolveGroup(lcObject: LCObject) {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                _dissolveGroupStatus.value= BaseStatus.LOADING("正在查找小组成员")
                val query = LCQuery<LCObject>("GroupCourseTableBind")
                query.whereEqualTo("group",lcObject)
                query.findInBackground().subscribe(object:Observer<List<LCObject>>{
                    override fun onSubscribe(d: Disposable) {
                    }
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        _dissolveGroupStatus.value= BaseStatus.ERROR("小组成员查找失败，因为：${e.message}")
                    }
                    override fun onComplete() {
                    }
                    override fun onNext(t: List<LCObject>) {
                        _dissolveGroupStatus.value= BaseStatus.LOADING("正在开始清除小组成员")
                        LCObject.deleteAllInBackground(t).subscribe(object:Observer<LCNull>{
                            override fun onSubscribe(d: Disposable) {
                            }
                            override fun onError(e: Throwable) {
                                e.printStackTrace()
                                _dissolveGroupStatus.value= BaseStatus.ERROR("小组成员删除失败，无法删除小组，因为：${e.message}")
                            }
                            override fun onComplete() {
                            }
                            override fun onNext(t: LCNull) {
                                _dissolveGroupStatus.value= BaseStatus.LOADING("小组成员删除成功，正在删除小组")
                                deleteGroup(lcObject)
                            }
                        })
                    }
                })
            }
        }
    }

    private fun deleteGroup(lcObject: LCObject) {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                _dissolveGroupStatus.value= BaseStatus.LOADING("正在删除小组")
                val deleteGroup = LCObject.createWithoutData("GroupCourseTable", lcObject.objectId);
                deleteGroup.deleteInBackground().subscribe(object : Observer<LCNull?> {
                    override fun onSubscribe(d: Disposable) {
                    }
                    override fun onError(e: Throwable) {
                        _dissolveGroupStatus.value= BaseStatus.ERROR("小组删除失败，因为：${e.message}")
                    }
                    override fun onComplete() {
                    }
                    override fun onNext(t: LCNull) {
                        _dissolveGroupStatus.value= BaseStatus.SUCCESS(true)
                    }
                })
            }
        }
    }

    fun cancelStatus() {
        _dissolveGroupStatus.value=null
        _kickOutStatus.value=null
    }

    fun kickOut(objectId: String?) {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                _kickOutStatus.value= BaseStatus.LOADING("正在踢出该成员")
                val deleteGroup = LCObject.createWithoutData("GroupCourseTableBind", objectId);
                deleteGroup.deleteInBackground().subscribe(object : Observer<LCNull?> {
                    override fun onSubscribe(d: Disposable) {
                    }
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        _kickOutStatus.value= BaseStatus.ERROR("成员踢出失败，因为：${e.message}")
                    }
                    override fun onComplete() {
                    }
                    override fun onNext(t: LCNull) {
                        _kickOutStatus.value= BaseStatus.SUCCESS("成功踢出")
                    }
                })
            }
        }
    }

    fun exitGroup(lcObject: LCObject) {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                _exitGroupStatus.value= BaseStatus.LOADING("正在退出小组")
                val query = LCQuery<LCObject>("GroupCourseTableBind")
                query.whereEqualTo("group",lcObject)
                query.whereEqualTo("user",LCUser.currentUser())
                query.findInBackground().subscribe(object:Observer<List<LCObject>>{
                    override fun onSubscribe(d: Disposable) {
                    }
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        _exitGroupStatus.value= BaseStatus.ERROR("退出小组失败，因为：${e.message}")
                    }
                    override fun onComplete() {
                    }
                    override fun onNext(t: List<LCObject>) {
                        _exitGroupStatus.value= BaseStatus.LOADING("退出小组中")
                        LCObject.deleteAllInBackground(t).subscribe(object:Observer<LCNull>{
                            override fun onSubscribe(d: Disposable) {
                            }
                            override fun onError(e: Throwable) {
                                e.printStackTrace()
                                _exitGroupStatus.value= BaseStatus.ERROR("退出小组失败，无法退出小组，因为：${e.message}")
                            }
                            override fun onComplete() {
                            }
                            override fun onNext(t: LCNull) {
                                _exitGroupStatus.value= BaseStatus.SUCCESS("退出小组成功")
                            }
                        })
                    }
                })



//                deleteGroup.deleteInBackground().subscribe(object : Observer<LCNull?> {
//                    override fun onSubscribe(d: Disposable) {
//                    }
//                    override fun onError(e: Throwable) {
//                        e.printStackTrace()
//                        _exitGroupStatus.value=BaseStatus.ERROR("退出小组失败，因为：${e.message}")
//                    }
//                    override fun onComplete() {
//                    }
//                    override fun onNext(t: LCNull) {
//                        _exitGroupStatus.value=BaseStatus.SUCCESS("成功退出")
//                    }
//                })
            }
        }
    }

    fun getHomeGroup() {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                _homeShowGroupListStatus.value= BaseStatus.LOADING("正在获取主页显示小组")
                val homeShowGroupList = SPUtils.getIns().getHomeShowGroupList()
                val lcQuery=LCQuery<LCObject>("GroupCourseTable")
                lcQuery.whereContainedIn(LCObject.KEY_OBJECT_ID,homeShowGroupList)
                lcQuery.findInBackground().subscribe(object:Observer<List<LCObject>>{
                    override fun onSubscribe(d: Disposable) {
                    }
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        _homeShowGroupListStatus.value= BaseStatus.ERROR("主页小组获取失败，因为：${e.message}")
                    }
                    override fun onComplete() {
                    }
                    override fun onNext(t: List<LCObject>) {
                        _homeShowGroupListStatus.value= BaseStatus.SUCCESS(t)
                    }
                })
            }
        }
    }
}