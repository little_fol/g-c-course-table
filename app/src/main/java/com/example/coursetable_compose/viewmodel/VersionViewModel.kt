package com.example.coursetable_compose.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.coursetable_compose.application.CourseTableApplication
import com.mypotota.datamodel.base.BaseStatus
import com.example.coursetable_compose.utils.APKVersionCodeUtils
import com.tapsdk.lc.LCObject
import com.tapsdk.lc.LCQuery
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class VersionViewModel : ViewModel() {
    private var _allVersionStatus =
        MutableStateFlow<BaseStatus<List<LCObject>>>(BaseStatus.LOADING())
    val allVersion = _allVersionStatus.asStateFlow()
    private var _checkUpdateStatus = MutableStateFlow<BaseStatus<List<LCObject>>?>(null)
    val checkUpdateStatus = _checkUpdateStatus.asStateFlow()
    //当前版本
    private var _checkCurrentVersionStatus = MutableStateFlow<BaseStatus<LCObject>?>(null)
    val checkCurrentVersionStatus = _checkCurrentVersionStatus.asStateFlow()


    init {
        checkUpdate(APKVersionCodeUtils.getVersionNumber(CourseTableApplication.context))
        checkCurrentVersion(APKVersionCodeUtils.getVersionNumber(CourseTableApplication.context))
    }

    private fun checkCurrentVersion(versionNumber: Int) {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                val query = LCQuery<LCObject>("VersionManage")
                query.whereEqualTo("versionNumber",versionNumber)
                query.findInBackground().subscribe(object : Observer<List<LCObject>> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onError(e: Throwable) {
                        _checkCurrentVersionStatus.value = BaseStatus.ERROR(e.message.toString())
                    }

                    override fun onComplete() {
                    }

                    override fun onNext(t: List<LCObject>) {
                        if(t.isEmpty()){
                            _checkCurrentVersionStatus.value = BaseStatus.ERROR("未知版本")
                        }else{
                            _checkCurrentVersionStatus.value = BaseStatus.SUCCESS(t[0])
                        }
                    }
                })
            }
        }
    }

    fun getAllVersion() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val query = LCQuery<LCObject>("VersionManage")
                query.orderByDescending(LCObject.KEY_CREATED_AT)
                query.findInBackground().subscribe(object : Observer<List<LCObject>> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onError(e: Throwable) {
                        _allVersionStatus.value = BaseStatus.ERROR(e.message.toString())
                    }

                    override fun onComplete() {
                    }

                    override fun onNext(t: List<LCObject>) {
                        _allVersionStatus.value = BaseStatus.SUCCESS(t)
                    }
                })
            }
        }
    }

    fun checkUpdate(currentVersionCode: Int) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _checkUpdateStatus.value= BaseStatus.LOADING()
                val query = LCQuery<LCObject>("VersionManage")
                query.whereGreaterThan("versionNumber", currentVersionCode)
                query.orderByDescending(LCObject.KEY_CREATED_AT)
                query.limit=1
                query.findInBackground().subscribe(object : Observer<List<LCObject>> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        _checkUpdateStatus.value = BaseStatus.ERROR(e.message.toString())
                    }

                    override fun onComplete() {
                    }

                    override fun onNext(t: List<LCObject>) {
                        _checkUpdateStatus.value= BaseStatus.SUCCESS(t)
                    }
                })
            }
        }
    }

}