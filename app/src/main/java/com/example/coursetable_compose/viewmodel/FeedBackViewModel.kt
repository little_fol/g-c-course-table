package com.example.coursetable_compose.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.baidu.mobstat.StatService
import com.example.coursetable_compose.application.CourseTableApplication
import com.mypotota.datamodel.base.BaseStatus
import com.tapsdk.lc.LCObject
import com.tapsdk.lc.LCQuery
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FeedBackViewModel:ViewModel() {
    private val _feedBackByDevicesStatus= MutableStateFlow<BaseStatus<List<LCObject>>>(BaseStatus.LOADING())
    private val _submitFeedBackStatus= MutableStateFlow<BaseStatus<LCObject>?>(null)
    val feedBackByDevicesStatus = _feedBackByDevicesStatus.asStateFlow()
    val submitFeedBackStatus = _submitFeedBackStatus.asStateFlow()

    init {
        getFeedBacks()
    }

    suspend fun getFeedBackBYDevices(devicesKey:String){
        withContext(Dispatchers.IO){
            val lcQuery = LCQuery<LCObject>("FeedBack")
            lcQuery.whereEqualTo("devicesKey",devicesKey)
            lcQuery.findInBackground().subscribe (object: Observer<List<LCObject>> {
                override fun onSubscribe(d: Disposable) {
                }
                override fun onError(e: Throwable) {
                    _feedBackByDevicesStatus.value= BaseStatus.ERROR(e.message.toString())
                }
                override fun onComplete() {
                }
                override fun onNext(t: List<LCObject>) {
                    _feedBackByDevicesStatus.value= BaseStatus.SUCCESS(t)
                }
            })
        }
    }

    fun submitFeedBack(title: String,content: String){
        viewModelScope.launch {
            val testDeviceId = StatService.getTestDeviceId(CourseTableApplication.context)
            submitFeedBack(testDeviceId,title,content)
        }
    }

    suspend fun submitFeedBack(devicesKey: String, title: String, content: String) {
        withContext(Dispatchers.IO){
            _submitFeedBackStatus.value= BaseStatus.LOADING()
            val feedBack = LCObject("FeedBack")
            feedBack.put("devicesKey", devicesKey)
            feedBack.put("title", title)
            feedBack.put("content", content)
            feedBack.saveInBackground().subscribe(object: Observer<LCObject> {
                override fun onSubscribe(d: Disposable) {
                }
                override fun onError(e: Throwable) {
                    _submitFeedBackStatus.value= BaseStatus.ERROR(e.message.toString())
                }
                override fun onComplete() {
                }
                override fun onNext(t: LCObject) {
                    _submitFeedBackStatus.value= BaseStatus.SUCCESS(t)
                }
            })
        }
    }

    fun getFeedBacks() {
        viewModelScope.launch {
            val testDeviceId = StatService.getTestDeviceId(CourseTableApplication.context)
            getFeedBackBYDevices(testDeviceId)
        }
    }
}