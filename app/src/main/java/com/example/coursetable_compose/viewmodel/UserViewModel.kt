package com.example.coursetable_compose.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mypotota.datamodel.base.BaseStatus
import com.orhanobut.logger.Logger
import com.tapsdk.lc.LCUser
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class UserViewModel : ViewModel() {
    private val _loginStatus = MutableStateFlow<BaseStatus<Boolean>?>(null)
    val loginStatus = _loginStatus.asStateFlow()

    private val _updateUserInfoStatus= MutableStateFlow<BaseStatus<String>>(BaseStatus.LOADING())
    val updateUserInfoStatus=_updateUserInfoStatus.asStateFlow()


    fun loginAndRegister(account: String, password: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _loginStatus.value = BaseStatus.LOADING("登录中...")
                LCUser.logIn(account, password).subscribe(object : Observer<LCUser?> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onError(e: Throwable) {
                        if (e.message.toString().contains("Could not find user.")) {
                            //注册
                            _loginStatus.value = BaseStatus.LOADING("未找到账户，正在注册....")
                            register(account, password)
                        } else {
                            //报错
                            _loginStatus.value = BaseStatus.ERROR("登录失败，原因：${e.message}")
                        }
                    }

                    override fun onComplete() {
                    }

                    override fun onNext(t: LCUser) {
                        _loginStatus.value = BaseStatus.SUCCESS(true)
                    }
                })
            }
        }
    }


    private fun register(account: String, password: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val user = LCUser()
                user.username = account
                user.password = password
                user.signUpInBackground().subscribe(object : Observer<LCUser> {
                    override fun onSubscribe(disposable: Disposable) {}
                    override fun onNext(user: LCUser) {
                        // 注册成功
                        _loginStatus.value = BaseStatus.LOADING("注册成功")
                        loginAndRegister(account,password)
                    }

                    override fun onError(throwable: Throwable) {
                        // 注册失败（通常是因为用户名已被使用）
                        _loginStatus.value = BaseStatus.ERROR("注册失败，原因：${throwable.message}")
                    }
                    override fun onComplete() {}
                })
            }
        }
    }

    fun updateUserInfo() {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                val userQuery = LCUser.getQuery()
                userQuery.getInBackground(LCUser.currentUser().objectId).subscribe(object:Observer<LCUser>{
                    override fun onSubscribe(d: Disposable) {
                    }
                    override fun onError(e: Throwable) {
                        if(e.message!!.contains("Could not find user.")){
                            _updateUserInfoStatus.value= BaseStatus.ERROR("您已被下线，请重新登录")
                            LCUser.logOut()
                        }
                        Logger.e("用户更新失败，$e")
//                        _updateUserInfoStatus.value=
//                        LCUser.logOut()
                    }
                    override fun onComplete() {
                    }
                    override fun onNext(t: LCUser) {
                        val currentUser = LCUser.getCurrentUser()
                        currentUser.put("groupMax",t.getInt("groupMax"))
                        currentUser.put("groupBindMax",t.getInt("groupBindMax"))
                    }
                })

            }
        }
    }

}