package com.example.coursetable_compose.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mypotota.datamodel.base.BaseStatus
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.utils.Utils
import com.mypotota.datamodel.bean.Kb
import com.mypotota.datamodel.bean.Sjk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.threeten.extra.YearWeek
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.Year
import java.time.chrono.Chronology
import java.time.chrono.IsoChronology
import java.time.format.DateTimeFormatter
import java.time.temporal.IsoFields


class CourseTableViewModel : ViewModel() {
    //tab 栏
    private val _weekCount: MutableStateFlow<Int> = MutableStateFlow(0)
    val weekCount = _weekCount.asStateFlow()

    //标题栏
    private val _weekMonthMapLiveData = MutableStateFlow<Map<String, String>?>(null)
    val weekMonthMap = _weekMonthMapLiveData.asStateFlow()

    //时间格式化
    private val simpleDateFormat1 = DateTimeFormatter.ofPattern("yyyy-MM-dd")

    //获取今天时间
    val currentDate =
        LocalDate.now().monthValue.toString() + "-" + LocalDate.now().dayOfMonth.toString()

    //侧栏
    private val _sideBarDataLiveData = MutableStateFlow<List<String>?>(null)
    val sideBarList = _sideBarDataLiveData.asStateFlow()

    //其他课程
    private val _sjkList = MutableStateFlow<List<Sjk>?>(null)

    //课表
    private val _courseTableList = MutableStateFlow<List<Kb>?>(null)
    val courseTableList = _courseTableList.asStateFlow()

    //tab layout 位置
    private val _selectCurrentWeek = MutableStateFlow<Int>(0)
    val selectCurrentWeek = _selectCurrentWeek.asStateFlow()

    //课表配色
    private val _colorMapByCourse = MutableStateFlow<Map<String, Long>>(HashMap())
    val colorMapByCourse = _colorMapByCourse.asStateFlow()

    //获取所有配色
    private val _colorList = MutableStateFlow<List<Long>>(ArrayList())
    val colorList = _colorList.asStateFlow()

    //今天课程
    private val _todayCourseTable = MutableStateFlow<BaseStatus<List<Kb>>>(BaseStatus.LOADING())
    val todayCourseTable = _todayCourseTable.asStateFlow()

    //明天课程
    private val _tomorrowCourseTable = MutableStateFlow<BaseStatus<List<Kb>>>(BaseStatus.LOADING())
    val tomorrowCourseTable = _tomorrowCourseTable.asStateFlow()

    val sjkList = _sjkList.asStateFlow()
    val weekCountDay = 7

    private val spUtils = SPUtils.getIns()

    init {
        viewModelScope.launch {
            spUtils.getCourseTable() ?: return@launch
            selectWeek(
                Utils.currentWeekNumber(
                    SPUtils.getIns().getStartFirstWeekDate().toString()
                ) - 1
            )
            getCountWeekNumber()
            getSideBarData()
            getOtherCourseList()
        }
    }

    /**
     * 获取今天的课表
     */
    fun getToDayCourseTable() {
        //获取今日是第几周、然后周几
        viewModelScope.launch {
            val today = LocalDate.now() // 获取当前日期
            val dayOfWeek = today.dayOfWeek // 获取今天是周几
            val toDayCourseTable = getCurrentWeekCourseTableByDay(dayOfWeek.value)
            _todayCourseTable.value = BaseStatus.SUCCESS(toDayCourseTable)
        }
    }

    /**
     * 获取明天的课表
     */

    fun getTomorrowCourseTable() {
        viewModelScope.launch {
            val today = LocalDate.now() // 获取当前日期
            val dayOfWeek = today.dayOfWeek // 获取今天是周几
            val tomorrow = getCurrentWeekCourseTableByDay(dayOfWeek.value + 1)
            _tomorrowCourseTable.value = BaseStatus.SUCCESS(tomorrow)
        }
    }


    private fun getCurrentWeekCourseTableByDay(dayOfWeek: Int): ArrayList<Kb> {
        //解析指定周次的课程
        val currentWeekNumber =
            Utils.currentWeekNumber(SPUtils.getIns().getStartFirstWeekDate().toString())

        val currentWeekCourseTable: ArrayList<Kb>
        val weekStr: String
        when (dayOfWeek) {
            1 -> {
                weekStr = "星期一"
                currentWeekCourseTable = getCurrentCourseTable(currentWeekNumber)
            }

            2 -> {
                weekStr = "星期二"
                currentWeekCourseTable = getCurrentCourseTable(currentWeekNumber)
            }

            3 -> {
                weekStr = "星期三"
                currentWeekCourseTable = getCurrentCourseTable(currentWeekNumber)
            }

            4 -> {
                weekStr = "星期四"
                currentWeekCourseTable = getCurrentCourseTable(currentWeekNumber)
            }

            5 -> {
                weekStr = "星期五"
                currentWeekCourseTable = getCurrentCourseTable(currentWeekNumber)
            }

            6 -> {
                weekStr = "星期六"
                currentWeekCourseTable = getCurrentCourseTable(currentWeekNumber)
            }

            7 -> {
                weekStr = "星期日"
                currentWeekCourseTable = getCurrentCourseTable(currentWeekNumber)
            }

            else -> {
                weekStr = "星期一"
                currentWeekCourseTable = getCurrentCourseTable(currentWeekNumber + 1)
            }
        }


        //获取今日的课表
        val toDayCourseTableList = ArrayList<Kb>()
        currentWeekCourseTable.forEach {
            if (it.xqjmc.equals(weekStr)) {
                toDayCourseTableList.add(it)
            }
        }
        return toDayCourseTableList
    }


    /**
     * 按照周次获取课表
     */
    private fun getWeekCourse(weekNumber: Int) {
        viewModelScope.launch {
            _courseTableList.value = getCurrentCourseTable(weekNumber)
        }
    }

    private fun getCurrentCourseTable(weekNumber: Int): ArrayList<Kb> {
        val courseTable = spUtils.getCourseTable()
        //解析指定周次的课程
        val kbList = courseTable!!.kbList

        val courseList = ArrayList<Kb>()

        val colorMap = spUtils.getColorMapByCourse()
        kbList.forEach { item ->
            if (colorMap["${item.kcmc}-${item.xm}"] == null) {
                colorMap["${item.kcmc}-${item.xm}"] = spUtils.getColorList().random()
            }
            //解析周次
            val zcd = item.zcd.replace("周", "")
            val zcds = zcd.split(",")
            //计算每节课是否包含在数据中 -- [4-8,13-20]
            zcds.forEach {
                if (it.indexOf("双") != -1) {
                    val week = it.replace("(双)", "")
                    val weekSp = week.split("-")
                    val newCourseWeek = weekSp[0].toInt()..weekSp[weekSp.size - 1].toInt()
                    if ((weekNumber % 2 == 0) && newCourseWeek.contains(weekNumber)) {
                        courseList.add(item)
                    }
                } else if (it.indexOf("单") != -1) {
                    val week = it.replace("(单)", "")
                    val weekSp = week.split("-")
                    val newCourseWeek = weekSp[0].toInt()..weekSp[weekSp.size - 1].toInt()
                    if ((weekNumber % 2 != 0) && newCourseWeek.contains(weekNumber)) {
                        courseList.add(item)
                    }
                } else {
                    if (it.indexOf("-") != -1) {
                        val newCourseWeek =
                            it.split("-")[0].toInt()..it.split("-")[it.split("-").size - 1].toInt()
                        if (newCourseWeek.contains(weekNumber)) {
                            courseList.add(item)
                        }
                    } else {
                        val courseWeek = it.toInt()
                        if (courseWeek == weekNumber) {
                            courseList.add(item)
                        }
                    }
                }
            }
        }
        spUtils.setColorMapByCourse(colorMap)
        return courseList
    }


    private suspend fun getSideBarData() {
        withContext(Dispatchers.IO) {
            val dayCourseNumber = spUtils.getDayCourseNumber()
            _sideBarDataLiveData.value = dayCourseNumber ?: ArrayList()
        }
    }

    private suspend fun getOtherCourseList() {
        withContext(Dispatchers.IO) {
            val courseTable = spUtils.getCourseTable()
            val sjkList = courseTable!!.sjkList
            _sjkList.value = sjkList
        }
    }

    suspend fun getTopViewData(weekNumber: Int) {
        withContext(Dispatchers.IO) {
            val map = HashMap<String, String>()
            //初始化日期
            var dateToCheck =
                LocalDate.parse(spUtils.getStartFirstWeekDate().toString(), simpleDateFormat1)
            //
            if (IsoChronology.INSTANCE != Chronology.from(dateToCheck)) {
                dateToCheck = LocalDate.from(dateToCheck)
            }
            // need to use getLong() as JDK Parsed class get() doesn't work properly
            // need to use getLong() as JDK Parsed class get() doesn't work properly
            val year = Math.toIntExact(dateToCheck.getLong(IsoFields.WEEK_BASED_YEAR))
            val week = Math.toIntExact(dateToCheck.getLong(IsoFields.WEEK_OF_WEEK_BASED_YEAR))

            val currentWeek = YearWeek.of(year,week).week + weekNumber
            val yearWeek = YearWeek.of(LocalDate.now().year, currentWeek - 1)


            val monday = atDay(DayOfWeek.MONDAY, yearWeek.year, yearWeek.week)
            map["monday"] = monday.monthValue.toString() + "-" + monday.dayOfMonth.toString()

            val tuesday = atDay(DayOfWeek.TUESDAY, yearWeek.year, yearWeek.week)
            map["tuesday"] = tuesday.monthValue.toString() + "-" + tuesday.dayOfMonth.toString()

            val wednesday = atDay(DayOfWeek.WEDNESDAY, yearWeek.year, yearWeek.week)
            map["wednesday"] =
                wednesday.monthValue.toString() + "-" + wednesday.dayOfMonth.toString()

            val thursday = atDay(DayOfWeek.THURSDAY, yearWeek.year, yearWeek.week)
            map["thursday"] = thursday.monthValue.toString() + "-" + thursday.dayOfMonth.toString()

            val friday = atDay(DayOfWeek.FRIDAY, yearWeek.year, yearWeek.week)
            map["friday"] = friday.monthValue.toString() + "-" + friday.dayOfMonth.toString()

            val saturday = atDay(DayOfWeek.SATURDAY, yearWeek.year, yearWeek.week)
            map["saturday"] = saturday.monthValue.toString() + "-" + saturday.dayOfMonth.toString()

            val sunday = atDay(DayOfWeek.SUNDAY, yearWeek.year, yearWeek.week)
            map["sunday"] = sunday.monthValue.toString() + "-" + sunday.dayOfMonth.toString()

            map["monthValue"] = monday.monthValue.toString() + "月"
            _weekMonthMapLiveData.value = map
        }
    }


    fun atDay(dayOfWeek: DayOfWeek, year: Int, week: Int): LocalDate {
        val correction = LocalDate.of(year, 1, 4).getDayOfWeek().value + 3
        val dayOfYear: Int = week * 7 + dayOfWeek.value - correction
        val maxDaysOfYear = if (Year.isLeap(year.toLong())) 366 else 365
        if (dayOfYear > maxDaysOfYear) {
            return LocalDate.ofYearDay(year + 1, dayOfYear - maxDaysOfYear)
        }
        return if (dayOfYear > 0) {
            LocalDate.ofYearDay(year, dayOfYear)
        } else {
            val daysOfPreviousYear = if (Year.isLeap((year - 1).toLong())) 366 else 365
            LocalDate.ofYearDay(year - 1, daysOfPreviousYear + dayOfYear)
        }
    }

    fun getCountWeekNumber() {
        _weekCount.value = spUtils.getCountWeekNumber()
    }

    fun selectWeek(index: Int) {
        viewModelScope.launch {
            _selectCurrentWeek.value = index
            getTopViewData(index + 1)
            getWeekCourse(index + 1)
        }
    }

    fun selectToday() {
        selectWeek(Utils.currentWeekNumber(SPUtils.getIns().getStartFirstWeekDate().toString()) - 1)
    }



    /**
     * 获取当前课表配色
     */
    fun getCourseTableColor() {
        viewModelScope.launch {
            delay(200)
            val colorMapByCourse = spUtils.getColorMapByCourse()
            println("getCourseTableColor")
            _colorMapByCourse.value = HashMap(colorMapByCourse)
        }
    }

    /**
     * 获取当前课表配色
     */
    fun getColorList() {
        viewModelScope.launch {
            val colorList = spUtils.getColorList()
            _colorList.value = ArrayList(colorList)
        }
    }

}