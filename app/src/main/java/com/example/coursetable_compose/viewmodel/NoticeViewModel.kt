package com.example.coursetable_compose.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mypotota.datamodel.base.BaseStatus
import com.tapsdk.lc.LCObject
import com.tapsdk.lc.LCQuery
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class NoticeViewModel : ViewModel() {
    private val _newNoticeModel = MutableStateFlow<BaseStatus<LCObject>>(BaseStatus.LOADING())
    private val _allNoticeStatus =
        MutableStateFlow<BaseStatus<List<LCObject>>>(BaseStatus.LOADING())
    val newNotice = _newNoticeModel.asStateFlow()
    val allNoticeStatus = _allNoticeStatus.asStateFlow()


    init {
        viewModelScope.launch {
            getNewNoticeModel()
        }
    }

    suspend fun getNewNoticeModel() {
        withContext(Dispatchers.IO) {
            val lcQuery = LCQuery<LCObject>("Notice")
            lcQuery.orderByDescending(LCObject.KEY_CREATED_AT)
            lcQuery.limit(1)
            lcQuery.findInBackground().subscribe(object : Observer<List<LCObject>> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    _newNoticeModel.value = BaseStatus.ERROR(e.message.toString())
                }

                override fun onComplete() {
                }

                override fun onNext(t: List<LCObject>) {
                    val lcObject = t.get(0)
                    _newNoticeModel.value = BaseStatus.SUCCESS(lcObject)
                }
            })
        }
    }

    fun getAllNotices() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val lcQuery = LCQuery<LCObject>("Notice")
                lcQuery.orderByDescending(LCObject.KEY_CREATED_AT)
                lcQuery.findInBackground().subscribe(object : Observer<List<LCObject>> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onError(e: Throwable) {
                        _allNoticeStatus.value = BaseStatus.ERROR(e.message.toString())
                    }

                    override fun onComplete() {
                    }

                    override fun onNext(t: List<LCObject>) {
                        _allNoticeStatus.value = BaseStatus.SUCCESS(t)
                    }
                })
            }
        }
    }
}