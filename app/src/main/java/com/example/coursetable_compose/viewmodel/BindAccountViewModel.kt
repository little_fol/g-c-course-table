package com.example.coursetable_compose.viewmodel

import androidx.lifecycle.ViewModel
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.utils.SchoolDateUtils
import com.mypotota.datamodel.base.BaseStatus
import com.mypotota.datamodel.bean.CourseTable
import com.mypotota.datamodel.model.CourseTableNetWork
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class BindAccountViewModel : ViewModel(), CourseTableNetWork.OnGetCourseTableStatus {
    private val courseTableNetWork by lazy {
        CourseTableNetWork(this)
    }
    private val _getCourseTAbleStatus = MutableStateFlow<BaseStatus<CourseTable>?>(null)
    val courseTableStatus = _getCourseTAbleStatus.asStateFlow()

    /**
     * 获取登录验证码
     */
    @OptIn(DelicateCoroutinesApi::class)
    fun getLoginTicket(account: String, password: String) {
        courseTableNetWork.getLoginTicket(account,password, SchoolDateUtils.currentSchoolSemester())
    }

    override fun onLoading(msg: String) {
        _getCourseTAbleStatus.value= BaseStatus.LOADING(msg)
    }

    override fun error(e: String) {
        _getCourseTAbleStatus.value= BaseStatus.ERROR(e)
    }

    override fun success(courseTable: CourseTable) {
        _getCourseTAbleStatus.value= BaseStatus.SUCCESS(courseTable)
    }

    fun cancelBind() {
        _getCourseTAbleStatus.value=null
    }
}