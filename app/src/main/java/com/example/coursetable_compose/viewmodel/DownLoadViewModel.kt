package com.example.coursetable_compose.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.coursetable_compose.application.CourseTableApplication
import com.mypotota.datamodel.base.BaseStatus
import com.tapsdk.lc.LCObject
import com.xuexiang.xupdate.XUpdate
import com.xuexiang.xupdate.service.OnFileDownloadListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File


class DownLoadViewModel(private val application:Application) : AndroidViewModel(application) {
    private val _downloadStatus = MutableStateFlow<BaseStatus<File>>(BaseStatus.LOADING())
    val downloadStatus = _downloadStatus.asStateFlow()


    fun startDownLoad(lcObject: LCObject) {
        //获取下载的路径
        val downLoadFile = CourseTableApplication.context.getExternalFilesDir("")
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _downloadStatus.value= BaseStatus.LOADING()
                XUpdate.newBuild(application)
                    .apkCacheDir(downLoadFile!!.path) //设置下载缓存的根目录
                    .build()
                    .download(lcObject.getString("url"), object : OnFileDownloadListener {
                        //设置下载的地址和下载的监听
                        override fun onStart() {
                            _downloadStatus.value= BaseStatus.LOADING("开始下载",0f)
                        }

                        override fun onProgress(progress: Float, total: Long) {
                            _downloadStatus.value= BaseStatus.LOADING("下载中",progress)
                        }

                        override fun onCompleted(file: File): Boolean {
                            _downloadStatus.value= BaseStatus.SUCCESS(file)
                            return false
                        }

                        override fun onError(throwable: Throwable) {
                            _downloadStatus.value= BaseStatus.ERROR("下载失败：${throwable.message.toString()}")
                        }
                    })
            }
        }
    }


}