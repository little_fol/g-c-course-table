package com.example.coursetable_compose.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mypotota.datamodel.api.ApiService
import com.mypotota.datamodel.base.BaseStatus
import com.example.coursetable_compose.utils.SPUtils
import com.example.coursetable_compose.utils.Utils
import com.google.gson.Gson
import com.mypotota.datamodel.bean.EnergyChargeBean
import com.mypotota.datamodel.bean.EnergyChargeBeanSubList
import com.orhanobut.logger.Logger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.Request
import org.jsoup.Connection
import org.jsoup.Jsoup

class EnergyChargeViewModel: ViewModel() {
    private val _getRoomEnergyChargeStatus=MutableStateFlow<BaseStatus<EnergyChargeBeanSubList>>(
        BaseStatus.LOADING())
    val getRoomEnergyChargeStatus=_getRoomEnergyChargeStatus.asStateFlow()

    fun getRoomEnergyCharge(type:Int=0) {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                val roomNumber = SPUtils.getIns().getRoomNumber()
                if(roomNumber==null){
                    _getRoomEnergyChargeStatus.value= BaseStatus.ERROR("未绑定房间号，无法获取电费情况")
                    return@withContext
                }
                if(type==0){
                    if(SPUtils.getIns().getRoomEnergyCharge().isNullOrEmpty()){
                        _getRoomEnergyChargeStatus.value= BaseStatus.ERROR("获取本地房间电费缓存失败，请尝试刷新电费")
                    }else{
                        _getRoomEnergyChargeStatus.value= BaseStatus.SUCCESS(SPUtils.getIns().getRoomEnergyCharge()!!)
                    }
                }
                _getRoomEnergyChargeStatus.value= BaseStatus.LOADING("正在刷新")

                val url="https://dfcx.cqvie.edu.cn/weixin/ashx/frmuser.ashx?test=lastlist&pid=$roomNumber"

                try {
                    val response =
                        Jsoup.connect(url)
                        .timeout(20000)
                        .method(Connection.Method.GET)
                        .execute()
                    val html = response.parse().html().replace(" ", "").replace("\n", "")
                    val data = html.substring(html.indexOf("vardata={"), html.indexOf("};") + 1)
                    val cookie = data.substring(data.indexOf("'cookie':"), data.indexOf("\","))
                        .replace("'cookie':\"", "")
                    if(!cookie.isNullOrEmpty()){
                        val text = Jsoup.connect(url)
                            .method(Connection.Method.GET)
                            .header("cookie", cookie)
                            .execute()
                            .parse()
                            .body().text()
                        val chargeBean = Gson().fromJson(text, EnergyChargeBean::class.java)
                        _getRoomEnergyChargeStatus.value=BaseStatus.SUCCESS(chargeBean[0])
                    }





                }catch (e:Exception){
                    e.printStackTrace()
                    _getRoomEnergyChargeStatus.value= BaseStatus.ERROR("无法获取房间电费，因为：${e.message}")
                }
            }
        }
    }
}