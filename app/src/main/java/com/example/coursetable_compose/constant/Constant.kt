package com.example.coursetable_compose.constant

import androidx.compose.ui.unit.dp
import com.example.coursetable_compose.utils.SPUtils

//课表item大小
var COURSE_TABLE_ITEM_HEIGHT=SPUtils.getIns().courseTableHeight.dp
val COURSE_TABLE_ITEM_WIDTH=35.dp
//刷新间隔时间-6 小时
//const val REFRESH_DATE=1000*60*60*6
const val REFRESH_DATE=1000*60*2