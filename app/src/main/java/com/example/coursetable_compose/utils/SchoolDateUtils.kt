package com.example.coursetable_compose.utils

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date

object SchoolDateUtils {
    const val SCHOOL_YEAR: String = "学年"

    const val YEAR: String = "year"

    const val SEMESTER: String = "semester"


    fun currentSchoolSemester(): String {
        val key=
            "xnm=${currentYear-1}&xqm=${
                if (currentSemester(
                        currentMonth
                    ) == "1"
                ) "3" else "12"
            }&xqh_id=3"
        return key
    }

    fun currentYear(year: Int): String {
        return getCurrentYearSemester(
            year,
            currentMonth,
            YEAR
        )
    }

    fun currentSemester(month: Int): String {
        return getCurrentYearSemester(
            currentYear,
            month,
            SEMESTER
        )
    }

    fun getCurrentYearSemester(
        currentYear: Int,
        currentMonth: Int,
        yearOrSemester: String
    ): String {
        val year: String
        val semester: String
        if (currentMonth >= 1 && currentMonth <= 2) {
            year = (currentYear - 1).toString() + "-" + currentYear + SCHOOL_YEAR
            semester = "1"
        } else if (currentMonth >= 3 && currentMonth <= 8) {
            year = (currentYear - 1).toString() + "-" + (currentYear) + SCHOOL_YEAR
            semester = "2"
        } else {
            year = currentYear.toString() + "-" + (currentYear + 1) + SCHOOL_YEAR
            semester = "1"
        }
        return if (yearOrSemester == YEAR) {
            year
        } else {
            semester
        }
    }

    val currentYear: Int
        get() {
            val cal = Calendar.getInstance()
            val year = cal[Calendar.YEAR]
            return year
        }

    val currentMonth: Int
        get() {
            val cal = Calendar.getInstance()
            val month = cal[Calendar.MONTH] + 1
            return month
        }
}
