package com.example.coursetable_compose.utils

import android.content.Context
import com.example.coursetable_compose.bean.ConfigBean
import com.example.coursetable_compose.bean.UserBean
import com.example.coursetable_compose.bean.defaultPageModeMap
import com.example.coursetable_compose.utils.Utils.courseTableColor
import com.example.coursetable_compose.utils.Utils.defaultDayCourseTable
import com.google.gson.Gson
import com.mypotota.datamodel.bean.CourseTable
import com.mypotota.datamodel.bean.EnergyChargeBeanSubList
import com.mypotota.datamodel.bean.GradesBean

class SPUtils private constructor(context: Context) {
    private val sharedPreferences = context.getSharedPreferences("spUtils", Context.MODE_PRIVATE)

    companion object {
        private lateinit var spUtils: SPUtils

        fun init(context: Context) {
            spUtils = SPUtils(context)
        }

        @Synchronized
        fun getIns(): SPUtils {
            return spUtils
        }
    }

    private var config: ConfigBean

    init {
        var configString = sharedPreferences.getString("_config", null)
        if (configString == null) {
            val configBean = ConfigBean()
            configBean.startFirstWeekDate = "2024-02-19"
            configBean.countWeekNumber = 19
            configBean.colorMapByCourse = HashMap()
            configBean.dayCourseNumber = defaultDayCourseTable
            configBean.courseColor = courseTableColor
            sharedPreferences.edit().putString("_config", Gson().toJson(configBean)).apply()
            configString = sharedPreferences.getString("_config", null)
        }
        config = Gson().fromJson(configString, ConfigBean::class.java)
        if (config.homePageMode.size!= defaultPageModeMap.size) {
            setMainPageModeMap(defaultPageModeMap)
        }
    }


    private fun save() {
        sharedPreferences.edit().putString("_config", Gson().toJson(config)).apply()
    }


    //账号
    fun getAccount()=config.account
    fun setAccount(account:String){
        config.account=account
        save()
    }
    //密码
    fun getPassword()=config.password
    fun setPassword(password:String){
        config.password=password
        save()
    }
    //第一周时间
    fun getStartFirstWeekDate()=config.startFirstWeekDate
    fun setStartFirstWeekDate(date:String){
        config.startFirstWeekDate=date
        save()
    }
    //课表颜色
    fun getColorMapByCourse(): HashMap<String,Long> {
        return config.colorMapByCourse
    }
    fun setColorMapByCourse(map:HashMap<String,Long>){
        config.colorMapByCourse=map
        save()
    }


    //颜色列表
    fun getColorList()=config.courseColor
    fun setColorList(colors:List<Long>){
        config.courseColor=colors
        save()
    }
    //总周次
    fun getCountWeekNumber()=config.countWeekNumber
    fun setCountWeekNumber(countWeekNumber:Int){
        config.countWeekNumber=countWeekNumber
        save()
    }
    //每天几节课
    fun getDayCourseNumber()=config.dayCourseNumber
    fun setDayCourseNumber(dayCourseNumber:List<String>){
        config.dayCourseNumber=dayCourseNumber
        save()
    }
    //课表
    fun getCourseTable(): CourseTable? {
        return config.courseTable
    }
    fun setCourseTable(courseTable: CourseTable?){
        config.courseTable=courseTable
        save()
    }
    //进入时同步
    fun setEnterOnSync(enterOnSync:Boolean){
        config.enterOnSync=enterOnSync
        save()
    }
    fun getEnterOnSync()=config.enterOnSync
    //深色
    fun setDark(isDark:Boolean){
        config.isDark=isDark
        save()
    }
    fun getDark()=config.isDark
    //添加不再提醒
    fun setReadNotice(objectId: String){
        config.readNotice.add(objectId)
        save()
    }
    fun getReadNotice()=config.readNotice


    //是否可用
    fun setAvailable(usable: Boolean) {
        config.available=usable
        save()
    }
    fun getAvailable()=config.available
    //隐私策略
    fun setPrivacyPolicy(privacyPolicy: String?) {
        config.privacyPolicy=privacyPolicy
        save()
    }
    fun getPrivacyPolicy()=config.privacyPolicy
    //统计
    fun isStatistics() =config.isStatistics
    fun setStatistics(isStatistics:Boolean){
        config.isStatistics=isStatistics
        save()
    }
    //开源地址
    fun setOpenSourceAddress(openSourceAddress: String) {
        config.openSourceAddress=openSourceAddress
        save()
    }
    fun getOpenSourceAddress()=config.openSourceAddress
    //是否已读反馈通知
    fun isReadFeedBackNotice() = config.isReadFeedBackNotice
    fun setReadFeedBackNotice(isRead: Boolean) {
        config.isReadFeedBackNotice = isRead
        save()
    }

    //反馈已读须知
    fun setFeedbackNoticeForUse(feedbackNoticeForUse: String) {
        config.feedbackNoticeForUse = feedbackNoticeForUse
        save()
    }

    fun getFeedbackNoticeForUse() = config.feedbackNoticeForUse
    //是否已读小组课表须知
    fun isReadGroupCourseTableTip()=config.isReadGroupCourseTableTip
    fun setReadGroupCourseTableTip(isReadGroupCourseTableTip: Boolean){
        config.isReadGroupCourseTableTip = isReadGroupCourseTableTip
        save()
    }
    //主页显示的小组
    fun getHomeShowGroupList() =config.homeShowGroupList
    fun addHomeShowGroupList(objectId: String){
        config.homeShowGroupList.add(objectId)
        save()
    }
    fun removeHomeShowGroupList(objectId: String){
        config.homeShowGroupList.remove(objectId)
        save()
    }
    //查电费房间号
    fun setRoomNumber(roomNumber: String) {
        config.roomNumber=roomNumber
        save()
    }
    fun getRoomNumber()=config.roomNumber
    fun setRoomEnergyCharge(energyChargeBeanSubList: EnergyChargeBeanSubList) {
        config.energyCharge=energyChargeBeanSubList
        save()
    }
    fun getRoomEnergyCharge()=config.energyCharge
    //背景图片
    fun setBackgroundImagePath(path: String?) {
        config.backgroundImagePath=path
        save()
    }
    fun getBackgroundImagePath()=config.backgroundImagePath
    //背景透明度
    fun getBackgroundAlpha()=config.backgroundAlpha
    fun setBackgroundAlpha(alpha:Float){
        config.backgroundAlpha=alpha
        save()
    }
    //是否显示自动刷新
    fun setShowAutoRefresh(autoRefresh: Boolean) {
        config.showAutoRefresh=autoRefresh
        save()
    }
    fun getShowAutoRefresh():Boolean{
        return config.showAutoRefresh
    }
    //是否打开自动刷新
    fun setAutoRefresh(autoRefresh: Boolean) {
        config.isAutoRefresh=autoRefresh
        save()
    }
    fun getAutoRefresh():Boolean{
        return config.isAutoRefresh
    }
    //上次自动刷新的时间
    fun setAutoRefreshDate(date: Long) {
        config.autoRefreshDate=date
        save()
    }
    fun getAutoRefreshDate():Long{
        return config.autoRefreshDate
    }
    //主页模块显示顺序
    fun getMainPageModeMap():Map<String,Boolean>{
        return config.homePageMode
    }
    fun setMainPageModeMap(map:Map<String,Boolean>){
        config.homePageMode=map
        save()
    }
    //成绩列表
    fun setGradesList(items: List<GradesBean.Item>) {
        config.gradesList=items
        save()
    }
    fun getGradesList(): List<GradesBean.Item> {
        return config.gradesList
    }
    //圆角度数
    fun getRadius(): Int {
        return config.radius
    }
    fun setRadius(radius:Int) {
        config.radius=radius
        save()
    }

    fun setQQGroup(qqGroup: String) {
        config.qqGroup=qqGroup
        save()
    }
    fun getQQGroup(): String {
        return config.qqGroup
    }

    //成绩刷新提示
    var isReadGradesRefresh: Boolean
        get() = config.isReadGradesRefresh
        set(value) {
            config.isReadGradesRefresh=value
            save()
        }
    //课表刷新提示
    var isReadCourseRefresh: Boolean
        get() = config.isReadCourseRefresh
        set(value) {
            config.isReadCourseRefresh=value
            save()
        }
    //左侧时间显示
    var isShowLeftTime: Boolean
        get() = config.isShowLeftTime
        set(value) {
            config.isShowLeftTime=value
            save()
        }
    //顶部日期显示
    var isShowTopDate: Boolean
        get() = config.isShowTopDate
        set(value) {
            config.isShowTopDate=value
            save()
        }
    //默认课表高度
    var courseTableHeight: Int
        get() = config.courseTableHeight
        set(value) {
            config.courseTableHeight=value
            save()
        }
    //获取用户列表
    var userList: ArrayList<UserBean>
        get() = config.userList
        set(value) {
            config.userList=value
            save()
        }


}