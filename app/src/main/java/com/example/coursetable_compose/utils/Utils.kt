package com.example.coursetable_compose.utils

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.BitmapFactory
import android.widget.Toast
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import com.mypotota.datamodel.bean.Kb
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.Date
import java.util.HashMap

object Utils {


    fun filePath2Bitmap(filePath: String): ImageBitmap {
        return BitmapFactory.decodeFile(filePath).asImageBitmap()
    }

    val courseTableColor = listOf(
        0xff5c2223,
        0xffeea2a4,
        0xffc02c38,
        0xff806d9e,
        0xff525288,
        0xff47484c,
        0xff2b333e,
        0xff3170a7,
        0xff495c69,
        0xff134857,
        0xff12a182,
        0xff2c9678,
        0xff43b244,
        0xfff97d1c,
        0xff835e1d,
        0xff5e5314,
        0xfff43e06,
        0xffd9a40e,
        0xffffa60f,
        0xffeea08c,
        0xffbacf65,
        0xffd6a01d,
        0xfffeba07,
        0xffe8b004,
    )


    val defaultDayCourseTable = listOf(
        "08:30\n09:15",
        "09:20\n10:05",
        "10:25\n11:10",
        "11:15\n12:00",
        "12:00\n12:55",
        "13:00\n13:55",
        "14:00\n14:45",
        "14:50\n15:35",
        "15:45\n16:30",
        "16:35\n17:20",
        "17:20\n18:25",
        "18:30\n19:15",
        "19:20\n20:05",
    )

    fun weekNumber2String(week:Int): String {
        return when(week){
            1->"周一"
            2->"周二"
            3->"周三"
            4->"周四"
            5->"周五"
            6->"周六"
            7->"周日"
            else->"未知"
        }
    }

    fun getCourseColor(kb: Kb, colorMap: HashMap<String, Long>): Long {
        return colorMap["${kb.kcmc}-${kb.xm}"]!!
    }

    //根据课表时间段获取课表上课时间下课时间
    fun kbJcTime(jc: String): String {
        val jcSp = jc.replace("节", "").split("-")
        val first = jcSp[0].toInt() - 1
        val last = jcSp[1].toInt() - 1
        val firstTime = defaultDayCourseTable[first].split("\n")[0]
        val lastTime = defaultDayCourseTable[last].split("\n")[1]
        return "$firstTime~$lastTime"
    }


    fun currentWeekNumber(date: String): Int {
        val startDateStr: String = date
        val endDateStr = SimpleDateFormat("yyyy-MM-dd").format(Date())
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        val startDate = LocalDate.parse(startDateStr, formatter)
        val endDate = LocalDate.parse(endDateStr, formatter)
        return ChronoUnit.WEEKS.between(startDate, endDate).toInt() + 1
    }




    fun copy(context: Context, content: String) {
        val manager: ClipboardManager =
            context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val mClipData = ClipData.newPlainText("Label", content)
        manager.setPrimaryClip(mClipData)
        Toast.makeText(context, "复制成功", Toast.LENGTH_SHORT).show()
    }

}