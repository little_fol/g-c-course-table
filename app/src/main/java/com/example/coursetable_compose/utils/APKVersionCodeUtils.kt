package com.example.coursetable_compose.utils

import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager


object APKVersionCodeUtils {
    @Throws(Exception::class)
     fun getVersionName(context:Context): String {
        // 获取packagemanager的实例
        val packageManager: PackageManager = context.packageManager
        // getPackageName()是你当前类的包名，0代表是获取版本信息
        val packInfo: PackageInfo = packageManager.getPackageInfo(context.packageName, 0)
        val version = packInfo.versionName
        return version
    }

    fun getSystemName(): String {
        val build = Class.forName("android.os.Build")
        return if(build==null){
            "Nothing"
        }else{
            "Android"
        }
    }

    fun getVersionNumber(context: Context): Int {
        // 获取packagemanager的实例
        val packageManager: PackageManager = context.packageManager
        // getPackageName()是你当前类的包名，0代表是获取版本信息
        val packInfo: PackageInfo = packageManager.getPackageInfo(context.packageName, 0)
        val version = packInfo.versionCode
        return version
    }
}