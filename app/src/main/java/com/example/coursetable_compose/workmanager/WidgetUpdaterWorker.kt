package com.example.coursetable_compose.workmanager

import android.content.Context
import androidx.glance.appwidget.GlanceAppWidgetManager
import androidx.glance.appwidget.updateAll
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.example.coursetable_compose.ui.glancewidget.CourTableWidget

class WidgetUpdaterWorker(private val appContext: Context, params: WorkerParameters) :
    CoroutineWorker(appContext, params) {

    override suspend fun doWork(): Result {
        val manager = GlanceAppWidgetManager(appContext)
        val widget = CourTableWidget()
        widget.updateAll(appContext)

        return Result.success()
    }
}