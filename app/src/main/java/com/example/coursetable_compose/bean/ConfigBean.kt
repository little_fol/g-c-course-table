package com.example.coursetable_compose.bean

import com.mypotota.datamodel.bean.CourseTable
import com.mypotota.datamodel.bean.EnergyChargeBeanSubList
import com.mypotota.datamodel.bean.GradesBean
import java.io.Serializable

private const val OPEN_SOURCE_ADDRESS="https://gitee.com/little_fol/g-c-course-table"

val defaultPageModeMap=mapOf(
    Pair("今明课表",true),
    Pair("小组课表",true),
    Pair("查电费",true),
    Pair("我的成绩",true),
    Pair("所有通知",true),
)
data class UserBean(
    var username:String,
    var password:String,
)

data class ConfigBean(
    var account:String?=null,
    var password:String?=null,
    var userList:ArrayList<UserBean> = ArrayList(),
    var enterOnSync:Boolean=false,
    var isDark:Boolean=false,
    var available:Boolean=true,
    var courseTable: CourseTable?=null,
    var startFirstWeekDate:String?=null,
    var privacyPolicy: String?=null,
    var isStatistics:Boolean=true,
    var isReadGroupCourseTableTip:Boolean= false,
    var openSourceAddress:String=OPEN_SOURCE_ADDRESS,
    var isReadFeedBackNotice: Boolean=false,
    val readNotice: ArrayList<String> =ArrayList(),
    var countWeekNumber:Int=19,
    var feedbackNoticeForUse: String?=null,
    var roomNumber: String? = null,
    var qqGroup: String= "",
    var colorMapByCourse:HashMap<String,Long> =HashMap(),
    var courseColor:List<Long> =ArrayList(),
    val homeShowGroupList: ArrayList<String> = ArrayList(),
    var energyCharge: EnergyChargeBeanSubList?=null,
    var backgroundImagePath: String?=null,
    var showAutoRefresh: Boolean=false,
    var isAutoRefresh: Boolean=false,
    var isReadGradesRefresh: Boolean=true,
    var isReadCourseRefresh: Boolean=true,
    var isShowLeftTime: Boolean=true,
    var isShowTopDate: Boolean=true,
    var autoRefreshDate:Long=0L,
    var dayCourseNumber:List<String> =ArrayList(),
    var gradesList:List<GradesBean.Item> =ArrayList(),
    var homePageMode:Map<String,Boolean> = defaultPageModeMap,
    var backgroundAlpha:Float=1.0f,
    var radius: Int=20,
    var courseTableHeight: Int=50
): Serializable {


}

